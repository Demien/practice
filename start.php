<?php
include "google.php"
?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="uk-UA">
 <head>

  <meta http-equiv="Content-type" content="text/html" charset="UTF-8"/>
  <meta name="generator" content="2018.0.0.379"/>
  
  <script type="text/javascript">
   // Redirect to phone/tablet as necessary
(function(a,b,c){var d=function(){if(navigator.maxTouchPoints>1)return!0;if(window.matchMedia&&window.matchMedia("(-moz-touch-enabled)").matches)return!0;for(var a=["Webkit","Moz","O","ms","Khtml"],b=0,c=a.length;b<c;b++){var f=a[b]+"MaxTouchPoints";if(f in navigator&&navigator[f])return!0}try{return document.createEvent("TouchEvent"),!0}catch(d){}return!1}(),g=function(a){a+="=";for(var b=document.cookie.split(";"),c=0;c<b.length;c++){for(var f=b[c];f.charAt(0)==" ";)f=f.substring(1,f.length);if(f.indexOf(a)==
0)return f.substring(a.length,f.length)}return null};if(g("inbrowserediting")!="true"){var f,g=g("devicelock");g=="phone"&&c?f=c:g=="tablet"&&b&&(f=b);if(g!=a&&!f)if(window.matchMedia)window.matchMedia("(max-device-width: 415px)").matches&&c?f=c:window.matchMedia("(max-device-width: 960px)").matches&&b&&d&&(f=b);else{var a=Math.min(screen.width,screen.height)/(window.devicePixelRatio||1),g=window.screen.systemXDPI||0,i=window.screen.systemYDPI||0,g=g>0&&i>0?Math.min(screen.width/g,screen.height/i):
0;(a<=370||g!=0&&g<=3)&&c?f=c:a<=960&&b&&d&&(f=b)}if(f)document.location=f+(document.location.search||"")+(document.location.hash||""),document.write('<style type="text/css">body {visibility:hidden}</style>')}})("desktop","","phone/start.php");

// Update the 'nojs'/'js' class on the html node
document.documentElement.className = document.documentElement.className.replace(/\bnojs\b/g, 'js');

// Check that all required assets are uploaded and up-to-date
if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["museutils.js", "museconfig.js", "jquery.musepolyfill.bgsize.js", "jquery.watch.js", "jquery.musemenu.js", "webpro.js", "musewpslideshow.js", "jquery.museoverlay.js", "touchswipe.js", "jquery.scrolleffects.js", "require.js", "index.css"], "outOfDate":[]};
</script>
  
  <link media="only screen and (max-width: 370px)" rel="alternate" href="http://cnap.berdychiv.com.ua/phone/start.php"/>
  <link rel="shortcut icon" href="images/logo.ico?crc=3910096577"/>
  <title>ЦНАП | Бердичів</title>
  <!-- CSS -->
  <link rel="stylesheet"  href="css/site_global.css?crc=281137763"/>
  <link rel="stylesheet"  href="css/master_______-a.css?crc=4026516166"/>
  <link rel="stylesheet"  href="css/index.css?crc=496365154" id="pagesheet"/>
  <!-- IE-only CSS -->
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="css/iefonts_index.css?crc=4006273545"/>
  <![endif]-->
  <!-- Other scripts -->
  <script type="text/javascript">
   var __adobewebfontsappname__ = "muse";
</script>
  <script src="https://webfonts.creativecloud.com/roboto:n4,n3:all.js" type="text/javascript"></script>
  <style>

::-webkit-scrollbar-button{background-image:url();background-repeat:no-repeat;width:3px;height:0}::-webkit-scrollbar-track{background-color:#fff;box-shadow:0 0 3px #adadad inset}::-webkit-scrollbar-thumb{-webkit-border-radius:2px;border-radius:2px;background-color:#93E5BA;box-shadow:0 1px 1px #63A884 inset}::-webkit-resizer{background-image:url();background-repeat:no-repeat;width:7px;height:0}::-webkit-scrollbar{width:11px}</style>
<style>
div.card{height:390px;overflow-y:scroll}div.container{padding:5px}.cardPlate{box-shadow:0 1px 3px rgba(0,0,0,0.12),0 1px 2px rgba(0,0,0,0.24);transition:all .2s;transform-origin:center center}.cardPlate:hover{box-shadow:0 14px 28px rgba(59,178,184,0.25),0 10px 10px rgba(59,178,184,0.22);transform:scale(1.05)}
</style>
  <!--HTML Widget code-->
  
</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34121,<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34158{color:#727270;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35669{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u7754{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:56px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35756{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37161{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:60px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34502,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35766,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35771,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35781,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35796,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35801,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37183,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37188,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37193,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37210,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37218,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37229,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37234,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37245,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37250,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u37268{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8340{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34507{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8482,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9236,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9270,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9304{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34512{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8576{color:#3BB2B8;font-size:50px;display:table-cell;width:60px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8620{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34527{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8611{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8743{color:#3BB2B8;font-size:50px;display:table-cell;width:60px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34552{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8803{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u34557{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u7832,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8845,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8856,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8865,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u8882{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9401{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:56px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9431,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9594,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9653,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9739{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35577{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9796{color:#3BB2B8;font-size:50px;display:table-cell;width:60px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u35664{color:#3BB2B8;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9869{color:#727270;font-size:50px;display:table-cell;width:50px;height:59px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u9877{color:#727270;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u20954{color:#727270;font-size:50px;display:table-cell;width:50px;height:60px;vertical-align:middle;text-align:center}</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u20956,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u21168,</style><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"><style>.u21176{color:#727270;font-size:50px;display:table-cell;width:50px;height:61px;vertical-align:middle;text-align:center}
</style>

 </head>
 <body>

  <div class="clearfix" id="page"><!-- group -->
   <div class="clearfix grpelem" id="pstart"><!-- column -->
    <a class="anchor_item colelem" id="start"></a>
    <a class="anchor_item colelem" id="cat"></a>
    <a class="anchor_item colelem" id="s1"></a>
   </div>
   <div class="clearfix grpelem" id="ppu35033"><!-- column -->
    <div class="clearfix colelem" id="pu35033"><!-- group -->
     <div class="browser_width grpelem" id="u35033-bw">
      <div class="shadow gradient" id="u35033"><!-- group -->
       <div class="clearfix" id="u35033_align_to_page">
        <div class="clearfix grpelem" id="pu35034"><!-- group -->
         <div class="museBGSize grpelem" id="u35034"><!-- simple frame --></div>
         <div class="clearfix grpelem" id="u35035-8"><!-- content -->
          <p id="u35035-2">БЕРДИЧІВ</p>
          <p id="u35035-4">Центр надання</p>
          <p id="u35035-6">адміністративних послуг</p>
         </div>
        </div>
        <nav class="MenuBar clearfix grpelem" id="menuu35036"><!-- horizontal box -->
         <div class="MenuItemContainer clearfix grpelem" id="u35051"><!-- vertical box -->
          <a class="nonblock nontext MenuItem MenuItemWithSubMenu MuseMenuActive rounded-corners rgba-background shadow clearfix colelem" id="u35052" href="start.php"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u35054-4"><!-- content --><p>Головна</p></div></a>
         </div>
         <div class="MenuItemContainer clearfix grpelem" id="u35044"><!-- vertical box -->
          <a class="nonblock nontext MenuItem MenuItemWithSubMenu rounded-corners rgba-background shadow clearfix colelem" id="u35045" href="%d0%ba%d0%be%d0%bd%d1%81%d1%83%d0%bb%d1%8c%d1%82%d0%b0%d1%86%d1%96%d1%8f.html"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u35046-4"><!-- content --><p>Запитання</p></div></a>
         </div>
         <div class="MenuItemContainer clearfix grpelem" id="u36535"><!-- vertical box -->
          <a class="nonblock nontext MenuItem MenuItemWithSubMenu rounded-corners rgba-background shadow clearfix colelem" id="u36538" href="%d1%81%d0%ba%d0%b0%d1%80%d0%b3%d0%b0.html"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u36539-4"><!-- content --><p>Скарга</p></div></a>
         </div>
         <div class="MenuItemContainer clearfix grpelem" id="u35037"><!-- vertical box -->
          <a class="nonblock nontext MenuItem MenuItemWithSubMenu rounded-corners rgba-background shadow clearfix colelem" id="u35040" href="%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.html"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u35043-4"><!-- content --><p>Контакти</p></div></a>
         </div>
        </nav>
       </div>
      </div>
     </div>
     <div class="browser_width grpelem" id="u4128-bw">
      <div class="museBGSize" id="u4128"><!-- column -->
       <div class="clearfix" id="u4128_align_to_page">
        <div class="clearfix colelem" id="pu1452"><!-- group -->
         <a class="nonblock nontext MuseLinkActive museBGSize grpelem" id="u1452" href="start.php"><!-- simple frame --></a>
         <nav class="MenuBar rounded-corners clearfix grpelem" id="menuu3984"><!-- horizontal box -->
          <div class="MenuItemContainer clearfix grpelem" id="u3999"><!-- vertical box -->
           <a class="nonblock nontext MenuItem MenuItemWithSubMenu MuseMenuActive rounded-corners shadow rgba-background clearfix colelem" id="u4002" href="start.php"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u4004-4"><!-- content --><p>Головна</p></div></a>
          </div>
          <div class="MenuItemContainer clearfix grpelem" id="u3992"><!-- vertical box -->
           <a class="nonblock nontext MenuItem MenuItemWithSubMenu rounded-corners rgba-background clearfix colelem" id="u3993" href="%d0%ba%d0%be%d0%bd%d1%81%d1%83%d0%bb%d1%8c%d1%82%d0%b0%d1%86%d1%96%d1%8f.html"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u3994-4"><!-- content --><p>Запитання</p></div></a>
          </div>
          <div class="MenuItemContainer clearfix grpelem" id="u36659"><!-- vertical box -->
           <a class="nonblock nontext MenuItem MenuItemWithSubMenu rounded-corners rgba-background clearfix colelem" id="u36662" href="%d1%81%d0%ba%d0%b0%d1%80%d0%b3%d0%b0.html"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u36664-4"><!-- content --><p>Скарга</p></div></a>
          </div>
          <div class="MenuItemContainer clearfix grpelem" id="u3985"><!-- vertical box -->
           <a class="nonblock nontext MenuItem MenuItemWithSubMenu rounded-corners rgba-background clearfix colelem" id="u3988" href="%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.html"><!-- horizontal box --><div class="MenuItemLabel NoWrap clearfix grpelem" id="u3989-4"><!-- content --><p>Контакти</p></div></a>
          </div>
         </nav>
        </div>
        <div class="clearfix colelem" id="u4145-10"><!-- content -->
         <p id="u4145-2">Вітаємо</p>
         <p id="u4145-4">на інформаційному ресурсі</p>
         <p id="u4145-6">Центру надання</p>
         <p id="u4145-8">адміністративних послуг</p>
        </div>
        <a class="nonblock nontext Button anim_swing cardPlate rounded-corners clearfix colelem" id="buttonu19012" href="start.php#start"><!-- container box --><div class="clearfix grpelem" id="u19013-6"><!-- content --><p>Обрати</p><p id="u19013-4">напрямок</p></div></a>
        <a class="nonblock nontext anim_swing clip_frame colelem" id="u1466" href="start.php#start"><!-- image --><img class="block" id="u1466_img" src="images/arr.gif?crc=4000146970" data-hidpi-src="images/arr_2x.gif?crc=4000146970" alt="" width="50" height="50"/></a>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="pu280-8"><!-- group -->
     <div class="clearfix grpelem" id="u280-8"><!-- content -->
      <p id="u280-2">1/3</p>
      <p id="u280-4">Оберіть</p>
      <p id="u280-6">напрямок</p>
     </div>
     <div class="PamphletWidget clearfix grpelem" id="pamphletu341"><!-- none box -->
      <div class="ThumbGroup clearfix grpelem" id="u342"><!-- none box -->
       <div class="popup_anchor" id="u403popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u403" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u773"><!-- simple frame --></div><div class="clearfix colelem" id="u767-4"><!-- content --><p><?php  echo Neruhomist() ?></p></div></a>
       </div>
       <div class="popup_anchor" id="u800popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u800" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u855"><!-- simple frame --></div><div class="clearfix colelem" id="u831-4"><!-- content --><p><?php echo FOP() ?></p></div></a>
       </div>
       <div class="popup_anchor" id="u810popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u810" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u849"><!-- simple frame --></div><div class="clearfix colelem" id="u869-4"><!-- content --><p><?php  echo Propiska() ?></p></div></a>
       </div>
       <div class="popup_anchor" id="u816popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u816" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u889"><!-- simple frame --></div><div class="clearfix colelem" id="u895-6"><!-- content --><p>&nbsp;<?php  echo SportFamily() ?></p><p></p></div></a>
       </div>
       <div class="popup_anchor" id="u386popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u386" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u565"><!-- simple frame --></div><div class="clearfix colelem" id="u503-6"><!-- content --><p><?php echo hromob() ?></p><p></p></div></a>
       </div>
       <div class="popup_anchor" id="u395popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u395" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u753"><!-- simple frame --></div><div class="clearfix colelem" id="u741-4"><!-- content --><p><?php echo DABK() ?></p></div></a>
       </div>
       <div class="popup_anchor" id="u1539popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u1539" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u1554"><!-- simple frame --></div><div class="clearfix colelem" id="u1560-4"><!-- content --><p><?php echo GEOKADASTR() ?></p></div></a>
       </div>
       <div class="popup_anchor" id="u344popup">
        <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u344" href="start.php#cat"><!-- column --><div class="museBGSize colelem" id="u534"><!-- simple frame --></div><div class="clearfix colelem" id="u491-6"><!-- content --><p>&nbsp;<?php echo ZKH() ?></p><p></p></div></a>
       </div>
      </div>
      <div class="popup_anchor" id="u348popup">
       <div class="ContainerGroup clearfix" id="u348"><!-- stack box -->
        <div class="Container invi clearfix grpelem" id="u407"><!-- group -->
         <div class="clearfix grpelem" id="u30677-8"><!-- content -->
          <p id="u30677-2">2/3</p>
          <p id="u30677-4">Оберіть</p>
          <p id="u30677-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu3023"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u3024"><!-- none box -->
           <div class="popup_anchor" id="u3031popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u3031" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u3032"><!-- simple frame --></div><div class="clearfix colelem" id="u3033-4"><!-- content --><p><?php echo RegRes() ?></p></div></a>
           </div>
           <div class="popup_anchor" id="u3034popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u3034" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u3035"><!-- simple frame --></div><div class="clearfix colelem" id="u3036-4"><!-- content --><p><?php echo GiveInfo() ?></p></div></a>
           </div>
           <div class="popup_anchor" id="u3028popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u3028" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u3030"><!-- simple frame --></div><div class="clearfix colelem" id="u3029-4"><!-- content --><p><?php echo WnesZmin() ?></p></div></a>
           </div>
           <div class="popup_anchor" id="u3025popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u3025" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u3027"><!-- simple frame --></div><div class="clearfix colelem" id="u3026-6"><!-- content --><p id="u3026-2"><span class="ts-----1" id="u3026"><?php echo Zaboron() ?></span></p><p id="u3026-4"><span class="ts-----1" id="u3026-3"></span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u3040popup">
           <div class="ContainerGroup clearfix" id="u3040"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u3115"><!-- group -->
             <div class="clearfix grpelem" id="u3116-8"><!-- content -->
              <p id="u3116-2">3/3</p>
              <p id="u3116-4">Оберіть</p>
              <p id="u3116-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu3117"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u3161"><!-- none box -->
               <div class="popup_anchor" id="u3168popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3168"><!-- group -->
                 <div class="clearfix grpelem" id="u3169-4"><!-- content -->
                  <p>Державна реєстрація права власності на нерухоме майно</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u3164popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3164"><!-- group -->
                 <div class="clearfix grpelem" id="u3165-4"><!-- content -->
                  <p>Державна реєстрація іншого речового права</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u3162popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3162"><!-- group -->
                 <div class="clearfix grpelem" id="u3163-4"><!-- content -->
                  <p>Скасування запису Державного реєстру</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u20204popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u20204"><!-- group -->
                 <div class="clearfix grpelem" id="u20244-4"><!-- content -->
                  <p>Взяття на облік безхазяйного нерухомого майна.</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u3118popup">
               <div class="ContainerGroup clearfix" id="u3118"><!-- stack box -->
                <div class="Container invi Card24 clearfix grpelem" id="u3149"><!-- group -->
                 <div class="clearfix grpelem" id="pu3151-4"><!-- column -->
                  <div class="clearfix colelem" id="u3151-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3154-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3155-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3153-4"><!-- column -->
                  <div class="clearfix colelem" id="u3153-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 colelem" id="u20382"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВ НА ОКРЕМИЙ ІНДИВІДУАЛЬНО ВИЗНАЧЕНИЙ ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА (КВАРТИРА, ЖИТЛОВЕ ПРИМІЩЕННЯ)</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що підтверджує набуття у власність особою закріпленого за особою об&rsquo;єкта інвестування, передбачений законодавством (інвестиційний договір, договір про пайову участь, договір купівлі-продажу майнових прав тощо)</li>
<li>у разі придбання майнових прав на об&rsquo;єкт нерухомості документом, що підтверджує набуття у власність закріпленого за особою об&rsquo;єкта будівництва, є договір купівлі-продажу майнових прав</li>
<li>у разі придбання особою безпроцентних облігацій, за якими базовим товаром є одиниця нерухомості, документами, що підтверджують набуття у власність, є договір купівлі-продажу облігацій та документ, згідно з яким здійснилося закріплення</li>
<li>у разі участі особи у фонді фінансування будівництва документом, що підтверджує набуття у власність закріпленого за особою об&rsquo;єкта будівництва, є видана управителем такого фонду довідка про право довірителя на набуття у власність об&rsquo;єкта інвестування</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА ІНДИВІДУАЛЬНІ ОБ'ЄКТИ НЕРУХОМОГО МАЙНА, ЯКІ ЗАКІНЧЕНІ БУДІВНИЦТВОМ ДО 5 СЕРПНЯ 1992 РОКУ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що посвідчує речове право на земельну ділянку, на якій розташований об&rsquo;єкт нерухомого майна, у тому числі рішення відповідної ради про передачу (надання) земельної ділянки в користування або власність чи відомості про передачу (надання) земельної ділянки в користування або власність з погосподарської книги</li>
<li>виписка з погосподарської книги, надана виконавчим органом сільської ради (якщо такий орган не створений, - сільським головою), селищної, міської ради або відповідною архівною установою</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРИПИНЕННЯ ПРАВА ВЛАСНОСТІ НА ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА, ОБ&rsquo;ЄКТ НЕЗАВЕРШЕНОГО БУДІВНИЦТВА У ЗВ&rsquo;ЯЗКУ ІЗ ЙОГО ЗНИЩЕННЯМ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що посвідчує право власності на об&rsquo;єкт нерухомого майна (крім випадків, коли право власності на такий об&rsquo;єкт вже зареєстровано в Державному реєстрі прав або коли такі документи було знищено одночасно із знищенням такого об&rsquo;єкта)</li>
<li>документ, відповідно до якого підтверджується факт знищення</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА ОБ&rsquo;ЄКТ НЕЗАВЕРШЕНОГО БУДІВНИЦТВА</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що посвідчує речове право на земельну ділянку під таким об&rsquo;єктом (крім випадку, коли речове право на земельну ділянку вже зареєстровано в Державному реєстрі прав)</li>
<li>документ, що відповідно до законодавства надає право на виконання будівельних робіт</li>
</ul>
<p>Документ, що відповідно до законодавства надає право на виконання будівельних робіт, не вимагається у разі, коли реєстрація такого документа здійснювалася в Єдиному реєстрі документів.</p>
<ul>
<li>технічний паспорт на об&rsquo;єкт незавершеного будівництва</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА ПІДСТАВІ ЗАЯВИ СПАДКОЄМЦЯ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документи, що підтверджують набуття спадкодавцем права власності на нерухоме майно</li>
<li>витяг із Спадкового реєстру про наявність заведеної спадкової справи</li>
<li>документ, що містить відомості про склад спадкоємців, виданий нотаріусом чи уповноваженою на це посадовою особою органу місцевого самоврядування, якими заведено відповідну спадкову справу</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ У РАЗІ ВИТРЕБУВАННЯ НЕРУХОМОГО МАЙНА З ЧУЖОГО НЕЗАКОННОГО ВОЛОДІННЯ НА ПІДСТАВІ РІШЕННЯ СУДУ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документи, що підтверджують право власності на нерухоме майно належного власника</li>
<li>рішення суду, що набрало законної сили, про витребування нерухомого майна з чужого незаконного володіння</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА НЕРУХОМЕ МАЙНО, УТВОРЕНЕ ШЛЯХОМ ПОДІЛУ, У Т.Ч. В РЕЗУЛЬТАТІ ВИДІЛЕННЯ ОКРЕМОГО ОБ'ЄКТА АБО ОБ'ЄДНАННЯ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>умови наявності технічної можливості поділу або об&rsquo;єднання нерухомого майна та можливості використання такого майна як самостійного об&rsquo;єкта цивільних правовідносин</li>
<li>документ, що посвідчує право власності на об&rsquo;єкт нерухомого майна до його поділу або об&rsquo;єднання (крім випадків, коли право власності на такий об&rsquo;єкт вже зареєстровано в Державному реєстрі прав)</li>
<li>документ, що засвідчує прийняття в експлуатацію закінченого будівництвом об&rsquo;єкта (крім випадків, коли об&rsquo;єкт нерухомого майна створюється шляхом поділу або об&rsquo;єднання без проведення будівельних робіт, що потребують отримання дозволу на їх проведення)</li>
<li>технічний паспорт на новостворений об&rsquo;єкт нерухомого майна</li>
<li>документ, що підтверджує присвоєння об&rsquo;єкту нерухомого майна адреси (крім випадків поділу або об&rsquo;єднання таких об&rsquo;єктів нерухомого майна, як квартира, житлове або нежитлове приміщення тощо)</li>
<li>письмова згода всіх співвласників на проведення поділу або об&rsquo;єднання майна, що перебуває у спільній власності (для державної реєстрації права власності на нерухоме майно, що створюється шляхом поділу або об&rsquo;єднання майна, що перебуває у спільній власності)</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ У ЗВ'ЯЗКУ ІЗ ВИДІЛЕННЯМ НЕРУХОМОГО МАЙНА В НАТУРІ ВЛАСНИКАМ МАЙНОВИХ ПАЇВ ЧЛЕНІВ КСП</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>свідоцтво про право власності на майновий пай члена КСП (майновий сертифікат) з відміткою підприємства правонаступника реорганізованого КСП про виділення майна в натурі, засвідченою підписом керівника такого підприємства та печаткою</li>
<li>акт приймання-передачі нерухомого майна</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА ДЕРЖАВНОЇ ВЛАСНОСТІ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>витяг з Єдиного реєстру об&rsquo;єктів державної власності щодо такого об&rsquo;єкта</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА КОМУНАЛЬНОЇ ВЛАСНОСТІ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що підтверджує факт перебування об&rsquo;єкта нерухомого майна у комунальній власності, виданий відповідним органом місцевого самоврядування</li>
<li>документ, що підтверджує факт відсутності перебування об&rsquo;єкта нерухомого майна у державній власності, виданий Фондом державного майна чи його регіональним відділенням</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ У ЗВ&rsquo;ЯЗКУ З ПЕРЕДАЧЕЮ МАЙНА У ВЛАСНІСТЬ ФІЗИЧНИМ ТА ЮРИДИЧНИМ ОСОБАМ, ЩО ВИЙШЛИ ЗІ СКЛАДУ ЮРИДИЧНОЇ ОСОБИ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що посвідчує право власності юридичної особи на майно, що передається у власність фізичним та юридичним особам (крім випадку, коли право власності на таке майно вже зареєстровано в Державному реєстрі прав)</li>
<li>акт приймання-передачі майна або інший документ, що підтверджує факт передачі такого майна (справжність підписів на акті приймання-передачі майна або іншому документі засвідчується відповідно до Закону України &ldquo;Про нотаріат&rdquo;)</li>
<li>рішення органу або особи, уповноважених установчими документами юридичної особи або законом, про передачу майна у власність фізичній або юридичній особі, що вийшла із складу засновників (учасників) юридичної особи</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ У ЗВ&rsquo;ЯЗКУ З ПЕРЕДАЧЕЮ МАЙНА У ВЛАСНІСТЬ ЮРИДИЧНОЇ ОСОБИ ЯК ВНЕСОК ДО СТАТУТНОГО ФОНДУ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що посвідчує право власності особи на майно, що передається у власність юридичної особи (крім випадку, коли право власності на таке майно вже зареєстровано в Державному реєстрі прав)</li>
<li>акт приймання-передачі майна або інший документ, що підтверджує факт передачі такого майна (справжність підписів на акті приймання-передачі майна або іншому документі засвідчується відповідно до Закону України &ldquo;Про нотаріат&rdquo;)</li>
<li>рішення органу або особи, уповноважених установчими документами юридичної особи або законом (у разі, коли передача майна здійснюється іншою юридичною особою)</li>
<li>письмова згода всіх співвласників (у разі, коли передача здійснюється щодо майна, що перебуває у спільній власності)</li>
</ul>
<p><strong>&nbsp;</strong></p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ У ЗВ'ЯЗКУ З ПЕРЕДАЧЕЮ У ВЛАСНІСТЬ НЕРУХОМОГО МАЙНА У РЕЗУЛЬТАТІ ПРИПИНЕННЯ ЮРИДИЧНОЇ ОСОБИ АБО ВИДІЛУ З НЕЇ НОВОЇ ЮРИДИЧНОЇ ОСОБИ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що посвідчує право власності юридичної особи на майно, що передається у власність фізичним та юридичним особам (крім випадку, коли право власності на таке майно вже зареєстровано в Державному реєстрі прав)</li>
<li>ліквідаційний баланс, затверджений засновниками (учасниками) юридичної особи або органом, що прийняв рішення про ліквідацію юридичної особи, та письмова заява таких осіб, яким передано нерухоме майно юридичної особи, що припиняється, про розподіл між ними такого майна або рішення відповідного органу про подальше використання зазначеного майна (у разі ліквідації юридичної особи)</li>
<li>передавальний акт, затверджений засновниками (учасниками) юридичної особи або органом, який прийняв рішення про злиття, приєднання або перетворення юридичної особи</li>
<li>розподільний баланс, затверджений засновниками (учасниками) юридичної особи або органом, який прийняв рішення про поділ юридичної особи або виділ з неї нової юридичної особи (у разі поділу юридичної особи або виділу з неї нової юридичної особи)</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА НОВОСФОРМОВАНУ ЗЕМЕЛЬНУ ДІЛЯНКУ (ВИДІЛЕННЯ ЗЕМЕЛЬНОЇ ДІЛЯНКИ, ПОДІЛ АБО ОБ&rsquo;ЄДНАННЯ)</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>рішення органу виконавчої влади або органу місцевого самоврядування про передачу земельної ділянки у власність чи надання у постійне користування або про затвердження документації із землеустрою щодо формування земельної ділянки та передачу її у власність чи надання у постійне користування</li>
<li>документ, що посвідчує право власності на земельну ділянку до її поділу або об&rsquo;єднання (крім випадків, коли право власності на таку земельну ділянку вже зареєстровано в Державному реєстрі прав)</li>
<li>витяг з Державного земельного кадастру</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА РЕКОНСТРУЙОВАНИЙ ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА (В Т.Ч. В РЕЗУЛЬТАТІ ПЕРЕВЕДЕННЯ ОБ'ЄКТА ІЗ ЖИТЛОВОГО У НЕЖИТЛОВИЙ АБО НАВПАКИ)</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що посвідчує право власності на об&rsquo;єкт нерухомого майна до його реконструкції (крім випадків, коли право власності на такий об&rsquo;єкт вже зареєстровано в Державному реєстрі прав)</li>
<li>документ, що засвідчує прийняття в експлуатацію закінченого будівництвом об&rsquo;єкта</li>
<li>письмова заява або договір співвласників про розподіл часток у спільній власності на реконструйований об&rsquo;єкт нерухомого майна (у разі, коли державна реєстрація проводиться щодо майна, що набувається у спільну часткову власність)</li>
<li>договір про спільну діяльність або договір простого товариства (у разі, коли державна реєстрація проводиться щодо майна, реконструкція якого здійснювалась у результаті спільної діяльності)</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВА ВЛАСНОСТІ НА ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА ЗА РІШЕННЯМ СУДУ</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код платника податків</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>рішення суду, що набрало законної сили, щодо права власності на нерухоме майно</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
<p>&nbsp;</p>
<p><strong>ДЕРЖАВНА РЕЄСТРАЦІЯ ПРАВ НА НОВОЗБУДОВАНИЙ ОБ&rsquo;ЄКТ НЕРУХОМОГО МАЙНА</strong></p>
<ul>
<li>паспорт громадянина України</li>
<li>ідентифікаційний код</li>
<li>документ, що підтверджує повноваження уповноваженої особи</li>
<li>документ, що засвідчує прийняття в експлуатацію закінченого будівництвом об&rsquo;єкта</li>
<li>документ, що підтверджує присвоєння об&rsquo;єкту нерухомого майна адреси (документ не вимагається у разі, коли державна реєстрація права власності проводиться на індивідуальний об&rsquo;єкт нерухомого майна, збудований на земельній ділянці, право власності на яку зареєстровано в Державному реєстрі прав)</li>
<li>письмова заява або договір співвласників про розподіл часток у спільній власності (у разі, коли державна реєстрація проводиться щодо майна, що набувається у спільну часткову власність)</li>
<li>договір про спільну діяльність або договір простого товариства (у разі, коли державна реєстрація проводиться щодо майна, будівництво якого здійснювалось у результаті спільної діяльності)</li>
<li>технічний паспорт на об&rsquo;єкт нерухомого майна</li>
</ul>
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3157-4"><!-- content -->
                   <p id="u3157-2">Особисто (або уповноваженою особою) шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3152-16"><!-- content -->
                   <p id="u3152-2">Платно:</p>
                   <p class="ts-------------1" id="u3152-5">у строк, що не перевищує 5 робочих днів <span id="u3152-4">180 грн.</span></p>
                   <p class="ts-------------1" id="u3152-8">у строк 2 робочі дні <span id="u3152-7">1760 грн.</span></p>
                   <p class="ts-------------1" id="u3152-11">у строк 1 робочий день <span id="u3152-10">3520 грн.</span></p>
                   <p class="ts-------------1" id="u3152-14">у строк 2 години <span id="u3152-13">8810 грн.</span></p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u3119"><!-- group -->
                 <div class="clearfix grpelem" id="pu3121-4"><!-- column -->
                  <div class="clearfix colelem" id="u3121-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3127-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3123-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3120-4"><!-- column -->
                  <div class="clearfix colelem" id="u3120-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3124-10"><!-- content -->
                   <p id="u3124-2">• паспорт громадянина України</p>
                   <p id="u3124-4">• ідентифікаційний код платника податків</p>
                   <p id="u3124-6">• документ, що підтверджує повноваження уповноваженої особи</p>
                   <p id="u3124-8">• документи, що підтверджують виникнення, перехід та припинення іншого речового права на нерухоме майно</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3126-4"><!-- content -->
                   <p id="u3126-2">Особисто (або уповноваженою особою) шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3122-16"><!-- content -->
                   <p id="u3122-2">Платно:</p>
                   <p class="ts-------------1" id="u3122-5"><span id="u3122-3">у строк, що не перевищує 5 робочих днів </span><span id="u3122-4">90 грн.</span></p>
                   <p class="ts-------------1" id="u3122-8">у строк 2 робочі дні <span id="u3122-7">880 грн.</span></p>
                   <p class="ts-------------1" id="u3122-11">у строк 1 робочий день <span id="u3122-10">1760 грн.</span></p>
                   <p class="ts-------------1" id="u3122-14">у строк 2 години <span id="u3122-13">4410 грн</span></p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u3129"><!-- group -->
                 <div class="clearfix grpelem" id="pu3135-4"><!-- column -->
                  <div class="clearfix colelem" id="u3135-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3130-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3132-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3134-4"><!-- column -->
                  <div class="clearfix colelem" id="u3134-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3133-6"><!-- content -->
                   <p id="u3133-2">Документ, що посвідчує особу, його копія;</p>
                   <p id="u3133-4">Рішення суду про скасування рішення державного реєстратора про державну реєстрацію прав та їх обтяжень, що набрало законної сили;</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3137-4"><!-- content -->
                   <p id="u3137-2">Особисто (або уповноваженою особою) шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3136-4"><!-- content -->
                   <p id="u3136-2">Безоплатно (крім випадку надання інформації з Державного реєстру речових прав на нерухоме майно за бажанням заявника).</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u20211"><!-- group -->
                 <div class="clearfix grpelem" id="pu20291-4"><!-- column -->
                  <div class="clearfix colelem" id="u20291-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u20292-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u20293-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu20290-4"><!-- column -->
                  <div class="clearfix colelem" id="u20290-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u20295-8"><!-- content -->
                   <p id="u20295-2">Документ, що посвідчує особу, його копія;</p>
                   <p id="u20295-4">Документ, що підтверджує повноваження, його копія;</p>
                   <p id="u20295-6">Заявник додатково пред’являє документ, що посвідчує посадову особу.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u20296-4"><!-- content -->
                   <p id="u20296-2">Особисто (або уповноваженою особою) шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u20294-4"><!-- content -->
                   <p id="u20294-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u3060"><!-- group -->
             <div class="clearfix grpelem" id="u3600-8"><!-- content -->
              <p id="u3600-2">3/3</p>
              <p id="u3600-4">Оберіть</p>
              <p id="u3600-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu20387"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u20390"><!-- none box -->
               <div class="popup_anchor" id="u20393popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u20393"><!-- group -->
                 <div class="clearfix grpelem" id="u20394-4"><!-- content -->
                  <p>Надання інформації з Державного реєстру речових прав на нерухоме майно.</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u20401popup">
               <div class="ContainerGroup clearfix" id="u20401"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u20426"><!-- group -->
                 <div class="clearfix grpelem" id="pu20432-4"><!-- column -->
                  <div class="clearfix colelem" id="u20432-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u20431-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u20433-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu20428-4"><!-- column -->
                  <div class="clearfix colelem" id="u20428-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u20427-6"><!-- content -->
                   <p id="u20427-2">• паспорт громадянина України</p>
                   <p id="u20427-4">• ідентифікаційний код платника податків</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u20430-4"><!-- content -->
                   <p id="u20430-2">Особисто (або уповноваженою особою) шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u20429-10"><!-- content -->
                   <p id="u20429-2">Платно:</p>
                   <p id="u20429-5">отримання інформації, витягу в паперовій формі у строк, що не перевищує 1 робочого дня <span id="u20429-4">40 грн.</span></p>
                   <p id="u20429-8">отримання інформації, витягу в електронній формі після внесення плати за надання інформації через Інтернет з використанням платіжних систем <span id="u20429-7">20 грн.</span></p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u3171"><!-- group -->
             <div class="clearfix grpelem" id="u3603-8"><!-- content -->
              <p id="u3603-2">3/3</p>
              <p id="u3603-4">Оберіть</p>
              <p id="u3603-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu3172"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u3176"><!-- none box -->
               <div class="popup_anchor" id="u3585popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3585"><!-- group -->
                 <div class="clearfix grpelem" id="u3651-4"><!-- content -->
                  <p>У зв’язку з допущенням технічної помилки не з вини державного реєстратора прав на нерухоме майно</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u3591popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3591"><!-- group -->
                 <div class="clearfix grpelem" id="u3660-4"><!-- content -->
                  <p>У зв’язку із зміною відомостей про нерухоме майно, право власності та суб'єкта цього права, інші речові права та суб'єкта цих прав, обтяження прав на нерухоме майно та суб'єкта цих прав</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u3726popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3726"><!-- group -->
                 <div class="clearfix grpelem" id="u3748-4"><!-- content -->
                  <p>У зв’язку з допущенням технічної помилки з вини державного реєстратора прав на нерухоме майно.</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u3181popup">
               <div class="ContainerGroup clearfix" id="u3181"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u3588"><!-- group -->
                 <div class="clearfix grpelem" id="pu3688-4"><!-- column -->
                  <div class="clearfix colelem" id="u3688-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3689-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3690-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3684-4"><!-- column -->
                  <div class="clearfix colelem" id="u3684-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3686-10"><!-- content -->
                   <p id="u3686-2">Документ, що посвідчує особу заявника (пред’являється), копія надається.</p>
                   <p id="u3686-4">У разі подання заяви уповноваженою особою така особа, крім документа, що посвідчує її особу, пред’являє оригінал та подає копію документа, що підтверджує її повноваження;</p>
                   <p id="u3686-6">Документи, що є підставою для внесення зміни відомостей;</p>
                   <p id="u3686-8">Документ про внесення плати за надання інформації з Державного реєстру речових прав на нерухоме майно (у разі отримання інформаційної довідки з Державного реєстру речових прав на нерухоме майно)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3687-4"><!-- content -->
                   <p id="u3687-2">Особисто або уповноважена особа шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3685-5"><!-- content -->
                   <p id="u3685-3">Платно: у строк, що не перевищує 1 робочого дня <span id="u3685-2">70 грн.</span></p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u3594"><!-- group -->
                 <div class="clearfix grpelem" id="pu3709-4"><!-- column -->
                  <div class="clearfix colelem" id="u3709-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3710-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3711-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3705-4"><!-- column -->
                  <div class="clearfix colelem" id="u3705-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3707-12"><!-- content -->
                   <p id="u3707-2">• паспорт громадянина України</p>
                   <p id="u3707-4">• ідентифікаційний код платника податків</p>
                   <p id="u3707-6">• документ, що підтверджує повноваження уповноваженої особи</p>
                   <p id="u3707-8">• документи щодо зміни ідентифікаційних даних суб’єкта права, визначення часток у праві спільної власності чи їх зміни, зміни суб’єкта управління об’єктами державної власності, відомостей про об’єкт нерухомого майна, у тому числі зміни його технічних характеристик</p>
                   <p id="u3707-10">• виявлення технічної помилки в записах Державного реєстру прав чи документах, виданих за допомогою програмних засобів ведення цього реєстру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3708-4"><!-- content -->
                   <p id="u3708-2">Особисто або уповноважена особа шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3706-5"><!-- content -->
                   <p id="u3706-3">Платно: у строк, що не перевищує 1 робочого дня <span id="u3706-2">70 грн.</span></p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u3731"><!-- group -->
                 <div class="clearfix grpelem" id="pu3755-4"><!-- column -->
                  <div class="clearfix colelem" id="u3755-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3756-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3757-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3751-4"><!-- column -->
                  <div class="clearfix colelem" id="u3751-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3753-10"><!-- content -->
                   <p id="u3753-2">Документ, що посвідчує особу заявника (пред’являється), копія надається.</p>
                   <p id="u3753-4">У разі подання заяви уповноваженою особою така особа, крім документа, що посвідчує її особу, пред’являє оригінал та подає копію документа, що підтверджує її повноваження;</p>
                   <p id="u3753-6">Документи, що є підставою для внесення зміни відомостей;</p>
                   <p id="u3753-8">Документ про внесення плати за надання інформації з Державного реєстру речових прав на нерухоме майно (у разі отримання інформації з Державного реєстру речових прав на нерухоме майно).</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3754-4"><!-- content -->
                   <p id="u3754-2">Особисто або уповноважена особа шляхом звернення до центру надання адміністративних послуг або шляхом надсилання до центру надання адміністративних послуг поштою з описом вкладення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3752-4"><!-- content -->
                   <p id="u3752-2">Безолатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u3041"><!-- group -->
             <div class="clearfix grpelem" id="u3606-8"><!-- content -->
              <p id="u3606-2">3/3</p>
              <p id="u3606-4">Оберіть</p>
              <p id="u3606-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu3042"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u3055"><!-- none box -->
               <div class="popup_anchor" id="u3056popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u3056"><!-- group -->
                 <div class="clearfix grpelem" id="u3057-4"><!-- content -->
                  <p>Заява власника об’єкта нерухомого майна про заборону вчинення реєстраційної дії, заява власника об’єкта нерухомого майна про відкликання заяви про заборону вчинення реєстраційних дій</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u34609popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u34609"><!-- group -->
                 <div class="clearfix grpelem" id="u34634-4"><!-- content -->
                  <p>Державна реєстрація обтяження речового права на нерухоме майно</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u3044popup">
               <div class="ContainerGroup clearfix" id="u3044"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u3045"><!-- group -->
                 <div class="clearfix grpelem" id="pu3047-4"><!-- column -->
                  <div class="clearfix colelem" id="u3047-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3048-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u3050-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu3051-4"><!-- column -->
                  <div class="clearfix colelem" id="u3051-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3054-8"><!-- content -->
                   <p id="u3054-2">• Документ, що посвідчує особу заявника (пред’являється), копія надається.</p>
                   <p id="u3054-4">У разі подання заяви уповноваженою особою така особа, крім документа, що посвідчує її особу, пред’являє оригінал та подає копію документа, що підтверджує її повноваження;</p>
                   <p id="u3054-6">• документ, що підтверджує набуття права власності.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3049-4"><!-- content -->
                   <p id="u3049-2">Особисто або уповноважена особа шляхом звернення до центру надання адміністративних послуг</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u3052-4"><!-- content -->
                   <p id="u3052-2">Безоплатно</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u34614"><!-- group -->
                 <div class="clearfix grpelem" id="pu34638-4"><!-- column -->
                  <div class="clearfix colelem" id="u34638-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u34639-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u34640-4"><!-- content -->
                   <p>Вартість</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu34637-4"><!-- column -->
                  <div class="clearfix colelem" id="u34637-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u34642-10"><!-- content -->
                   <p id="u34642-2">• Документ, що посвідчує особу заявника (пред’являється), копія надається.</p>
                   <p id="u34642-4">• У разі подання заяви заінтересованою особою особисто така особа, крім документа, що посвідчує її особу, подає копію реєстраційного номера облікової картки платника податку згідно з Державним реєстром фізичних осіб – платників податків (крім випадків, коли фізична особа через свої релігійні або інші переконання відмовляється від прийняття реєстраційного номера облікової картки платника податку, офіційно повідомила про це відповідні органи державної влади та має відмітку в паспорті громадянина України). У разі подання заяви уповноваженою особою така особа, крім документа, що посвідчує її особу, пред’являє оригінал та подає копію документа, що підтверджує її повноваження. У разі подання заяви уповноваженою особою, яка діє від імені фізичної особи, така особа також подає копію документа, що посвідчує особу, яку вона представляє, та копію реєстраційного номера облікової картки платника податку такої фізичної особи.</p>
                   <p id="u34642-6">• Документ про сплату адміністративного збору (крім випадків, коли особа звільнена від сплати адміністративного збору);</p>
                   <p id="u34642-8">• Документи, що підтверджують виникнення або припинення обтяження речового права на нерухоме майно відповідно до ст. 27 Закону України «Про державну реєстрацію речових прав на нерухоме майно та їх обтяжень», від 1 липня 2004 року № 1952-IV.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u34643-4"><!-- content -->
                   <p id="u34643-2">Особисто або уповноважена особа шляхом звернення до центру надання адміністративних послуг</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u34641-4"><!-- content -->
                   <p id="u34641-2">Безоплатно</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u807"><!-- group -->
         <div class="clearfix grpelem" id="u30680-8"><!-- content -->
          <p id="u30680-2">2/3</p>
          <p id="u30680-4">Оберіть</p>
          <p id="u30680-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu6356"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u6623"><!-- none box -->
           <div class="popup_anchor" id="u6627popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u6627" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u6628"><!-- simple frame --></div><div class="clearfix colelem" id="u6629-4"><!-- content --><p id="u6629-2"><span class="ts-----1" id="u6629"><?php ?></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u6630popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u6630" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u6632"><!-- simple frame --></div><div class="clearfix colelem" id="u6631-4"><!-- content --><p id="u6631-2"><span class="ts-----1" id="u6631">Фізичні особи</span></p></div></a>
           </div>
           <div class="popup_anchor" id="u6624popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u6624" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u6625"><!-- simple frame --></div><div class="clearfix colelem" id="u6626-4"><!-- content --><p id="u6626-2"><span class="ts-----1" id="u6626">Юридичні особи</span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u6358popup">
           <div class="ContainerGroup clearfix" id="u6358"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u6359"><!-- group -->
             <div class="clearfix grpelem" id="u6360-8"><!-- content -->
              <p id="u6360-2">3/3</p>
              <p id="u6360-4">Оберіть</p>
              <p id="u6360-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu6361"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u6362"><!-- none box -->
               <div class="popup_anchor" id="u6363popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6363"><!-- group -->
                 <div class="clearfix grpelem" id="u6364-4"><!-- content -->
                  <p>Видача витягу з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6365popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6365"><!-- group -->
                 <div class="clearfix grpelem" id="u6366-4"><!-- content -->
                  <p>Видача документів, що містяться в реєстраційній справі відповідної юридичної особи, громадського формування, що не має статусу юридичної особи, фізичної особи – підприємця</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u6367popup">
               <div class="ContainerGroup clearfix" id="u6367"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u6368"><!-- group -->
                 <div class="clearfix grpelem" id="pu6369-4"><!-- column -->
                  <div class="clearfix colelem" id="u6369-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6376-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6373-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6374-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6371-4"><!-- column -->
                  <div class="clearfix colelem" id="u6371-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6372-4"><!-- content -->
                   <p id="u6372-2">Запит фізичної особи або юридичної особи, які бажають отримати витяг з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, або уповноваженої особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6375-12"><!-- content -->
                   <p id="u6375-2">Запит про надання витягу з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань (додаток 2 до Порядку надання відомостей з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, затвердженого наказом Міністерства юстиції України від 10.06.2016</p>
                   <p id="u6375-4">№ 1657/5, зареєстрованого у Міністерстві юстиції України 10.06.2016 за № 839/28969);</p>
                   <p id="u6375-6">&nbsp;документ, що підтверджує внесення плати за отримання відповідних відомостей.</p>
                   <p id="u6375-8">У разі подання запиту представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6375-10">Заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6370-6"><!-- content -->
                   <p id="u6370-2">1. У паперовій формі запит подається заявником особисто.</p>
                   <p id="u6370-4">2. В електронній формі запит подається через портал електронних сервісів виключно за умови реєстрації користувача на відповідному порталі.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6377-4"><!-- content -->
                   <p id="u6377-2">Платно, 0,05 прожиткового мінімуму для працездатних осіб (90 грн.)</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6378"><!-- group -->
                 <div class="clearfix grpelem" id="pu6386-4"><!-- column -->
                  <div class="clearfix colelem" id="u6386-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6387-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6380-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6379-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6382-4"><!-- column -->
                  <div class="clearfix colelem" id="u6382-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6381-4"><!-- content -->
                   <p id="u6381-2">Запит фізичної особи або юридичної особи, які бажають отримати документи з реєстраційної справи юридичних осіб, фізичних осіб – підприємців та громадських формувань, або уповноваженої особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6385-10"><!-- content -->
                   <p id="u6385-2">Запит про надання документів, що містяться в реєстраційній справі відповідної юридичної особи, громадського формування, що не має статусу юридичної особи, фізичної особи – підприємця (додаток 3 до Порядку надання відомостей з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, затвердженого наказом Міністерства юстиції України від 10.06.2016 № 1657/5, зареєстрованого у Міністерстві юстиції України 10.06.2016 за № 839/28969);</p>
                   <p id="u6385-4">&nbsp;документ, що підтверджує внесення плати за отримання відповідних відомостей.</p>
                   <p id="u6385-6">У разі подання запиту представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6385-8">Заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6383-6"><!-- content -->
                   <p id="u6383-2">1. У паперовій формі запит подається заявником особисто.</p>
                   <p id="u6383-4">2. В електронній формі запит подається через портал електронних сервісів виключно за умови реєстрації користувача на відповідному порталі</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6384-4"><!-- content -->
                   <p id="u6384-2">Платно, 0,07 прожиткового мінімуму для працездатних осіб (120 грн.)</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u6567"><!-- group -->
             <div class="clearfix grpelem" id="u6568-8"><!-- content -->
              <p id="u6568-2">3/3</p>
              <p id="u6568-4">Оберіть</p>
              <p id="u6568-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu6569"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u6613"><!-- none box -->
               <div class="popup_anchor" id="u6614popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6614"><!-- group -->
                 <div class="clearfix grpelem" id="u6615-6"><!-- content -->
                  <p>Державна реєстрація</p>
                  <p>фізичної особи підприємцем</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6618popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6618"><!-- group -->
                 <div class="clearfix grpelem" id="u6619-8"><!-- content -->
                  <p>Державна реєстрація</p>
                  <p>включення відомостей</p>
                  <p>про фізичну особу – підприємця</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6616popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6616"><!-- group -->
                 <div class="clearfix grpelem" id="u6617-8"><!-- content -->
                  <p>Державна реєстрація</p>
                  <p>змін до відомостей</p>
                  <p>про фізичну особу – підприємця</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6620popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6620"><!-- group -->
                 <div class="clearfix grpelem" id="u6621-6"><!-- content -->
                  <p>Державна реєстрація</p>
                  <p>припинення підприємницької діяльності фізичної особи – підприємця</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u6571popup">
               <div class="ContainerGroup clearfix" id="u6571"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u6602"><!-- group -->
                 <div class="clearfix grpelem" id="pu6609-4"><!-- column -->
                  <div class="clearfix colelem" id="u6609-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6611-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6610-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6605-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6604-4"><!-- column -->
                  <div class="clearfix colelem" id="u6604-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6603-4"><!-- content -->
                   <p id="u6603-2">Звернення фізичної особи, яка має намір стати підприємцем, або уповноваженої нею особи&nbsp; (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6607-8"><!-- content -->
                   <p id="u6607-2">- заяву про державну реєстрацію;</p>
                   <p id="u6607-4">- нотаріально завірену згоду батьків (усиновителів), попечителя або органу опіки і піклування, якщо зареєструватися в якості ФОП хоче особа, яка досягла віку 16 років, але не володіє повною цивільною дієздатністю;</p>
                   <p id="u6607-6">- заяву про вибір спрощеної системи оподатковування та/або реєстрації платником ПДВ (за бажанням, якщо що-небудь з цього входить у ваші плани);</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6606-6"><!-- content -->
                   <p id="u6606-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6606-4">2. В електронній формі документи подаються через портал електронних сервісі</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6608-4"><!-- content -->
                   <p id="u6608-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6572"><!-- group -->
                 <div class="clearfix grpelem" id="pu6573-4"><!-- column -->
                  <div class="clearfix colelem" id="u6573-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6577-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6575-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6579-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6578-4"><!-- column -->
                  <div class="clearfix colelem" id="u6578-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6574-4"><!-- content -->
                   <p id="u6574-2">Звернення фізичної особи – підприємця або уповноваженої нею особи&nbsp; (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6576-8"><!-- content -->
                   <p id="u6576-2">Заява про державну реєстрацію включення відомостей про фізичну особу – підприємця до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань.</p>
                   <p id="u6576-4">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання.</p>
                   <p id="u6576-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6580-6"><!-- content -->
                   <p id="u6580-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6580-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6581-4"><!-- content -->
                   <p id="u6581-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6582"><!-- group -->
                 <div class="clearfix grpelem" id="pu6583-4"><!-- column -->
                  <div class="clearfix colelem" id="u6583-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6588-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6590-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6591-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6585-4"><!-- column -->
                  <div class="clearfix colelem" id="u6585-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6584-4"><!-- content -->
                   <p id="u6584-2">Звернення фізичної особи – підприємця або уповноваженої нею особи&nbsp; (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6587-14"><!-- content -->
                   <p id="u6587-2">Заява про державну реєстрацію змін до відомостей про фізичну особу – підприємця, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;</p>
                   <p id="u6587-4">документ про сплату адміністративного збору – у випадку державної реєстрації змін відомостей про прізвище, ім’я, по батькові або місцезнаходження фізичної особи – підприємця;</p>
                   <p id="u6587-6">копія довідки про зміну реєстраційного номера облікової картки – у разі внесення змін, пов'язаних із зміною реєстраційного номера облікової картки платника податків;</p>
                   <p id="u6587-8">копія першої сторінки паспорта та сторінки з відміткою про наявність права здійснювати будь-які платежі за серією та номером паспорта – у разі внесення змін, пов'язаних із зміною серії та номера паспорта, – для фізичних осіб, які через свої релігійні переконання відмовилися від прийняття реєстраційного номера облікової картки платника податків, повідомили про це відповідний контролюючий орган і мають відмітку в паспорті про право здійснювати платежі за серією та номером паспорта.</p>
                   <p id="u6587-10">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання.</p>
                   <p id="u6587-12">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6586-6"><!-- content -->
                   <p id="u6586-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6586-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6589-4"><!-- content -->
                   <p id="u6589-2">Платно, 0,1 прожиткового мінімуму для працездатних осіб (180 грн.)</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6592"><!-- group -->
                 <div class="clearfix grpelem" id="pu6600-4"><!-- column -->
                  <div class="clearfix colelem" id="u6600-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6598-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6601-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6593-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6599-4"><!-- column -->
                  <div class="clearfix colelem" id="u6599-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6594-4"><!-- content -->
                   <p id="u6594-2">Звернення фізичної особи – підприємця або уповноваженої нею особи&nbsp; (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6597-8"><!-- content -->
                   <p id="u6597-2">Заява про державну реєстрацію припинення підприємницької діяльності фізичної особи – підприємця за її рішенням.</p>
                   <p id="u6597-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6597-6">Якщо документи подаються особисто, заявник пред’являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6595-6"><!-- content -->
                   <p id="u6595-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6595-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6596-4"><!-- content -->
                   <p id="u6596-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu34149"><!-- none box -->
              <div class="popup_anchor" id="u34152popup">
               <div class="ContainerGroup clearfix" id="u34152"><!-- none box -->
                <div class="Container invi shadow rounded-corners gs clearfix grpelem" id="u34153"><!-- group -->
                 <div class="ts-------------1 clearfix grpelem" id="u34154-30"><!-- content -->
                  <p id="u34154-4"><a class="nonblock" href="http://zakon2.rada.gov.ua/laws/show/z1500-16"><span id="u34154">Форми заяв у сфері державної реєстрації юридичних осіб, фізичних осіб - підприємців та громадських формувань затверджені наказом Міністерства юстиції від 18.11.2016 № 3268/5 “Про затвердження форм заяв у сфері державної реєстрації юридичних осіб, фізичних осіб - підприємців та громадських формувань”</span></a><span id="u34154-3">.</span></p>
                  <p id="u34154-6">Увага! Заяву можна роздрукувати бланком та заповнити “від руки”, а можна заповнити на комп’ютері.</p>
                  <p id="u34154-8">При заповненні заяв про державну реєстрацію необхідно вказати коди видів економічної діяльності якими ви плануєте займатися (вид економічної діяльності, який записаний першим, вважається основним).</p>
                  <p id="u34154-12"><a class="nonblock" href="http://www.ukrstat.gov.ua/klasf/nac_kls/op_dk009_2016.htm">Класифікатор видів економічної діяльності затверджено наказом Держспоживстандарту України від 11.10.2010 р. № 457 (із змінами, внесеними наказом Держспоживстандарту України від 29 листопада 2010 № 530)</a>.</p>
                  <p id="u34154-14">Документи можуть бути подані як у паперовій, так і в електронній формі.</p>
                  <p id="u34154-16">Якщо документи подаються в паперовій формі, то особа, що їх подає, повинна пред'явити суб’єкту державної реєстрації документ, що засвідчує особу, і доручення (якщо вона є представником особи, що реєструється ФОП, або діє від імені засновників юридичної особи ).</p>
                  <p id="u34154-18">В електронній формі документи можна подати через портал Мін’юсту.</p>
                  <p id="u34154-20">Етапи роботи із реєстраційним порталом описані на офіційному сайті Мін’юсту. Треба увійти в розділ “Електронні сервіси”, далі натиснути кнопку “Реєстрація юридичної особи або фізичної особи-підприємця”:</p>
                  <p id="u34154-22">Якщо документи подані в повному обсязі і відповідають вимогам Закону, то суб’єкт державної реєстрації протягом 24 годин (без врахування святкових і вихідних днів) з моменту одержання документів приймає рішення про реєстрацію.</p>
                  <p id="u34154-24">Якщо документи подані не в повному обсязі або не відповідають іншим вимогам, зазначеним у ч. 1 ст. 27 Закону 755, то в той же термін суб’єкт державної реєстрації приймає рішення про зупинення їхнього розгляду і надає заявникові 15 календарних днів на усунення недоліків. У випадку усунення недоліків розгляд документів відновляється, якщо ж недоліки не усунуті – приймається рішення про відмову в реєстрації.</p>
                  <p id="u34154-26">Реєстрація відбувається шляхом внесення відомостей до Єдиного державного реєстру юридичних осіб, фізичних осіб - підприємців та громадських формувань (далі – ЄДР).</p>
                  <p id="u34154-28">Увага! Свідоцтво про реєстрацію за новими правилами не видається. Замість нього формується електронна виписка на порталі електронних сервісів Мін’юсту.</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="ThumbGroup clearfix grpelem" id="u34156"><!-- none box -->
               <div class="popup_anchor" id="u34157popup">
                <div class="Thumb popup_element clearfix" id="u34157"><!-- group -->
                 <div class="size_fixed grpelem" id="u34158"><!-- custom html -->
                  
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34158"><i class="fa fa-file"></i></div>
  	  
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u6391"><!-- group -->
             <div class="clearfix grpelem" id="u6392-8"><!-- content -->
              <p id="u6392-2">3/3</p>
              <p id="u6392-4">Оберіть</p>
              <p id="u6392-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu6393"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u6395"><!-- none box -->
               <div class="popup_anchor" id="u6418popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6418"><!-- group -->
                 <div class="clearfix grpelem" id="u6419-4"><!-- content -->
                  <p>Створення юридичної особи</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6398popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6398"><!-- group -->
                 <div class="clearfix grpelem" id="u6399-4"><!-- content -->
                  <p>Включення відомостей про юридичну особу</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6414popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6414"><!-- group -->
                 <div class="clearfix grpelem" id="u6415-4"><!-- content -->
                  <p>Зміни до відомостей про юридичну особу</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6406popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6406"><!-- group -->
                 <div class="clearfix grpelem" id="u6407-4"><!-- content -->
                  <p>Перехід юридичної особи з модельного статуту</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6400popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6400"><!-- group -->
                 <div class="clearfix grpelem" id="u6401-4"><!-- content -->
                  <p>Перехід юридичної особи на модельний статут</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6408popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6408"><!-- group -->
                 <div class="clearfix grpelem" id="u6409-4"><!-- content -->
                  <p>Виділ юридичної особи</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6420popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6420"><!-- group -->
                 <div class="clearfix grpelem" id="u6421-6"><!-- content -->
                  <p>Припинення</p>
                  <p>юридичної особи</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6402popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6402"><!-- group -->
                 <div class="clearfix grpelem" id="u6403-4"><!-- content -->
                  <p>Відміна рішення про припинення</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6422popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6422"><!-- group -->
                 <div class="clearfix grpelem" id="u6423-4"><!-- content -->
                  <p>Зміни складу комісії з припинення</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6404popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6404"><!-- group -->
                 <div class="clearfix grpelem" id="u6405-4"><!-- content -->
                  <p>Ліквідація юридичної особи</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6410popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6410"><!-- group -->
                 <div class="clearfix grpelem" id="u6411-4"><!-- content -->
                  <p>&nbsp;Реорганізація юридичної особи</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6396popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6396"><!-- group -->
                 <div class="clearfix grpelem" id="u6397-4"><!-- content -->
                  <p>Створення відокремленого підрозділу</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6416popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6416"><!-- group -->
                 <div class="clearfix grpelem" id="u6417-5"><!-- content -->
                  <p id="u6417-3"><span id="u6417">Зміни про </span><span id="u6417-2">відокремлений підрозділ</span></p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u6412popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u6412"><!-- group -->
                 <div class="clearfix grpelem" id="u6413-5"><!-- content -->
                  <p id="u6413-3"><span id="u6413">Припинення </span><span id="u6413-2">відокремленого підрозділу</span></p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u6424popup">
               <div class="ContainerGroup clearfix" id="u6424"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u6425"><!-- group -->
                 <div class="clearfix grpelem" id="pu6430-4"><!-- column -->
                  <div class="clearfix colelem" id="u6430-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6433-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6427-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6434-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6429-4"><!-- column -->
                  <div class="clearfix colelem" id="u6429-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6426-4"><!-- content -->
                   <p id="u6426-2">Звернення засновника (засновників),або уповноваженої ним (ними) особи, або керівника державного органу, органу місцевого самоврядування, або уповноваженої ними особи(далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u21025-10"><!-- content -->
                   <p id="u21025-2">- заяву про реєстрацію за установленою формою;</p>
                   <p id="u21025-4">- установчий документ юридичної особи – у разі створення юридичної особи на підставі власного установчого документа (статут, установчий договір і так далі в залежності від обраної організаційно-правової форми);</p>
                   <p id="u21025-6">- документ, що підтверджує реєстрацію іноземної юридичної особи, що виступає засновником, у відповідній країні;</p>
                   <p id="u21025-8">- заяву про вибір спрощеної системи оподатковування та/або реєстрації платником ПДВ та/або включенні до Реєстру неприбуткових організацій і установ (за бажанням, якщо що-небудь з цього входить у ваші плани).</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6431-6"><!-- content -->
                   <p id="u6431-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6431-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6432-4"><!-- content -->
                   <p id="u6432-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6535"><!-- group -->
                 <div class="clearfix grpelem" id="pu6539-4"><!-- column -->
                  <div class="clearfix colelem" id="u6539-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6537-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6536-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6538-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6544-4"><!-- column -->
                  <div class="clearfix colelem" id="u6544-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6540-4"><!-- content -->
                   <p id="u6540-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6543-8"><!-- content -->
                   <p id="u6543-2">Заява про державну реєстрацію включення відомостей про юридичну особу до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань.</p>
                   <p id="u6543-4">Представником юридичної особи додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6543-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6541-6"><!-- content -->
                   <p id="u6541-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6541-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6542-4"><!-- content -->
                   <p id="u6542-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6445"><!-- group -->
                 <div class="clearfix grpelem" id="pu6453-4"><!-- column -->
                  <div class="clearfix colelem" id="u6453-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6451-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6452-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6447-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6450-4"><!-- column -->
                  <div class="clearfix colelem" id="u6450-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6449-4"><!-- content -->
                   <p id="u6449-2">Звернення уповноваженого представника&nbsp; юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 colelem" id="u18977"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
1. Для державної реєстрації змін до відомостей про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, у тому числі змін до установчих документів юридичної особи(крім місцевої ради, виконавчого комітету місцевої ради, виконавчого органу місцевої ради),подаються:
заява про державну реєстрацію змін до відомостей про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;
примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про зміни, що вносяться до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, крім внесення змін до інформації про кінцевих бенефіціарних власників (контролерів) юридичної особи, у тому числі кінцевих бенефіціарних власників (контролерів) її засновника, якщо засновник – юридична особа, про місцезнаходження та про здійснення зв’язку з юридичною особою;
документ, що підтверджує реєстрацію іноземної особи в країні її місцезнаходження (витяг із торговельного, банківського, судового реєстру тощо), – у разі змін, пов’язаних із входженням до складу засновників юридичної особи іноземної юридичної особи;
документ про сплату адміністративного збору, крім внесення змін до інформації про здійснення зв’язку з юридичною особою;
установчий документ юридичної особи в новій редакції – у разі внесення змін, що містяться в установчому документі;
примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи;
примірник оригіналу (нотаріально засвідчена копія) передавального акта – у разі внесення змін, пов’язаних із внесенням даних про юридичну особу, правонаступником якої є зареєстрована юридична особа;
примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про вихід із складу засновників (учасників), та/або заява фізичної особи про вихід із складу засновників (учасників), та/або договору, іншого документа про перехід чи передачу частки засновника (учасника) у статутному (складеному) капіталі (пайовому фонді) юридичної особи, та/або рішення уповноваженого органу управління юридичної особи про примусове виключення із складу засновників (учасників) юридичної особи або ксерокопія свідоцтва про смерть фізичної особи, судове рішення про визнання фізичної особи безвісно відсутньою – у разі внесення змін, пов’язаних із зміною складу засновників (учасників) юридичної особи.
2. Для державної реєстрації внесення змін до відомостей про юридичну особу – місцеву раду, виконавчий комітет місцевої ради, виконавчий орган місцевої ради подаються:
заява про державну реєстрацію змін до відомостей про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;
акт сільського (селищного, міського) голови про призначення керівника – у разі внесення змін про керівника виконавчого органу місцевої ради (крім виконавчого комітету).
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6454-6"><!-- content -->
                   <p id="u6454-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6454-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6448-4"><!-- content -->
                   <p id="u6448-2">Платно, 0,3 прожиткового мінімуму для працездатних осіб (530 грн.)</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6505"><!-- group -->
                 <div class="clearfix grpelem" id="pu6508-4"><!-- column -->
                  <div class="clearfix colelem" id="u6508-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6512-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6510-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6513-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6509-4"><!-- column -->
                  <div class="clearfix colelem" id="u6509-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6514-4"><!-- content -->
                   <p id="u6514-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 colelem" id="u18972"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
Заява про державну реєстрацію переходу з модельного статуту на діяльність на підставі власного установчого документа;
примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про перехід на діяльність на підставі власного установчого документа та затвердження установчого документа;
установчий документ юридичної особи;
примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6506-6"><!-- content -->
                   <p id="u6506-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6506-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6507-4"><!-- content -->
                   <p id="u6507-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6435"><!-- group -->
                 <div class="clearfix grpelem" id="pu6444-4"><!-- column -->
                  <div class="clearfix colelem" id="u6444-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6436-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6441-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6439-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6438-4"><!-- column -->
                  <div class="clearfix colelem" id="u6438-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6437-4"><!-- content -->
                   <p id="u6437-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6440-12"><!-- content -->
                   <p id="u6440-2">Заява про державну реєстрацію переходу з власного установчого документа на діяльність на підставі модельного статуту;</p>
                   <p id="u6440-4">примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про перехід на діяльність на підставі модельного статуту;</p>
                   <p id="u6440-6">примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.</p>
                   <p id="u6440-8">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6440-10">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6442-6"><!-- content -->
                   <p id="u6442-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6442-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6443-4"><!-- content -->
                   <p id="u6443-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6525"><!-- group -->
                 <div class="clearfix grpelem" id="pu6526-4"><!-- column -->
                  <div class="clearfix colelem" id="u6526-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6530-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6532-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6534-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6528-4"><!-- column -->
                  <div class="clearfix colelem" id="u6528-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6531-4"><!-- content -->
                   <p id="u6531-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6527-10"><!-- content -->
                   <p id="u6527-2">Примірник оригіналу (нотаріально засвідчена копія) рішення учасників або відповідного органу юридичної особи про виділ юридичної особи;</p>
                   <p id="u6527-4">примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.</p>
                   <p id="u6527-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6527-8">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6529-6"><!-- content -->
                   <p id="u6529-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6529-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6533-4"><!-- content -->
                   <p id="u6533-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6485"><!-- group -->
                 <div class="clearfix grpelem" id="pu6490-4"><!-- column -->
                  <div class="clearfix colelem" id="u6490-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6487-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6494-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6492-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6488-4"><!-- column -->
                  <div class="clearfix colelem" id="u6488-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6493-4"><!-- content -->
                   <p id="u6493-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 colelem" id="u18987"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
1. Для державної реєстрації рішення про припинення юридичної особи подається:
примірник оригіналу (нотаріально засвідчена копія) рішення учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – рішення відповідного державного органу про припинення юридичної особи;
примірник оригіналу (нотаріально засвідчена копія) документа, яким затверджено персональний склад комісії з припинення (комісії з реорганізації, ліквідаційної комісії) або ліквідатора, реєстраційні номери облікових карток платників податків (або відомості про серію та номер паспорта – для фізичних осіб, які через свої релігійні переконання відмовилися від прийняття реєстраційного номера облікової картки платника податків та повідомили про це відповідний контролюючий орган і мають відмітку в паспорті про право здійснювати платежі за серією та номером паспорта), строк заявлення кредиторами своїх вимог, – у разі відсутності зазначених відомостей у рішенні учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – у рішенні відповідного державного органу про припинення юридичної особи;
примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.
2. Для державної реєстрації рішення про припинення банку у зв’язку з прийняттям рішення про відкликання банківської ліцензії та ліквідацію банку Фондом гарантування вкладів фізичних осіб подаються:
копія рішення Національного банку України про відкликання банківської ліцензії та ліквідацію банку;
копія рішення Фонду гарантування вкладів фізичних осіб про призначення уповноваженої особи Фонду.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6491-6"><!-- content -->
                   <p id="u6491-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6491-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6486-4"><!-- content -->
                   <p id="u6486-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6495"><!-- group -->
                 <div class="clearfix grpelem" id="pu6497-4"><!-- column -->
                  <div class="clearfix colelem" id="u6497-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6496-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6500-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6502-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6498-4"><!-- column -->
                  <div class="clearfix colelem" id="u6498-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6503-4"><!-- content -->
                   <p id="u6503-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6504-10"><!-- content -->
                   <p id="u6504-2">Примірник оригіналу (нотаріально засвідчена копія) рішення учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – рішення відповідного державного органу про відміну рішення про припинення юридичної особи;</p>
                   <p id="u6504-4">примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.</p>
                   <p id="u6504-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6504-8">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6499-6"><!-- content -->
                   <p id="u6499-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6499-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6501-4"><!-- content -->
                   <p id="u6501-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6465"><!-- group -->
                 <div class="clearfix grpelem" id="pu6466-4"><!-- column -->
                  <div class="clearfix colelem" id="u6466-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6470-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6469-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6471-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6474-4"><!-- column -->
                  <div class="clearfix colelem" id="u6474-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6468-4"><!-- content -->
                   <p id="u6468-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6467-10"><!-- content -->
                   <p id="u6467-2">Примірник оригіналу (нотаріально засвідчена копія) рішення учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – рішення відповідного державного органу про зміни;</p>
                   <p id="u6467-4">примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.</p>
                   <p id="u6467-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6467-8">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6472-6"><!-- content -->
                   <p id="u6472-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6472-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6473-4"><!-- content -->
                   <p id="u6473-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6545"><!-- group -->
                 <div class="clearfix grpelem" id="pu6546-4"><!-- column -->
                  <div class="clearfix colelem" id="u6546-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6548-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6553-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6549-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6550-4"><!-- column -->
                  <div class="clearfix colelem" id="u6550-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6552-4"><!-- content -->
                   <p id="u6552-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 colelem" id="u18992"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
1. Для державної реєстрації припинення юридичної особи в результаті її ліквідації (крім місцевої ради, виконавчого комітету місцевої ради, виконавчого органу місцевої ради) подаються:
заява про державну реєстрацію припинення юридичної особи в результаті її ліквідації;
довідка архівної установи про прийняття документів, що відповідно до закону підлягають довгостроковому зберіганню.
2. Для державної реєстрації припиненняюридичної 
особи – місцевої ради, виконавчого комітету місцевої ради, виконавчого органу місцевої ради подається заява про державну реєстрацію припинення юридичної особи в результаті її ліквідації.
3. Для державної реєстрації припинення банку у зв’язку з прийняттям рішення про відкликання банківської ліцензії та ліквідацію банку подається рішення Фонду гарантування вкладів фізичних осіб про затвердження звіту про завершення ліквідації банку.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6551-6"><!-- content -->
                   <p id="u6551-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6551-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6554-4"><!-- content -->
                   <p id="u6554-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6515"><!-- group -->
                 <div class="clearfix grpelem" id="pu6518-4"><!-- column -->
                  <div class="clearfix colelem" id="u6518-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6516-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6524-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6520-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6519-4"><!-- column -->
                  <div class="clearfix colelem" id="u6519-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6523-4"><!-- content -->
                   <p id="u6523-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 colelem" id="u18997"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
Заява про державну реєстрацію припинення юридичної особи в результаті її реорганізації;
примірник оригіналу (нотаріально засвідчена копія) розподільчого балансу – у разі припинення юридичної особи в результаті поділу;
примірник оригіналу (нотаріально засвідчена копія) передавального акта– у разі припинення юридичної особи в результаті перетворення, злиття або приєднання;
довідка архівної установи про прийняття документів, що відповідно до закону підлягають довгостроковому зберіганню, – у разі припинення юридичної особи в результаті поділу, злиття або приєднання;
документи для державної реєстрації створення юридичної особи, визначені частиною першою статті 17 Закону України «Про державну реєстрацію юридичних осіб, фізичних осіб – підприємців та громадських формувань», – у разі припинення юридичної особи в результаті перетворення;
документи для державної реєстрації змін до відомостей про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, визначені частиною четвертою статті 17 Закону України «Про державну реєстрацію юридичних осіб, фізичних осіб – підприємців та громадських формувань», – у разі припинення юридичної особи в результаті приєднання.
Державна реєстрація при реорганізації органів місцевого самоврядування як юридичних осіб після добровільного об’єднання територіальних громад здійснюється з урахуванням особливостей, передбачених Законом України «Про добровільне об’єднання територіальних громад».
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6522-6"><!-- content -->
                   <p id="u6522-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6522-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6521-4"><!-- content -->
                   <p id="u6521-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6555"><!-- group -->
                 <div class="clearfix grpelem" id="pu6559-4"><!-- column -->
                  <div class="clearfix colelem" id="u6559-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6560-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6563-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6562-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6558-4"><!-- column -->
                  <div class="clearfix colelem" id="u6558-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6561-4"><!-- content -->
                   <p id="u6561-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6564-12"><!-- content -->
                   <p id="u6564-2">Заява про державну реєстрацію створення відокремленого підрозділу юридичної особи;</p>
                   <p id="u6564-4">примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про створення відокремленого підрозділу;</p>
                   <p id="u6564-6">примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує повноваження представника засновника (учасника) юридичної особи – у разі участі представника засновника (учасника) юридичної особи у прийнятті рішення уповноваженим органом управління юридичної особи.</p>
                   <p id="u6564-8">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6564-10">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6557-6"><!-- content -->
                   <p id="u6557-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6557-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6556-4"><!-- content -->
                   <p id="u6556-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6475"><!-- group -->
                 <div class="clearfix grpelem" id="pu6482-4"><!-- column -->
                  <div class="clearfix colelem" id="u6482-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6483-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6477-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6481-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6478-4"><!-- column -->
                  <div class="clearfix colelem" id="u6478-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6479-4"><!-- content -->
                   <p id="u6479-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6476-10"><!-- content -->
                   <p id="u6476-2">Заява про державну реєстрацію змін до відомостей про відокремлений підрозділ юридичної особи, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних</p>
                   <p id="u6476-4">осіб – підприємців та громадських формувань.</p>
                   <p id="u6476-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6476-8">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6484-6"><!-- content -->
                   <p id="u6484-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6484-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6480-4"><!-- content -->
                   <p id="u6480-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u6455"><!-- group -->
                 <div class="clearfix grpelem" id="pu6462-4"><!-- column -->
                  <div class="clearfix colelem" id="u6462-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u6459-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6456-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u6464-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu6463-4"><!-- column -->
                  <div class="clearfix colelem" id="u6463-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6461-4"><!-- content -->
                   <p id="u6461-2">Звернення уповноваженого представника юридичної особи (далі – заявник)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6458-8"><!-- content -->
                   <p id="u6458-2">Заява про державну реєстрацію припинення відокремленого підрозділу.</p>
                   <p id="u6458-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u6458-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6457-6"><!-- content -->
                   <p id="u6457-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u6457-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u6460-4"><!-- content -->
                   <p id="u6460-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu34117"><!-- none box -->
              <div class="popup_anchor" id="u34122popup">
               <div class="ContainerGroup clearfix" id="u34122"><!-- none box -->
                <div class="Container invi gs shadow clearfix grpelem" id="u34123"><!-- group -->
                 <div class="ts-------------1 clearfix grpelem" id="u34124-30"><!-- content -->
                  <p id="u34124-4"><a class="nonblock" href="http://zakon2.rada.gov.ua/laws/show/z1500-16"><span id="u34124">Форми заяв у сфері державної реєстрації юридичних осіб, фізичних осіб - підприємців та громадських формувань затверджені наказом Міністерства юстиції від 18.11.2016 № 3268/5 “Про затвердження форм заяв у сфері державної реєстрації юридичних осіб, фізичних осіб - підприємців та громадських формувань”</span></a><span id="u34124-3">.</span></p>
                  <p id="u34124-6">Увага! Заяву можна роздрукувати бланком та заповнити “від руки”, а можна заповнити на комп’ютері.</p>
                  <p id="u34124-8">При заповненні заяв про державну реєстрацію необхідно вказати коди видів економічної діяльності якими ви плануєте займатися (вид економічної діяльності, який записаний першим, вважається основним).</p>
                  <p id="u34124-12"><a class="nonblock" href="http://www.ukrstat.gov.ua/klasf/nac_kls/op_dk009_2016.htm">Класифікатор видів економічної діяльності затверджено наказом Держспоживстандарту України від 11.10.2010 р. № 457 (із змінами, внесеними наказом Держспоживстандарту України від 29 листопада 2010 № 530)</a>.</p>
                  <p id="u34124-14">Документи можуть бути подані як у паперовій, так і в електронній формі.</p>
                  <p id="u34124-16">Якщо документи подаються в паперовій формі, то особа, що їх подає, повинна пред'явити суб’єкту державної реєстрації документ, що засвідчує особу, і доручення (якщо вона є представником особи, що реєструється ФОП, або діє від імені засновників юридичної особи ).</p>
                  <p id="u34124-18">В електронній формі документи можна подати через портал Мін’юсту.</p>
                  <p id="u34124-20">Етапи роботи із реєстраційним порталом описані на офіційному сайті Мін’юсту. Треба увійти в розділ “Електронні сервіси”, далі натиснути кнопку “Реєстрація юридичної особи або фізичної особи-підприємця”:</p>
                  <p id="u34124-22">Якщо документи подані в повному обсязі і відповідають вимогам Закону, то суб’єкт державної реєстрації протягом 24 годин (без врахування святкових і вихідних днів) з моменту одержання документів приймає рішення про реєстрацію.</p>
                  <p id="u34124-24">Якщо документи подані не в повному обсязі або не відповідають іншим вимогам, зазначеним у ч. 1 ст. 27 Закону 755, то в той же термін суб’єкт державної реєстрації приймає рішення про зупинення їхнього розгляду і надає заявникові 15 календарних днів на усунення недоліків. У випадку усунення недоліків розгляд документів відновляється, якщо ж недоліки не усунуті – приймається рішення про відмову в реєстрації.</p>
                  <p id="u34124-26">Реєстрація відбувається шляхом внесення відомостей до Єдиного державного реєстру юридичних осіб, фізичних осіб - підприємців та громадських формувань (далі – ЄДР).</p>
                  <p id="u34124-28">Увага! Свідоцтво про реєстрацію за новими правилами не видається. Замість нього формується електронна виписка на порталі електронних сервісів Мін’юсту.</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="ThumbGroup clearfix grpelem" id="u34119"><!-- none box -->
               <div class="popup_anchor" id="u34120popup">
                <div class="Thumb popup_element clearfix" id="u34120"><!-- group -->
                 <div class="size_fixed grpelem" id="u34121"><!-- custom html -->
                  
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34121"><i class="fa fa-file"></i></div>
  	  
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u813"><!-- group -->
         <div class="clearfix grpelem" id="u30683-8"><!-- content -->
          <p id="u30683-2">2/3</p>
          <p id="u30683-4">Оберіть</p>
          <p id="u30683-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu4812"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u4894"><!-- none box -->
           <div class="popup_anchor" id="u4895popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u4895" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u4896"><!-- simple frame --></div><div class="clearfix colelem" id="u4897-4"><!-- content --><p id="u4897-2"><span class="ts-----1" id="u4897">Довідка</span></p></div></a>
           </div>
           <div class="popup_anchor" id="u4904popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u4904" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u4906"><!-- simple frame --></div><div class="clearfix colelem" id="u4905-4"><!-- content --><p id="u4905-2"><span class="ts-----1" id="u4905">Реєстрація</span></p></div></a>
           </div>
           <div class="popup_anchor" id="u4901popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u4901" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u4903"><!-- simple frame --></div><div class="clearfix colelem" id="u4902-4"><!-- content --><p id="u4902-2"><span class="ts-----1" id="u4902">Зняття</span></p></div></a>
           </div>
           <div class="popup_anchor" id="u4898popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u4898" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u4900"><!-- simple frame --></div><div class="clearfix colelem" id="u4899-4"><!-- content --><p id="u4899-2"><span class="ts-----1" id="u4899">Перейменування</span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u4814popup">
           <div class="ContainerGroup clearfix" id="u4814"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u4815"><!-- group -->
             <div class="clearfix grpelem" id="u4834-8"><!-- content -->
              <p id="u4834-2">3/3</p>
              <p id="u4834-4">Оберіть</p>
              <p id="u4834-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu4816"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u4830"><!-- none box -->
               <div class="popup_anchor" id="u4831popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u4831"><!-- group -->
                 <div class="clearfix grpelem" id="u4832-4"><!-- content -->
                  <p>ВИДАЧА ДОВІДКИ ПРО РЕЄСТРАЦІЮ МІСЦЯ ПРОЖИВАННЯ/ПЕРЕБУВАННЯ, ЗНЯТТЯ З РЕЄСТРАЦІЇ МІСЦЯ ПРОЖИВАННЯ ОСОБИ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u4817popup">
               <div class="ContainerGroup clearfix" id="u4817"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u4818"><!-- group -->
                 <div class="clearfix grpelem" id="pu4819-4"><!-- column -->
                  <div class="clearfix colelem" id="u4819-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u4825-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u4827-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u4821-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu4824-4"><!-- column -->
                  <div class="clearfix colelem" id="u4824-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4822-4"><!-- content -->
                   <p id="u4822-2">Заява особи або її представника.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4826-18"><!-- content -->
                   <p id="u4826-2">Заява особи або її представника.</p>
                   <p id="u4826-4">У разі подання запиту законним представником особи додатково подаються:</p>
                   <p id="u4826-6">документ, що посвідчує особу законного представника;</p>
                   <p id="u4826-8">документ, що підтверджує повноваження законного представника.</p>
                   <p id="u4826-10">У разі подання запиту представником особи додатково подаються:</p>
                   <p id="u4826-12">документ, що посвідчує особу представника;</p>
                   <p id="u4826-14">документ, що підтверджує повноваження представника;</p>
                   <p id="u4826-16">письмова згода особи, щодо якої запитується інформація.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4820-4"><!-- content -->
                   <p id="u4820-2">Особа або її представник для одержання адміністративної послуги з отримання довідки реєстрації або зняття з реєстрації місця проживання/перебування звертається до ЦНАП відповідно до місця проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4823-4"><!-- content -->
                   <p id="u4823-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u35669" href="http://api.berdychiv.com.ua/%D0%B2%D0%B8%D0%B4%D0%B0%D1%87%D0%B0-%D0%B4%D0%BE%D0%B2%D1%96%D0%B4%D0%BA%D0%B8/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35669"><i class="fa fa-share-square"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u4835"><!-- group -->
             <div class="clearfix grpelem" id="u4836-8"><!-- content -->
              <p id="u4836-2">3/3</p>
              <p id="u4836-4">Оберіть</p>
              <p id="u4836-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu4837"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u4851"><!-- none box -->
               <div class="popup_anchor" id="u4852popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u4852"><!-- group -->
                 <div class="clearfix grpelem" id="u4853-4"><!-- content -->
                  <p>РЕЄСТРАЦІЯ МІСЦЯ ПРОЖИВАННЯ/ПЕРЕБУВАННЯ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u4840popup">
               <div class="ContainerGroup clearfix" id="u4840"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u4841"><!-- group -->
                 <div class="clearfix grpelem" id="pu4842-4"><!-- column -->
                  <div class="clearfix colelem" id="u4842-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u4845-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u4848-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u4843-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu4844-4"><!-- column -->
                  <div class="clearfix colelem" id="u4844-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4847-4"><!-- content -->
                   <p id="u4847-2">Заява особи або її представника.</p>
                  </div>
                  <div class="Text-1 colelem" id="u19002"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
<p>1) письмова заява про реєстрацію місця проживання особи (<a href="http://zakon2.rada.gov.ua/laws/show/207-2016-%D0%BF" target="_blank">зразок, наведений у додатку 6,7 або 8до Правил</a>);</p>
<p>2) документ, до якого вносяться відомості про місце проживання. Якщо дитина не досягла 16 років, подається свідоцтво про народження. Реєстрація місця проживання дітей, які є іноземцями чи особами без громадянства, здійснюється за умови внесення даних про дітей до посвідки на постійне або тимчасове проживання їх батьків та копії свідоцтва про народження. Документи, видані компетентними органами іноземних держав, підлягають легалізації в установленому порядку, якщо інше не передбачено міжнародними договорами;</p>
<p>3) квитанція про сплату адміністративного збору (у разі реєстрації місця проживання одночасно із зняттям з попереднього місця проживання адміністративний збір стягується лише за одну послугу);</p>
<p>4) документи, що підтверджують:</p>
<p>право на проживання в житлі - ордер, свідоцтво про право власності, договір найму (піднайму, оренди), рішення суду, яке набрало законної сили, про надання особі права на вселення до житлового приміщення, визнання за особою права користування житловим приміщенням або права власності на нього, права на реєстрацію місця проживання або інші документи.</p>
<p>У разі відсутності зазначених документів реєстрація місця проживання особи здійснюється за згодою власника/співвласників житла, наймача та членів його сім&rsquo;ї (зазначені документи або згода не вимагаються при реєстрації місця проживання неповнолітніх дітей за адресою реєстрації місця проживання батьків/одного з батьків або законного представника/представників);</p>
<p>право на перебування або взяття на облік у спеціалізованій соціальній установі, закладі соціального обслуговування та соціального захисту особи, - довідка про прийняття на обслуговування в спеціалізованій соціальній установі, закладі соціального обслуговування та соціального захисту особи (зразок, наведений у додатку 9 до Правил), копія посвідчення про взяття на облік бездомної особи (для осіб, які перебувають на обліку у цих установах);</p>
<p>проходження служби у військовій частині, адреса якої зазначається під час реєстрації, - довідка про проходження служби у військовій частині, видана командиром військової частини (зразок, наведений у додатку 10 до Правил для військовослужбовців, крім військовослужбовців строкової служби) ;</p>
<p>5) військовий квиток або посвідчення про приписку (для громадян, які підлягають взяттю на військовий облік або перебувають на військовому обліку);</p>
<p>6) заяву про зняття з реєстрації місця проживання особи (зразок, наведений у додатку 11 до Правил), у разі здійснення реєстрації місця проживання одночасно із зняттям із реєстрації попереднього місця проживання .</p>
<p>У разі подання заяви представником особи, крім зазначених документів подаються:</p>
<p>документ, що посвідчує особу представника;</p>
<p>документ, що підтверджує повноваження особи як представника, крім випадків, коли заява подається законними представниками малолітньої дитини &ndash; батьками (усиновлювачами), реєстрація місця проживання особи за заявою законного представника здійснюється за згодою інших законних представників.</p>
<p>У разі проживання батьків за різними адресами місце&nbsp; проживання дитини, яка не досягла 14 років, реєструється разом з одним&nbsp; із батьків за письмовою згодою другого з батьків у присутності особи, яка приймає заяву, або на підставі засвідченої в установленому порядку письмової згоди другого з батьків (крім випадків, коли місце проживання дитини визначено відповідним рішенням суду або рішенням органу опіки та піклування).</p>
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4850-4"><!-- content -->
                   <p id="u4850-2">Особа або її представник для одержання адміністративної послуги з оформлення реєстрації місця проживання/перебування звертається до центру надання адміністративних послуг виконавчого комітету Бердичівської міської ради.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4846-10"><!-- content -->
                   <p id="u4846-2">Платно (крім осіб, що звернулись за захистом в Україні у випадку реєстрації місця перебування):</p>
                   <p id="u4846-5">у разі звернення особи протягом 30 днів з моменту зняття з реєстрації – <span id="u4846-4">15,00 грн.</span></p>
                   <p id="u4846-8">у разі звернення особи з порушенням зазначеного терміну – <span id="u4846-7">45,00 грн.</span></p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u4873"><!-- group -->
             <div class="clearfix grpelem" id="u4874-8"><!-- content -->
              <p id="u4874-2">3/3</p>
              <p id="u4874-4">Оберіть</p>
              <p id="u4874-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu4875"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u4877"><!-- none box -->
               <div class="popup_anchor" id="u4878popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u4878"><!-- group -->
                 <div class="clearfix grpelem" id="u4879-4"><!-- content -->
                  <p>ЗНЯТТЯ З РЕЄСТРАЦІЇ МІСЦЯ ПРОЖИВАННЯ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u4880popup">
               <div class="ContainerGroup clearfix" id="u4880"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u4881"><!-- group -->
                 <div class="clearfix grpelem" id="pu4886-4"><!-- column -->
                  <div class="clearfix colelem" id="u4886-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u4885-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u4883-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u4890-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu4882-4"><!-- column -->
                  <div class="clearfix colelem" id="u4882-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4889-4"><!-- content -->
                   <p id="u4889-2">Документ, до якого вносяться відомості про місце проживання.</p>
                  </div>
                  <div class="Text-1 colelem" id="u19007"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
<p>1) письмова заява про зняття з реєстрації місця проживання особи (<a href="http://zakon2.rada.gov.ua/laws/show/207-2016-%D0%BF" target="_blank">зразок, наведений у додатку 11 до Правил</a>);</p>
<p>2) рішення суду, яке набрало законної сили, про позбавлення права власності на житлове приміщення або права користування житловим приміщенням, про виселення, про зняття з реєстрації місця проживання особи, про визнання особи безвісно відсутньою або оголошення її померлою;</p>
<p>3) свідоцтво про смерть;</p>
<p>4) повідомлення територіального органу або підрозділу ДМС із зазначенням відповідних реквізитів паспорта померлої особи або документа про смерть, виданого компетентним органом іноземної держави, легалізованого в установленому порядку;</p>
<p>інших документів, які свідчать про припинення:</p>
<p>підстав для перебування на території<br />України іноземців та осіб без громадянства (інформація територіального органу ДМС або територіального підрозділу ДМС, на території обслуговування якого зареєстровано місце проживання особи, про закінчення строку дії посвідки на тимчасове проживання або копія рішення про скасування посвідки на тимчасове проживання чи скасування дозволу на імміграцію та посвідки на постійне проживання в Україні);</p>
<p>підстав для проживання бездомної особи у спеціалізованій установі, закладі соціального обслуговування та соціального захисту (письмове повідомлення соціальної установи, закладу соціального обслуговування та соціального захисту);</p>
<p>підстав на право користування житловим приміщенням (закінчення строку дії договору оренди, найму, піднайму житлового приміщення, строку навчання в навчальному закладі (у разі реєстрації місця проживання в гуртожитку навчального закладу на час навчання), відчуження житла та інших визначених законодавством документів);<br />Зняття з реєстрації місця проживання на підставах, визначених в абзацах восьмому та дев&rsquo;ятому цього пункту, здійснюється за клопотанням уповноваженої особи спеціалізованої соціальної установи, закладу соціального обслуговування та соціального захисту або за заявою власника/наймача житла або їх представників.</p>
<p>Зняття з реєстрації місця проживання дітей-сиріт та дітей, позбавлених батьківського піклування, осіб, стосовно яких встановлено опіку та піклування, здійснюється за погодженням з органами опіки та піклування.</p>
<p>Разом із заявою особа подає:</p>
<p>- документ, до якого вносяться відомості про зняття з реєстрації місця проживання. Якщо дитина не досягла 16 років, подається свідоцтво про народження;<br />- квитанцію про сплату адміністративного збору;<br />- військовий квиток або посвідчення про приписку (для громадян, які підлягають взяттю на військовий облік або перебувають на військовому обліку);</p>
<p>У разі подання заяви представником особи, крім зазначених документів, додатково подаються:<br />- документ, що посвідчує особу представника;<br />- документ, що підтверджує повноваження особи як представника, крім випадків, коли заява подається законними представниками малолітньої дитини &ndash; батьками (усиновлювачами).</p>
<p>Зняття з реєстрації місця проживання особи за заявою законного представника здійснюється за згодою інших законних представників.</p>
<p>У разі реєстрації місця проживання батьків за різними адресами зняття з реєстрації місця проживання дитини, яка не досягла 14 років, разом одним із батьків здійснюється за письмовою згодою другого з батьків у присутності особи, яка приймає заяву, або на підставі засвідченої у встановленому порядку письмової згоди другого з батьків ( крім випадків, коли місце проживання дитини визначено відповідним рішенням суду або рішенням органу опіки та піклування).</p>
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4887-4"><!-- content -->
                   <p id="u4887-2">Особа або її представник для одержання адміністративної послуги з оформлення зняття з реєстрації місця проживання звертається до центру надання адміністративних послуг виконавчого комітету Бердичівської міської ради.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u4888-5"><!-- content -->
                   <p id="u4888-3">Платно – <span id="u4888-2">15,00 грн.</span></p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u4855"><!-- group -->
             <div class="clearfix grpelem" id="u4872-8"><!-- content -->
              <p id="u4872-2">3/3</p>
              <p id="u4872-4">Оберіть</p>
              <p id="u4872-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu5135"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u5136"><!-- none box -->
               <div class="popup_anchor" id="u5137popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u5137"><!-- group -->
                 <div class="clearfix grpelem" id="u5138-4"><!-- content -->
                  <p>ПЕРЕЙМЕНУВАННЯ ВУЛИЦЬ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u5140popup">
               <div class="ContainerGroup clearfix" id="u5140"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u5141"><!-- group -->
                 <div class="clearfix grpelem" id="pu5148-4"><!-- column -->
                  <div class="clearfix colelem" id="u5148-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u5145-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u5147-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u5144-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu5142-4"><!-- column -->
                  <div class="clearfix colelem" id="u5142-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u5143-4"><!-- content -->
                   <p id="u5143-2">Документ, до якого вносяться відомості про місце проживання.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u5146-10"><!-- content -->
                   <p id="u5146-2">1) документ, до якого вносяться відомості про місце проживання. Документи, видані компетентними органами іноземних держав, підлягають легалізації в установленому порядку, якщо інше не передбачено міжнародними договорами;</p>
                   <p id="u5146-4">У разі звернення&nbsp; представником особи, крім зазначених документів подаються:</p>
                   <p id="u5146-6">документ, що посвідчує особу представника;</p>
                   <p id="u5146-8">документ, що підтверджує повноваження особи як представника, крім випадків, коли заява подається законними представниками малолітньої дитини – батьками (усиновлювачами), реєстрація місця проживання особи за заявою законного представника здійснюється за згодою інших законних представників.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u5149-4"><!-- content -->
                   <p id="u5149-2">Особа або її представник для одержання адміністративної послуги звертається до центру надання адміністративних послуг виконавчого комітету Бердичівської міської ради.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u5150-4"><!-- content -->
                   <p id="u5150-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u819"><!-- group -->
         <div class="clearfix grpelem" id="u30686-8"><!-- content -->
          <p id="u30686-2">2/3</p>
          <p id="u30686-4">Оберіть</p>
          <p id="u30686-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu7203"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u7371"><!-- none box -->
           <div class="popup_anchor" id="u7372popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u7372" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u7374"><!-- simple frame --></div><div class="clearfix colelem" id="u7373-4"><!-- content --><p id="u7373-2"><span class="ts-----1" id="u7373">Посвідчення</span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u7204popup">
           <div class="ContainerGroup clearfix" id="u7204"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u7237"><!-- group -->
             <div class="clearfix grpelem" id="u7238-8"><!-- content -->
              <p id="u7238-2">3/3</p>
              <p id="u7238-4">Оберіть</p>
              <p id="u7238-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu7239"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u7284"><!-- none box -->
               <div class="popup_anchor" id="u7285popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u7285"><!-- group -->
                 <div class="clearfix grpelem" id="u7286-6"><!-- content -->
                  <p>Видача&nbsp; посвідчень батьків та дитини</p>
                  <p>&nbsp;багатодітних сімей</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u7242popup">
               <div class="ContainerGroup clearfix" id="u7242"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u7263"><!-- group -->
                 <div class="clearfix grpelem" id="pu7269-4"><!-- column -->
                  <div class="clearfix colelem" id="u7269-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u7266-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u7264-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu7268-4"><!-- column -->
                  <div class="clearfix colelem" id="u7268-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u7271-24"><!-- content -->
                   <p id="u7271-2">Заява на ім'я міського голови.</p>
                   <p id="u7271-4">До заяви додаються:</p>
                   <p id="u7271-6">Для отримання посвідчень необхідно подати наступні документи:</p>
                   <p id="u7271-8">1. Ксерокопія паспортів батьків (сторінки:1,2 та реєстрація).</p>
                   <p id="u7271-10">2. Ксерокопія свідоцтв про&nbsp; народження дітей (у разі, якщо хтось із дітей досяг 16-річного віку, то копія паспорту дитини).</p>
                   <p id="u7271-12">3. Довідка про склад сім'ї (форма 3)( оригінал).</p>
                   <p id="u7271-14">4. Фотокартки 3х4 батьків та&nbsp; фотокартки 3х4 дітей.</p>
                   <p id="u7271-16">5. Довідка про те, що за місцем реєстрації батька або матері посвідчення структурним підрозділом не видавалося (у разі реєстрації&nbsp; батька або матері за межами м. Бердичева).</p>
                   <p id="u7271-18">6. Довідка з навчального закладу (оригінал)про те, що дитина дійсно навчається,&nbsp; де вказується клас, факультет, курс,&nbsp; термін навчання (у разі, якщо одна чи декілька дітей з 18 до 23 років навчаються за денною формою навчання у загальноосвітніх, професійно-технічних та вищих навчальних закладах).</p>
                   <p id="u7271-20">7. Ксерокопія ідентифікаційного коду батьків.</p>
                   <p id="u7271-22">8. Копія свідоцтва про укладення шлюбу між батьками дітей (у разі наявності), копія свідоцтва про розлучення (якщо батьки розлучені).</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u7267-4"><!-- content -->
                   <p id="u7267-2">Видача посвідчень - 10 днів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u7265-4"><!-- content -->
                   <p id="u7265-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u7754" href="assets/%d0%b7%d0%b0%d1%8f%d0%b2%d0%b8-%d0%b1%d0%b0%d0%b3%d0%b0%d1%82%d0%be%d0%b4%d1%96%d1%82%d0%bd%d0%be%d1%97-%d1%81%d1%96%d0%bc%d1%97.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u7754"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u391"><!-- group -->
         <div class="clearfix grpelem" id="u30668-8"><!-- content -->
          <p id="u30668-2">2/3</p>
          <p id="u30668-4">Оберіть</p>
          <p id="u30668-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu9904"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u9907"><!-- none box -->
           <div class="popup_anchor" id="u9908popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u9908" href="start.php#s1"><!-- group --><div class="clearfix grpelem" id="u9909-12"><!-- content --><p id="u9909-2"><span class="ts-----1" id="u9909"><?php Orgprof() ?></span></p><p id="u9909-4"><span class="ts-----1" id="u9909-3"></span></p><p id="u9909-6"><span class="ts-----1" id="u9909-5"></span></p><p id="u9909-8"><span class="ts-----1" id="u9909-7"></span></p><p id="u9909-10"><span class="ts-----1" id="u9909-9"></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u9911popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u9911" href="start.php#s1"><!-- group --><div class="clearfix grpelem" id="u9913-8"><!-- content --><p id="u9913-2"><span class="ts-----1" id="u9913"><?php HromOrg() ?></span></p><p id="u9913-4"><span class="ts-----1" id="u9913-3"></span></p><p id="u9913-6"><span class="ts-----1" id="u9913-5"></span></p></div><div class="museBGSize grpelem" id="u9912"><!-- simple frame --></div></a>
           </div>
           <div class="popup_anchor" id="u10248popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u10248" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u10331"><!-- simple frame --></div><div class="clearfix colelem" id="u10337-6"><!-- content --><p id="u10337-2"><span class="ts-----1" id="u10337"><?php Vidokremlen() ?></span></p><p id="u10337-4"><span class="ts-----1" id="u10337-3"></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u10325popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u10325" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u10334"><!-- simple frame --></div><div class="clearfix colelem" id="u10343-4"><!-- content --><p id="u10343-2"><span class="ts-----1" id="u10343"><?php Treteyskiysud() ?></span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u9917popup">
           <div class="ContainerGroup clearfix" id="u9917"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u9947"><!-- group -->
             <div class="clearfix grpelem" id="u9948-8"><!-- content -->
              <p id="u9948-2">3/3</p>
              <p id="u9948-4">Оберіть</p>
              <p id="u9948-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu9949"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u9979"><!-- none box -->
               <div class="popup_anchor" id="u9982popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u9982"><!-- group -->
                 <div class="clearfix grpelem" id="u9983-4"><!-- content -->
                  <p>Включення відомостей</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u10346popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u10346"><!-- group -->
                 <div class="clearfix grpelem" id="u10408-4"><!-- content -->
                  <p>Створення та об’єднання</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u10375popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u10375"><!-- group -->
                 <div class="clearfix grpelem" id="u10441-4"><!-- content -->
                  <p>Зміни до відомостей</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u10381popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u10381"><!-- group -->
                 <div class="clearfix grpelem" id="u10481-4"><!-- content -->
                  <p>Припинення</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u10387popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u10387"><!-- group -->
                 <div class="clearfix grpelem" id="u10518-4"><!-- content -->
                  <p>Зміни складу комісії</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u10393popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u10393"><!-- group -->
                 <div class="clearfix grpelem" id="u10557-4"><!-- content -->
                  <p>Ліквідація</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u10399popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u10399"><!-- group -->
                 <div class="clearfix grpelem" id="u10596-4"><!-- content -->
                  <p>Реорганізація</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u9950popup">
               <div class="ContainerGroup clearfix" id="u9950"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u9965"><!-- group -->
                 <div class="clearfix grpelem" id="pu9966-4"><!-- column -->
                  <div class="clearfix colelem" id="u9966-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u9971-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9968-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9974-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9973-4"><!-- column -->
                  <div class="clearfix colelem" id="u9973-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9975-4"><!-- content -->
                   <p id="u9975-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9972-8"><!-- content -->
                   <p id="u9972-2">Заява про державну реєстрацію включення відомостей про юридичну особу до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань.</p>
                   <p id="u9972-4">Представником юридичної особи додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u9972-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9969-6"><!-- content -->
                   <p id="u9969-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u9969-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9967-4"><!-- content -->
                   <p id="u9967-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u10351"><!-- group -->
                 <div class="clearfix grpelem" id="pu10418-4"><!-- column -->
                  <div class="clearfix colelem" id="u10418-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u10412-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10413-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10414-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu10411-4"><!-- column -->
                  <div class="clearfix colelem" id="u10411-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10419-4"><!-- content -->
                   <p id="u10419-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 colelem" id="u10996"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
Заява про державну реєстрацію створення юридичної особи;
заява про включення до Реєстру неприбуткових установ та організацій за формою, затвердженою відповідно до законодавства, – за бажанням заявника*;
примірник оригіналу (нотаріально засвідчену копію) рішення засновників про створення юридичної особи, невід’ємною частиною якого є реєстр осіб, які брали участь  у загальних зборах (з’їзді, конференції);
відомості про керівні органи громадського формування (ім’я, дата народження керівника, членів інших керівних органів, реєстраційний номер облікової картки платника податків (за наявності), посада, контактний номер телефону та інші засоби зв’язку), відомості про особу (осіб), яка має право представляти громадське формування для здійснення реєстраційних дій (ім’я, дата народження, контактний номер телефону та інші засоби зв’язку);
установчий документ юридичної особи –  у разі створення юридичної особи на підставі власного установчого документа;
документ про сплату адміністративного збору;
примірник оригіналу (нотаріально засвідчена копія) передавального акта або розподільчого балансу – у разі створення юридичної особи в результаті злиття або виділу;
примірник оригіналу (нотаріально засвідчена копія) розподільчого балансу – у разі створення юридичної особи в результаті поділу;
документи для державної реєстрації змін про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, визначені частиною четвертою статті 17 Закону України «Про державну реєстрацію юридичних осіб, фізичних осіб – підприємців та громадських формувань», – у разі створення юридичної особи в результаті виділу;
документи для державної реєстрації припинення юридичної особи в результаті злиття та поділу – у разі створення юридичної особи в результаті злиття та поділу.
У разі подання документів представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10417-6"><!-- content -->
                   <p id="u10417-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u10417-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10415-4"><!-- content -->
                   <p id="u10415-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u10378"><!-- group -->
                 <div class="clearfix grpelem" id="pu10451-4"><!-- column -->
                  <div class="clearfix colelem" id="u10451-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u10445-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10446-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10447-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu10444-4"><!-- column -->
                  <div class="clearfix colelem" id="u10444-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10452-4"><!-- content -->
                   <p id="u10452-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 colelem" id="u11001"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
1. Для державної реєстрації змін до відомостей, що містяться у Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, у тому числі змін до установчих документів юридичної особи, подаються:
заява про державну реєстрацію змін до відомостей про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;
примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про зміни, що вносяться до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, крім внесення змін до інформації про місцезнаходження та про здійснення зв’язку з юридичною особою;
документ, що підтверджує правомочність прийняття рішення відповідно до статуту громадського формування про внесення змін до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань;
відомості про керівні органи громадського формування (ім’я, дата народження керівника, членів інших керівних органів, реєстраційний номер облікової картки платника податків (за наявності), посада, контактний номер телефону та інші засоби зв’язку);
документ про сплату адміністративного збору, крім внесення змін до інформації про здійснення зв’язку;
установчий документ юридичної особи у новій редакції – у разі внесення змін, що містяться в установчому документі;
заява про включення до Реєстру неприбуткових установ та організацій за формою, затвердженою відповідно до законодавства, - за бажанням заявника у разі внесення до установчих документів змін, які впливають на систему його оподаткування*.
2. Для державної реєстрації змін до відомостей, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, у зв’язку із зупиненням (припиненням) членства у громадському формуванні член керівного органу (крім керівника) подається копія заяви про зупинення (припинення) ним членства до відповідних статутних органів громадського формування з відміткою про її прийняття.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10450-6"><!-- content -->
                   <p id="u10450-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u10450-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10448-4"><!-- content -->
                   <p id="u10448-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u10384"><!-- group -->
                 <div class="clearfix grpelem" id="pu10492-4"><!-- column -->
                  <div class="clearfix colelem" id="u10492-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u10487-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10488-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10489-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu10486-4"><!-- column -->
                  <div class="clearfix colelem" id="u10486-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10493-4"><!-- content -->
                   <p id="u10493-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 colelem" id="u11006"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
Примірник оригіналу (нотаріально засвідчена копія) рішення учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – рішення відповідного державного органу про припинення юридичної особи;
примірник оригіналу (нотаріально засвідчена копія) документа, яким затверджено персональний склад комісії з припинення (комісії з реорганізації, ліквідаційної комісії) або ліквідатора, реєстраційні номери облікових карток платників податків (або відомості про серію та номер паспорта – для фізичних осіб, які через свої релігійні переконання відмовилися від прийняття реєстраційного номера облікової картки платника податків та повідомили про це відповідний контролюючий орган і мають відмітку в паспорті про право здійснювати платежі за серією та номером паспорта), строк заявлення кредиторами своїх вимог, – у разі відсутності зазначених відомостей у рішенні учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – у рішенні відповідного державного органу про припинення юридичної особи.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10491-6"><!-- content -->
                   <p id="u10491-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u10491-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10490-4"><!-- content -->
                   <p id="u10490-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u10390"><!-- group -->
                 <div class="clearfix grpelem" id="pu10528-4"><!-- column -->
                  <div class="clearfix colelem" id="u10528-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u10522-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10523-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10524-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu10521-4"><!-- column -->
                  <div class="clearfix colelem" id="u10521-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10529-4"><!-- content -->
                   <p id="u10529-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10526-8"><!-- content -->
                   <p id="u10526-2">Примірник оригіналу (нотаріально засвідчена копія) рішення учасників юридичної особи або відповідного органу юридичної особи, а у випадках, передбачених законом, – рішення відповідного державного органу про зміни.</p>
                   <p id="u10526-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u10526-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10527-6"><!-- content -->
                   <p id="u10527-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u10527-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10525-4"><!-- content -->
                   <p id="u10525-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u10396"><!-- group -->
                 <div class="clearfix grpelem" id="pu10567-4"><!-- column -->
                  <div class="clearfix colelem" id="u10567-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u10561-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10562-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10563-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu10560-4"><!-- column -->
                  <div class="clearfix colelem" id="u10560-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10568-4"><!-- content -->
                   <p id="u10568-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10565-10"><!-- content -->
                   <p id="u10565-2">Заява про проведення державної реєстрації припинення юридичної особи в результаті її ліквідації;</p>
                   <p id="u10565-4">довідка архівної установи про прийняття документів, що відповідно до закону підлягають довгостроковому зберіганню.</p>
                   <p id="u10565-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u10565-8">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10566-6"><!-- content -->
                   <p id="u10566-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u10566-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10564-4"><!-- content -->
                   <p id="u10564-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u10402"><!-- group -->
                 <div class="clearfix grpelem" id="pu10605-4"><!-- column -->
                  <div class="clearfix colelem" id="u10605-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u10600-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10601-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u10602-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu10599-4"><!-- column -->
                  <div class="clearfix colelem" id="u10599-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10606-4"><!-- content -->
                   <p id="u10606-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 colelem" id="u11011"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
Заява про державну реєстрацію припинення юридичної особи в результаті її реорганізації;
примірник оригіналу (нотаріально засвідчена копія) розподільчого балансу – у разі припинення юридичної особи в результаті поділу;
примірник оригіналу (нотаріально засвідчена копія) передавального акта – у разі припинення юридичної особи в результаті злиття або приєднання;
довідка архівної установи про прийняття документів, що відповідно до закону підлягають довгостроковому 
зберіганню, – у разі припинення юридичної особи в результаті поділу, злиття або приєднання;
документи для державної реєстрації змін до відомостей про юридичну особу, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, – у разі припинення юридичної особи в результаті приєднання.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10604-6"><!-- content -->
                   <p id="u10604-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u10604-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u10603-4"><!-- content -->
                   <p id="u10603-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u9918"><!-- group -->
             <div class="clearfix grpelem" id="u30872-8"><!-- content -->
              <p id="u30872-2">3/3</p>
              <p id="u30872-4">Оберіть</p>
              <p id="u30872-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu9919"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u9942"><!-- none box -->
               <div class="popup_anchor" id="u9945popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u9945"><!-- group -->
                 <div class="clearfix grpelem" id="u9946-4"><!-- content -->
                  <p>Реєстрація</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9943popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u9943"><!-- group -->
                 <div class="clearfix grpelem" id="u9944-4"><!-- content -->
                  <p>Зміни до відомостей</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19019popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u19019"><!-- group -->
                 <div class="clearfix grpelem" id="u19047-4"><!-- content -->
                  <p>Припинення</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u9922popup">
               <div class="ContainerGroup clearfix" id="u9922"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u9923"><!-- group -->
                 <div class="clearfix grpelem" id="pu19057-4"><!-- column -->
                  <div class="clearfix colelem" id="u19057-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19051-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19052-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19053-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19050-4"><!-- column -->
                  <div class="clearfix colelem" id="u19050-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19058-4"><!-- content -->
                   <p id="u19058-2">Звернення уповноваженого представника</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19055-14"><!-- content -->
                   <p id="u19055-2">Заява про державну реєстрацію громадського об’єднання, що не має статусу юридичної особи;</p>
                   <p id="u19055-4">примірник оригіналу (нотаріально засвідчена копія) рішення про утворення громадського об’єднання;</p>
                   <p id="u19055-6">відомості про засновників громадського об’єднання (прізвище, ім’я, по батькові, дата народження, адреса місця проживання, реєстраційний номер облікової картки платника податків (за наявності) – для фізичної особи; її найменування, місцезнаходження, ідентифікаційний код – для юридичної особи);</p>
                   <p id="u19055-8">відомості про особу (осіб), уповноважену представляти громадське об’єднання (прізвище, ім’я, по батькові, дата народження, реєстраційний номер облікової картки платника податків (за наявності), контактний номер телефону та інші засоби зв’язку).</p>
                   <p id="u19055-10">У разі подання документів представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19055-12">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19056-6"><!-- content -->
                   <p id="u19056-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19056-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19054-4"><!-- content -->
                   <p id="u19054-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u9932"><!-- group -->
                 <div class="clearfix grpelem" id="pu19087-4"><!-- column -->
                  <div class="clearfix colelem" id="u19087-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19081-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19082-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19083-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19080-4"><!-- column -->
                  <div class="clearfix colelem" id="u19080-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19088-4"><!-- content -->
                   <p id="u19088-2">Звернення уповноваженого представника</p>
                  </div>
                  <div class="Text-1 colelem" id="u19107"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p>
1. Для державної реєстрації змін до відомостей про громадське об’єднання, що не має статусу юридичної особи, що містяться у Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, подається:
заява про державну реєстрацію змін до відомостей про громадське об’єднання, що не має статусу юридичної особи, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;
примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління громадського об’єднання про зміни, що вносяться до Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань.
2. Для державної реєстрації змін до відомостей про громадське об’єднання, що не має статусу юридичної особи, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, у зв’язку із зупиненням (припиненням) членства в громадському об’єднанні, що не має статусу юридичної особи, член керівного органу (крім керівника) подає копію заяви про зупинення (припинення) ним членства до відповідних статутних органів громадського об’єднання з відміткою про її прийняття.
У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.
Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання
 </p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19086-6"><!-- content -->
                   <p id="u19086-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19086-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19084-4"><!-- content -->
                   <p id="u19084-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19024"><!-- group -->
                 <div class="clearfix grpelem" id="pu19118-4"><!-- column -->
                  <div class="clearfix colelem" id="u19118-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u30913-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19113-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19114-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19112-4"><!-- column -->
                  <div class="clearfix colelem" id="u19112-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19119-4"><!-- content -->
                   <p id="u19119-2">Звернення уповноваженого представника</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19116-8"><!-- content -->
                   <p id="u19116-2">Примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління громадського об'єднання, що не має статусу юридичної особи, про його саморозпуск.</p>
                   <p id="u19116-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19116-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19117-6"><!-- content -->
                   <p id="u19117-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19117-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19115-4"><!-- content -->
                   <p id="u19115-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u10255"><!-- group -->
             <div class="clearfix grpelem" id="u30875-8"><!-- content -->
              <p id="u30875-2">3/3</p>
              <p id="u30875-4">Оберіть</p>
              <p id="u30875-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu19136"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u19211"><!-- none box -->
               <div class="popup_anchor" id="u19216popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u19216"><!-- group -->
                 <div class="clearfix grpelem" id="u19217-4"><!-- content -->
                  <p>Створення відокремленого підрозділу</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19218popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u19218"><!-- group -->
                 <div class="clearfix grpelem" id="u19219-4"><!-- content -->
                  <p>Зміни до відомостей про відокремлений підрозділ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19224popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u19224"><!-- group -->
                 <div class="clearfix grpelem" id="u19225-4"><!-- content -->
                  <p>Припинення відокремленого підрозділу</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19214popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u19214"><!-- group -->
                 <div class="clearfix grpelem" id="u19215-4"><!-- content -->
                  <p>Рішення про виділ громадського об'єднання</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19212popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u19212"><!-- group -->
                 <div class="clearfix grpelem" id="u19213-4"><!-- content -->
                  <p>Підтвердження всеукраїнського статусу</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19222popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u19222"><!-- group -->
                 <div class="clearfix grpelem" id="u19223-4"><!-- content -->
                  <p>Відмова від всеукраїнського статусу</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u19137popup">
               <div class="ContainerGroup clearfix" id="u19137"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u19198"><!-- group -->
                 <div class="clearfix grpelem" id="pu19199-4"><!-- column -->
                  <div class="clearfix colelem" id="u19199-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19203-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19207-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19201-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19205-4"><!-- column -->
                  <div class="clearfix colelem" id="u19205-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19206-4"><!-- content -->
                   <p id="u19206-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19202-10"><!-- content -->
                   <p id="u19202-2">Заява про державну реєстрацію створення відокремленого підрозділу юридичної особи;</p>
                   <p id="u19202-4">примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління юридичної особи про створення відокремленого підрозділу.</p>
                   <p id="u19202-6">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19202-8">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19200-6"><!-- content -->
                   <p id="u19200-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19200-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19204-4"><!-- content -->
                   <p id="u19204-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19148"><!-- group -->
                 <div class="clearfix grpelem" id="pu19153-4"><!-- column -->
                  <div class="clearfix colelem" id="u19153-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19157-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19149-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19150-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19156-4"><!-- column -->
                  <div class="clearfix colelem" id="u19156-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19154-4"><!-- content -->
                   <p id="u19154-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19417-8"><!-- content -->
                   <p id="u19417-2">Заява про державну реєстрацію змін до відомостей про відокремлений підрозділ юридичної особи, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань.</p>
                   <p id="u19417-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19417-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19152-6"><!-- content -->
                   <p id="u19152-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19152-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19151-4"><!-- content -->
                   <p id="u19151-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19168"><!-- group -->
                 <div class="clearfix grpelem" id="pu19170-4"><!-- column -->
                  <div class="clearfix colelem" id="u19170-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19174-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19173-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19172-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19175-4"><!-- column -->
                  <div class="clearfix colelem" id="u19175-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19176-4"><!-- content -->
                   <p id="u19176-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19423-8"><!-- content -->
                   <p id="u19423-2">Заява про державну реєстрацію припинення відокремленого підрозділу юридичної особи.</p>
                   <p id="u19423-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19423-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19177-6"><!-- content -->
                   <p id="u19177-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19177-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19171-4"><!-- content -->
                   <p id="u19171-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19178"><!-- group -->
                 <div class="clearfix grpelem" id="pu19184-4"><!-- column -->
                  <div class="clearfix colelem" id="u19184-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19186-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19182-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19179-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19187-4"><!-- column -->
                  <div class="clearfix colelem" id="u19187-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19180-4"><!-- content -->
                   <p id="u19180-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19429-8"><!-- content -->
                   <p id="u19429-2">Примірник оригіналу (нотаріально засвідчена копія) рішення відповідного органу юридичної особи про виділ юридичної особи.</p>
                   <p id="u19429-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19429-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19183-6"><!-- content -->
                   <p id="u19183-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19183-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19181-4"><!-- content -->
                   <p id="u19181-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19138"><!-- group -->
                 <div class="clearfix grpelem" id="pu19139-4"><!-- column -->
                  <div class="clearfix colelem" id="u19139-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19140-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19143-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19145-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19141-4"><!-- column -->
                  <div class="clearfix colelem" id="u19141-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19147-4"><!-- content -->
                   <p id="u19147-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19142-8"><!-- content -->
                   <p id="u19142-2">Заява про державну реєстрацію підтвердження всеукраїнського статусу.</p>
                   <p id="u19142-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19142-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19146-6"><!-- content -->
                   <p id="u19146-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19146-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19144-4"><!-- content -->
                   <p id="u19144-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19158"><!-- group -->
                 <div class="clearfix grpelem" id="pu19167-4"><!-- column -->
                  <div class="clearfix colelem" id="u19167-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19162-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19163-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19164-4"><!-- content -->
                   <p>П��атність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19161-4"><!-- column -->
                  <div class="clearfix colelem" id="u19161-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19159-4"><!-- content -->
                   <p id="u19159-2">Звернення уповноваженого представника юридичної особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19160-8"><!-- content -->
                   <p id="u19160-2">Заява про державну реєстрацію відмови від&nbsp; всеукраїнського статусу.</p>
                   <p id="u19160-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19160-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19166-6"><!-- content -->
                   <p id="u19166-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19166-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19165-4"><!-- content -->
                   <p id="u19165-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u10328"><!-- group -->
             <div class="clearfix grpelem" id="u30878-8"><!-- content -->
              <p id="u30878-2">3/3</p>
              <p id="u30878-4">Оберіть</p>
              <p id="u30878-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu19432"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u19435"><!-- none box -->
               <div class="popup_anchor" id="u19442popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u19442"><!-- group -->
                 <div class="clearfix grpelem" id="u19443-4"><!-- content -->
                  <p>Створення суду</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19446popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u19446"><!-- group -->
                 <div class="clearfix grpelem" id="u19447-4"><!-- content -->
                  <p>Зміни до відомостей про суд</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19436popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u19436"><!-- group -->
                 <div class="clearfix grpelem" id="u19437-4"><!-- content -->
                  <p>Припинення суду</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19438popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u19438"><!-- group -->
                 <div class="clearfix grpelem" id="u19439-4"><!-- content -->
                  <p>Видача виписки у паперовій формі для проставлення апостиля</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u19440popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u19440"><!-- group -->
                 <div class="clearfix grpelem" id="u19441-4"><!-- content -->
                  <p>Видача документів, що містяться в реєстраційній справі</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u19448popup">
               <div class="ContainerGroup clearfix" id="u19448"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u19499"><!-- group -->
                 <div class="clearfix grpelem" id="pu19502-4"><!-- column -->
                  <div class="clearfix colelem" id="u19502-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19504-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19503-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19508-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19501-4"><!-- column -->
                  <div class="clearfix colelem" id="u19501-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19505-4"><!-- content -->
                   <p id="u19505-2">Звернення уповноваженого представника</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19500-16"><!-- content -->
                   <p id="u19500-2">заява про державну реєстрацію постійно діючого третейського суду;</p>
                   <p id="u19500-4">примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління засновника про створення постійно діючого третейського суду;</p>
                   <p id="u19500-6">установчі документи постійно діючого третейського суду (положення, регламент третейського суду);</p>
                   <p id="u19500-8">список третейських суддів;</p>
                   <p id="u19500-10">копія статуту засновника третейського суду.</p>
                   <p id="u19500-12">У разі подання документів представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19500-14">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19507-6"><!-- content -->
                   <p id="u19507-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19507-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19506-4"><!-- content -->
                   <p id="u19506-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19469"><!-- group -->
                 <div class="clearfix grpelem" id="pu19470-4"><!-- column -->
                  <div class="clearfix colelem" id="u19470-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19475-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19478-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19476-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19477-4"><!-- column -->
                  <div class="clearfix colelem" id="u19477-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19473-4"><!-- content -->
                   <p id="u19473-2">Звернення уповноваженого представника</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19474-16"><!-- content -->
                   <p id="u19474-2">Заява про державну реєстрацію змін до відомостей про постійно діючий третейський суд, які містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;</p>
                   <p id="u19474-4">примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління засновника про внесення змін до відомостей про постійно діючий третейський суд, що містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань;</p>
                   <p id="u19474-6">установчі документи постійно діючого третейського суду (положення, регламент третейського суду) у новій редакції – у разі внесення змін, що містяться в установчих документах постійно діючого третейського суду;</p>
                   <p id="u19474-8">список третейських суддів у новій редакції - у разі внесення змін до складу третейських суддів;</p>
                   <p id="u19474-10">копія статуту засновника третейського суду.</p>
                   <p id="u19474-12">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19474-14">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19472-6"><!-- content -->
                   <p id="u19472-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19472-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19471-4"><!-- content -->
                   <p id="u19471-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19459"><!-- group -->
                 <div class="clearfix grpelem" id="pu19463-4"><!-- column -->
                  <div class="clearfix colelem" id="u19463-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19460-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19468-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19466-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19467-4"><!-- column -->
                  <div class="clearfix colelem" id="u19467-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19465-4"><!-- content -->
                   <p id="u19465-2">Звернення уповноваженого представника</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19462-8"><!-- content -->
                   <p id="u19462-2">Примірник оригіналу (нотаріально засвідчена копія) рішення уповноваженого органу управління засновника постійно діючого третейського суду про його припинення.</p>
                   <p id="u19462-4">У разі подання документів, крім випадку, коли відомості про повноваження цього представника містяться в Єдиному державному реєстрі юридичних осіб, фізичних осіб – підприємців та громадських формувань, представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19462-6">Якщо документи подаються особисто, заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19461-6"><!-- content -->
                   <p id="u19461-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19461-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19464-4"><!-- content -->
                   <p id="u19464-2">Безоплатно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19479"><!-- group -->
                 <div class="clearfix grpelem" id="pu19481-4"><!-- column -->
                  <div class="clearfix colelem" id="u19481-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19483-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19480-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19484-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19486-4"><!-- column -->
                  <div class="clearfix colelem" id="u19486-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19482-4"><!-- content -->
                   <p id="u19482-2">Запит фізичної особи, або юридичної особи, або уповноваженої особи, яка бажає отримати виписку з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань в паперовій формі для проставлення апостиля</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19487-10"><!-- content -->
                   <p id="u19487-2">Запит про надання виписки з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань (додаток 1 до Порядку надання відомостей з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, затвердженого наказом Міністерства юстиції України від 10.06.2016 № 1657/5, зареєстрованого у Міністерстві юстиції України 10.06.2016 за № 839/28969);</p>
                   <p id="u19487-4">&nbsp;документ, що підтверджує внесення плати за отримання відповідних відомостей.</p>
                   <p id="u19487-6">У разі подання запиту представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19487-8">Заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19485-4"><!-- content -->
                   <p id="u19485-2">Запит про надання виписки з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань подається в паперовій формі особисто заявником</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19488-4"><!-- content -->
                   <p id="u19488-2">Платно.</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u19489"><!-- group -->
                 <div class="clearfix grpelem" id="pu19494-4"><!-- column -->
                  <div class="clearfix colelem" id="u19494-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u19495-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19493-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u19490-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu19491-4"><!-- column -->
                  <div class="clearfix colelem" id="u19491-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19498-4"><!-- content -->
                   <p id="u19498-2">Запит фізичної особи або юридичної особи, які бажають отримати документи з реєстраційної справи юридичних осіб, фізичних осіб – підприємців та громадських формувань, або уповноваженої особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19497-10"><!-- content -->
                   <p id="u19497-2">Запит про надання документів, що містяться в реєстраційній справі відповідної юридичної особи, громадського формування, що не має статусу юридичної особи, фізичної особи – підприємця (додаток 3 до Порядку надання відомостей з Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань, затвердженого наказом Міністерства юстиції України від 10.06.2016 № 1657/5, зареєстрованого у Міністерстві юстиції України 10.06.2016 за № 839/28969);</p>
                   <p id="u19497-4">&nbsp;документ, що підтверджує внесення плати за отримання відповідних відомостей.</p>
                   <p id="u19497-6">У разі подання запиту представником додатково подається примірник оригіналу (нотаріально засвідчена копія) документа, що засвідчує його повноваження.</p>
                   <p id="u19497-8">Заявник пред'являє свій паспорт громадянина України, або тимчасове посвідчення громадянина України, або паспортний документ іноземця, або посвідчення особи без громадянства, або посвідку на постійне або тимчасове проживання</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19496-6"><!-- content -->
                   <p id="u19496-2">1. У паперовій формі документи подаються заявником особисто або поштовим відправленням.</p>
                   <p id="u19496-4">2. В електронній формі документи подаються через портал електронних сервісів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u19492-4"><!-- content -->
                   <p id="u19492-2">Платно.</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u399"><!-- group -->
         <div class="clearfix grpelem" id="u30671-8"><!-- content -->
          <p id="u30671-2">2/3</p>
          <p id="u30671-4">Оберіть</p>
          <p id="u30671-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu1617"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u1618"><!-- none box -->
           <div class="popup_anchor" id="u1621popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u1621" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u2008"><!-- simple frame --></div><div class="clearfix colelem" id="u1622-4"><!-- content --><p id="u1622-2"><span class="ts-----1" id="u1622"><?php Powidomlen() ?></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u1619popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u1619" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u2021"><!-- simple frame --></div><div class="clearfix colelem" id="u1620-4"><!-- content --><p id="u1620-2"><span class="ts-----1" id="u1620"><?php Declar() ?></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u1623popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u1623" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u2027"><!-- simple frame --></div><div class="clearfix colelem" id="u1624-4"><!-- content --><p id="u1624-2"><span class="ts-----1" id="u1624"><?php Dozvil() ?></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u1625popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u1625" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u2033"><!-- simple frame --></div><div class="clearfix colelem" id="u1626-4"><!-- content --><p id="u1626-2"><span class="ts-----1" id="u1626"><?php Sertif2()?></span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u1629popup">
           <div class="ContainerGroup clearfix" id="u1629"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u1632"><!-- group -->
             <div class="clearfix grpelem" id="u7836-8"><!-- content -->
              <p id="u7836-2">3/3</p>
              <p id="u7836-4">Оберіть</p>
              <p id="u7836-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu1633"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u1634"><!-- none box -->
               <div class="popup_anchor" id="u1641popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u1641"><!-- group -->
                 <div class="clearfix grpelem" id="u1642-4"><!-- content -->
                  <p>Подання повідомлення про початок виконання підготовчих робіт</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u1637popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u1637"><!-- group -->
                 <div class="clearfix grpelem" id="u1638-4"><!-- content -->
                  <p>Внесення змін до повідомлення про початок виконання підготовчих робіт</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u1639popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u1639"><!-- group -->
                 <div class="clearfix grpelem" id="u1640-4"><!-- content -->
                  <p>Подання повідомлення про початок виконання будівельних робіт</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u1635popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u1635"><!-- group -->
                 <div class="clearfix grpelem" id="u1636-4"><!-- content -->
                  <p>Внесення змін до повідомлення про початок виконання будівельних робіт</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u1643popup">
               <div class="ContainerGroup clearfix" id="u1643"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u1644"><!-- group -->
                 <div class="clearfix grpelem" id="pu1651-4"><!-- column -->
                  <div class="clearfix colelem" id="u1651-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u1645-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u1648-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u1652-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu1646-4"><!-- column -->
                  <div class="clearfix colelem" id="u1646-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u1653-4"><!-- content -->
                   <p id="u1653-2">Початок виконання підготовчих робіт.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u1650-4"><!-- content -->
                   <p id="u1650-2">Повідомлення про початок виконання підготовчих робіт відповідно до вимог статті 35 Закону України «Про регулювання містобудівної діяльності» за формою встановленого зразка.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u1649-4"><!-- content -->
                   <p id="u1649-2">Подається замовником (його уповноваженою особою) особисто або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництві.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u1647-4"><!-- content -->
                   <p id="u1647-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu35756"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u35756" href="https://e-dabi.gov.ua/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35756"><i class="fa fa-share-square"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u37161" href="assets/dab1.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37161"><i class="fa fa-download"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u1655"><!-- group -->
                 <div class="clearfix grpelem" id="pu2066-4"><!-- column -->
                  <div class="clearfix colelem" id="u2066-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2067-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2068-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2069-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2039-4"><!-- column -->
                  <div class="clearfix colelem" id="u2039-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2044-6"><!-- content -->
                   <p id="u2044-2">1.У разі коли право на будівництво об´єкта передано іншому замовнику або змінено осіб, відповідальних за проведення авторського і технічного нагляду під час виконання підготовчих робіт.</p>
                   <p id="u2044-4">2.Виявлення замовником технічної помилки (описки, друкарської, граматичної, арифметичної помилки) у поданому повідомленні про початок виконання підготовчих робіт або отримання відомостей про виявлення недостовірних даних.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2046-8"><!-- content -->
                   <p id="u2046-2">1.Повідомлення про зміну даних у повідомленні про початок виконання підготовчих робіт, у якому враховані зміни згідно пункту 14 Порядку виконання підготовчих та будівельних робіт, затвердженого постановою Кабінету Міністрів України від 13.04.2011 №466 «Деякі питання виконання підготовчих і будівельних робіт» (в редакції постанови Кабінету Міністрів від 07.06.2017 № 404 «Про внесення змін до постанови Кабінету Міністрів України від 13 квітня 2011р. №466) за формою встановленого зразка.</p>
                   <p id="u2046-4">2.Заява згідно статті 39¹ Закону України «Про регулювання містобудівної діяльності» та абзацу другого пункту 15 Порядку виконання підготовчих та будівельних робіт, затвердженого постановою Кабінету Міністрів України від 13.04.2011 №466 «Деякі питання виконання підготовчих і будівельних робіт» (в редакції постанови Кабінету Міністрів України від 07.06.2017&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; №404 «Про внесення змін до постанови Кабінету Міністрів України від 13 квітня 2011р. №466) за формою встановленого зразка.</p>
                   <p id="u2046-6">Один примірник повідомлення про зміну даних у повідомленні про початок виконання підготовчих робіт, у якому враховані зміни згідно статті 39¹ Закону України «Про регулювання містобудівної діяльності» та абзацу другого пункту 15 Порядку виконання підготовчих та будівельних робіт, затвердженого постановою Кабінету Міністрів України від 13.04.2011 №466 «Деякі питання виконання підготовчих і будівельних робіт (в редакції постанови Кабінету Міністрів України від 07.06.2017 №404&nbsp; «Про внесення змін до постанови Кабінету Міністрів України від 13 квітня 2011 р. №466) за формою встановленого зразка.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2047-4"><!-- content -->
                   <p id="u2047-2">Подається замовником (його уповноваженою особою) особисто або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництві протягом трьох робочих днів з дня визначення змін/протягом трьох робочих днів з дня самостійного виявлення технічної помилки.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2045-4"><!-- content -->
                   <p id="u2045-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu35766"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u35766" href="https://e-dabi.gov.ua/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35766"><i class="fa fa-share-square"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u37183" href="assets/dab2.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37183"><i class="fa fa-download"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u1654"><!-- group -->
                 <div class="clearfix grpelem" id="pu2083-4"><!-- column -->
                  <div class="clearfix colelem" id="u2083-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2084-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2085-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2086-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2078-4"><!-- column -->
                  <div class="clearfix colelem" id="u2078-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2079-4"><!-- content -->
                   <p id="u2079-2">Виконання будівельних робіт.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2081-4"><!-- content -->
                   <p id="u2081-2">Повідомлення про початок виконання будівельних робіт відповідно до вимог статті 34 Закону України «Про регулювання містобудівної діяльності» за формою встановленого зразка.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2082-4"><!-- content -->
                   <p id="u2082-2">Подається замовником (його уповноваженою особою) особисто або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництві.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2080-4"><!-- content -->
                   <p id="u2080-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu35771"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u35771" href="https://e-dabi.gov.ua/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35771"><i class="fa fa-share-square"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u37188" href="assets/dab3.1.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37188"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u37201-6"><!-- content -->
                   <p>на підставі</p>
                   <p>будівельного паспорта</p>
                  </div>
                  <a class="nonblock nontext size_fixed colelem" id="u37193" href="assets/dab3.2.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37193"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u37207-4"><!-- content -->
                   <p>CC1</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u1656"><!-- group -->
                 <div class="clearfix grpelem" id="pu2110-4"><!-- column -->
                  <div class="clearfix colelem" id="u2110-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2111-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2112-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2113-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2105-4"><!-- column -->
                  <div class="clearfix colelem" id="u2105-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2106-6"><!-- content -->
                   <p id="u2106-2">1.У разі коли змінено осіб, відповідальних за проведення авторського та технічного нагляду під час виконання будівельних робіт, а також у разі коригування проектної документації на виконання будівельних робіт у встановленому законодавством порядку під час виконання будівельних робіт</p>
                   <p id="u2106-4">2.Виявлення замовником технічної помилки (описки, друкарської, граматичної, арифметичної помилки) у поданому повідомленні про початок виконання будівельних робіт або отримання відомостей про виявлення недостовірних даних.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2108-8"><!-- content -->
                   <p id="u2108-2">1.Повідомлення про зміну даних у повідомленні про початок виконання будівельних робіт, у якому враховані зміни згідно пункту 14 Порядку виконання підготовчих та будівельних робіт, затвердженого постановою Кабінету Міністрів України від 13.04.2011 № 466 «Деякі питання виконання підготовчих і будівельних робіт» (в редакції постанови Кабінету Міністрів від 07.06.2017 № 404 «Про внесення змін до постанови Кабінету Міністрів України від 13 квітня 2011р. №466) за формою встановленого зразка.</p>
                   <p id="u2108-4">2.Заява згідно статті 39 Закону України «Про регулювання містобудівної діяльності» та абзацу другого пункту 15 Порядку виконання підготовчих та будівельних робіт, затвердженого постановою Кабінету Міністрів України від 13.04.2011 №466 «Деякі питання виконання підготовчих і будівельних робіт» (в редакції постанови Кабінету Міністрів України від 07.06.2017&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; №404 «Про внесення змін до постанови Кабінету Міністрів України від 13 квітня 2011р. №466) за формою встановленого зразка.</p>
                   <p id="u2108-6">Один примірник повідомлення про зміну даних у повідомленні про початок виконання будівельних робіт, у якому враховані зміни згідно статті 39 Закону України «Про регулювання містобудівної діяльності» та абзацу другого пункту 15 Порядку виконання підготовчих та будівельних робіт, затвердженого постановою Кабінету Міністрів України від 13.04.2011 №466 «Деякі питання виконання підготовчих і будівельних робіт (в редакції постанови Кабінету Міністрів України від 07.06.2017 №404 «Про внесення змін до постанови Кабінету Міністрів України від 13 квітня 2011 р. №466) за формою встановленого зразка</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2109-4"><!-- content -->
                   <p id="u2109-2">Подається замовником (його уповноваженою особою) особисто або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництв протягом трьох робочих днів з дня визначення змін/ протягом трьох робочих днів з дня самостійного виявлення технічної помилки.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2107-4"><!-- content -->
                   <p id="u2107-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu35781"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u35781" href="https://e-dabi.gov.ua/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35781"><i class="fa fa-share-square"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u37234" href="assets/dab4.1.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37234"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u37226-6"><!-- content -->
                   <p>на підставі</p>
                   <p>будівельного паспорта</p>
                  </div>
                  <a class="nonblock nontext size_fixed colelem" id="u37229" href="assets/dab4.2.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37229"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u37239-4"><!-- content -->
                   <p>CC1</p>
                  </div>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u1630"><!-- group -->
             <div class="clearfix grpelem" id="u30961-8"><!-- content -->
              <p id="u30961-2">3/3</p>
              <p id="u30961-4">Оберіть</p>
              <p id="u30961-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu2489"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u2491"><!-- none box -->
               <div class="popup_anchor" id="u2494popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u2494"><!-- group -->
                 <div class="clearfix grpelem" id="u2495-4"><!-- content -->
                  <p>Реєстрація декларації про готовність об’єкта до експлуатації</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u2498popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u2498"><!-- group -->
                 <div class="clearfix grpelem" id="u2499-4"><!-- content -->
                  <p>Внесення змін до декларації про готовність об’єкта до експлуатації</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u2502popup">
               <div class="ContainerGroup clearfix" id="u2502"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u2503"><!-- group -->
                 <div class="clearfix grpelem" id="pu2509-4"><!-- column -->
                  <div class="clearfix colelem" id="u2509-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2508-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2505-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2511-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2510-4"><!-- column -->
                  <div class="clearfix colelem" id="u2510-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2512-4"><!-- content -->
                   <p id="u2512-2">Прийняття в експлуатацію закінчених будівництвом об’єктів, що за класом наслідків (відповідальності) належать об’єктів з незначними наслідками (СС1) та об’єктів, будівництво яких здійснювалося на підставі будівельного паспорта.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2506-10"><!-- content -->
                   <p id="u2506-2">Один примірник декларації про готовність об’єкта до експлуатації:</p>
                   <p id="u2506-4">- щодо об’єктів, будівництво яких здійснено на підставі будівельного паспорта, за формою встановленого зразка;</p>
                   <p id="u2506-6">- щодо об’єктів, що за класом наслідків (відповідальності) належать до об’єктів з незначними наслідками (СС1), за формою встановленого зразка;</p>
                   <p id="u2506-8">- щодо самочинно збудованого об’єкта, на яке визнано право власності за рішенням суду, за формою встановленого зразка.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2507-4"><!-- content -->
                   <p id="u2507-2">Через центр надання адміністративних послуг. Заповнюється і подається замовником (його уповноваженою особою) особисто * або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництві.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2504-4"><!-- content -->
                   <p id="u2504-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu35796"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u35796" href="https://e-dabi.gov.ua/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35796"><i class="fa fa-share-square"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u37245" href="assets/d6.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37245"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u37242-4"><!-- content -->
                   <p>декларація</p>
                  </div>
                  <a class="nonblock nontext size_fixed colelem" id="u37250" href="assets/d7.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37250"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u37255-4"><!-- content -->
                   <p>заява</p>
                  </div>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u2513"><!-- group -->
                 <div class="clearfix grpelem" id="pu2516-4"><!-- column -->
                  <div class="clearfix colelem" id="u2516-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2519-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2522-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2521-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2518-4"><!-- column -->
                  <div class="clearfix colelem" id="u2518-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2515-4"><!-- content -->
                   <p id="u2515-2">Виявлення замовником технiчної помилки (описки, друкарської, граматичної, арифметичної помилки) в зареєстрованiй декларації або отримання вiдомостей про виявлення недостовiрних даних (встановлення факту, шо на дату реєстрації декларацiї iнформацiя, яка зазначалася в нiй, не вiдповiдала дiйсностi та/або виявлення розбiжностей мiж даними, зазначеними у декларації), якi не є пiдставою вважати об'єкт самочинним будiвництвом.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2517-8"><!-- content -->
                   <p id="u2517-2">1.Заява згідно статті 39¹ Закону України «Про регулювання містобудівної діяльності» та пункту 22 Порядку прийняття в експлуатацію закінчених будівництвом об'єктів, затвердженого постановою Кабінету Міністрів України від 13.04.2011 № 461 «Питання прийняття в експлуатацію закінчених будівництвом об'єктів» (в редакції постанови Кабінету Міністрів України від 07.06.2017 №409&nbsp; «Про внесення змін до порядків, затверджених постановою Кабінету Міністрів України від 13 квітня 2011 р. №461), за формою встановленого зразка;</p>
                   <p id="u2517-4">2.Декларація, в якій враховані зміни згідно статті 39¹ Закону України «Про регулювання містобудівної діяльності» та пункту 22 Порядку прийняття в експлуатацію закінчених будівництвом об'єктів, затвердженого постановою Кабінету Міністрів України від 13.04.2011 № 461 «Питання прийняття в експлуатацію закінчених будівництвом об'єктів» (в редакції постанови Кабінету Міністрів України від 07.06.2017 №409&nbsp; «Про внесення змін до порядків, затверджених постановою Кабінету Міністрів України від 13 квітня 2011 р. №461), за формою встановленого зразка;</p>
                   <p id="u2517-6">Щодо об’єкта, який належить до III категорії складності (клас наслідків (відповідальності) СС2), заява подається в довільній формі, до якої додаються засвідчені в установленому порядку копії документів, що підтверджують зміни.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2520-4"><!-- content -->
                   <p id="u2520-2">Через центр надання адміністративних послуг. Подається замовником (його уповноваженою особою) особисто * або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будів��ицтві.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2514-4"><!-- content -->
                   <p id="u2514-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u35801" href="https://e-dabi.gov.ua/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35801"><i class="fa fa-share-square"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u1661"><!-- group -->
             <div class="clearfix grpelem" id="u3612-8"><!-- content -->
              <p id="u3612-2">3/3</p>
              <p id="u3612-4">Оберіть</p>
              <p id="u3612-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu2708"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u2752"><!-- none box -->
               <div class="popup_anchor" id="u2755popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u2755"><!-- group -->
                 <div class="clearfix grpelem" id="u2756-4"><!-- content -->
                  <p>Видача дозволу на виконання будівельних робіт</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u2753popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u2753"><!-- group -->
                 <div class="clearfix grpelem" id="u2754-4"><!-- content -->
                  <p>Зміна даних у виданому дозволі на виконання будівельних робіт</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u2709popup">
               <div class="ContainerGroup clearfix" id="u2709"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u2710"><!-- group -->
                 <div class="clearfix grpelem" id="pu2717-4"><!-- column -->
                  <div class="clearfix colelem" id="u2717-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2711-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2719-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2716-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2712-4"><!-- column -->
                  <div class="clearfix colelem" id="u2712-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2713-4"><!-- content -->
                   <p id="u2713-2">Початок виконання підготовчих (якщо вони не були виконані раніше згідно з повідомленням про початок виконання підготовчих робіт) і будівельних робіт на об’єктах, що за класом наслідків (відповідальності) належать до об’єктів з середніми (СС2) наслідками.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2714-18"><!-- content -->
                   <p id="u2714-2">1.Заява про отримання дозволу відповідно до вимог частини третьої статті 37 Закону України «Про регулювання містобудівної діяльності» за формою встановленого зразка;</p>
                   <p id="u2714-4">До заяви додаються:</p>
                   <p id="u2714-6">2.Копія документа, що посвідчує право власності чи користування земельною ділянкою або копія договору суперфіцію (крім випадків, визначених пунктом 7 Порядку)*;</p>
                   <p id="u2714-8">3.Копія розпорядчого документа щодо комплексної реконструкції кварталів (мікрорайонів) застарілого житлового фонду у разі здійснення комплексної реконструкції кварталів (мікрорайонів) застарілого житлового фонду на замовлення органів державної влади чи органів місцевого самоврядування на відповідних землях державної чи комунальної власності (замість копії документа, що посвідчує право власності чи користування земельною ділянкою);</p>
                   <p id="u2714-10">4.Проектна документація на будівництво, розроблена та затверджена в установленому законодавством порядку;</p>
                   <p id="u2714-12">5.Копія документа, що посвідчує право власності на будинок чи споруду, або згода його власника, засвідчена у встановленому законом порядку,&nbsp; на проведення будівельних робіт у разі здійснення реконструкції, реставрації, капітального ремонту чи технічного переоснащення діючих підприємств;</p>
                   <p id="u2714-14">6.Копії документів про призначення осіб, відповідальних за виконання будівельних робіт, та осіб, які здійснюють авторський і технічний нагляд;</p>
                   <p id="u2714-16">7.Інформація про ліцензію, що дає право на виконання будівельних робіт, та кваліфікаційні сертифікати.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2715-4"><!-- content -->
                   <p id="u2715-2">Через центр надання адміністративних послуг або подається замовником (його уповноваженою особою) особисто або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництві.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2718-4"><!-- content -->
                   <p id="u2718-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u37210" href="assets/d8.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37210"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u2740"><!-- group -->
                 <div class="clearfix grpelem" id="pu2743-4"><!-- column -->
                  <div class="clearfix colelem" id="u2743-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2746-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2745-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2744-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2747-4"><!-- column -->
                  <div class="clearfix colelem" id="u2747-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2749-4"><!-- content -->
                   <p id="u2749-2">У разі коли право на будівництво об´єкта передано іншому замовникові або змінено генерального підрядника чи підрядника (якщо будівельні роботи виконуються без залучення субпідрядників), чи змінено осіб, відповідальних за проведення авторського і технічного нагляду, або відповідальних виконавців робіт, а також у разі коригування проектної документації.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2748-6"><!-- content -->
                   <p id="u2748-2">1.Заява про зміну даних у виданому дозволі про виконання будівельних робіт за формою встановленого зразка;</p>
                   <p id="u2748-4">2.Засвідчені у встановленому порядку копії документів, що підтверджують зазначені зміни.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2741-4"><!-- content -->
                   <p id="u2741-2">&nbsp;Подається замовником (його уповноваженою особою) особисто</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2742-4"><!-- content -->
                   <p id="u2742-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u37268" href="assets/d9.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37268"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u1631"><!-- group -->
             <div class="clearfix grpelem" id="u3615-8"><!-- content -->
              <p id="u3615-2">3/3</p>
              <p id="u3615-4">Оберіть</p>
              <p id="u3615-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu2960"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u2975"><!-- none box -->
               <div class="popup_anchor" id="u2976popup">
                <div class="Thumb popup_element cardPlate shadow rounded-corners clearfix" id="u2976"><!-- group -->
                 <div class="clearfix grpelem" id="u2977-4"><!-- content -->
                  <p>Видача сертифіката у разі прийняття в експлуатацію закінченого будівництвом об’єкта</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u2963popup">
               <div class="ContainerGroup clearfix" id="u2963"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u2964"><!-- group -->
                 <div class="clearfix grpelem" id="pu2973-4"><!-- column -->
                  <div class="clearfix colelem" id="u2973-4"><!-- content -->
                   <p>Підстава для одержання</p>
                  </div>
                  <div class="clearfix colelem" id="u2971-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2972-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u2969-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu2966-4"><!-- column -->
                  <div class="clearfix colelem" id="u2966-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2967-4"><!-- content -->
                   <p id="u2967-2">Готовність до експлуатації закінченого будівництвом об’єкта, що за класом наслідків (відповідальності) належить до об’єктів із середніми (СС2) наслідками.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2968-6"><!-- content -->
                   <p id="u2968-2">1. Заява про прийняття в експлуатацію об’єкта та видачу сертифіката, за формою встановленого зразка;</p>
                   <p id="u2968-4">2. Акт готовності об’єкта до експлуатації відповідно до вимог статті 39 Закону України «Про регулювання містобудівної діяльності» за формою встановленого зразка.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2965-4"><!-- content -->
                   <p id="u2965-2">Через центр надання адміністративних послуг, або подається замовником (його уповноваженою особою) особисто*, або надсилається рекомендованим листом з описом вкладення чи через електронну систему здійснення декларативних та дозвільних процедур у будівництві.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u2970-4"><!-- content -->
                   <p id="u2970-2">Платна.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u37218" href="assets/d10.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u37218"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u1546"><!-- group -->
         <div class="clearfix grpelem" id="u30674-8"><!-- content -->
          <p id="u30674-2">2/3</p>
          <p id="u30674-4">Оберіть</p>
          <p id="u30674-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu7957"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u7959"><!-- none box -->
           <div class="popup_anchor" id="u7963popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u7963" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u7964"><!-- simple frame --></div><div class="clearfix colelem" id="u7965-4"><!-- content --><p><?php echo Vytag() ?></p></div></a>
           </div>
           <div class="popup_anchor" id="u7960popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u7960" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u7962"><!-- simple frame --></div><div class="clearfix colelem" id="u7961-4"><!-- content --><p id="u7961-2"><span class="ts-----1" id="u7961"><?php Derz1() ?></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u8055popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u8055" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u8077"><!-- simple frame --></div><div class="clearfix colelem" id="u8080-4"><!-- content --><p id="u8080-2"><span class="ts-----1" id="u8080"><?php Dow1dka() ?></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u8065popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u8065" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u8089"><!-- simple frame --></div><div class="clearfix colelem" id="u8092-4"><!-- content --><p id="u8092-2"><span class="ts-----1" id="u8092"><?php  Vidom() ?></span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u7966popup">
           <div class="ContainerGroup clearfix" id="u7966"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u7967"><!-- group -->
             <div class="clearfix grpelem" id="u7985-8"><!-- content -->
              <p id="u7985-2">3/3</p>
              <p id="u7985-4">Оберіть</p>
              <p id="u7985-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu7968"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u7970"><!-- none box -->
               <div class="popup_anchor" id="u8139popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8139"><!-- group -->
                 <div class="clearfix grpelem" id="u8274-4"><!-- content -->
                  <p>&nbsp;ВИДАЧА ВИТЯГУ З ТЕХНІЧНОЇ ДОКУМЕНТАЦІЇ ПРО НОРМАТИВНУ ГРОШОВУ ОЦІНКУ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8145popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8145"><!-- group -->
                 <div class="clearfix grpelem" id="u8310-6"><!-- content -->
                  <p>НАДАННЯ&nbsp; ВИТЯГУ</p>
                  <p>ПРО ЗЕМЕЛЬНУ ДІЛЯНКУ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9162popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u9162"><!-- group -->
                 <div class="clearfix grpelem" id="u9212-4"><!-- content -->
                  <p>НАДАННЯ ВИТЯГУ ПРО ЗЕМЛІ В МЕЖАХ ТЕРИТОРІЇ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9191popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u9191"><!-- group -->
                 <div class="clearfix grpelem" id="u9218-4"><!-- content -->
                  <p>ВНЕСЕННЯ ВІДОМОСТЕЙ ПРО МЕЖІ ЧАСТИНИ ЗЕМЕЛЬНОЇ ДІЛЯНКИ, З ВИДАЧЕЮ ВИТЯГУ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9197popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u9197"><!-- group -->
                 <div class="clearfix grpelem" id="u9224-4"><!-- content -->
                  <p>НАДАННЯ ВИТЯГУ ПРО ОБМЕЖЕННЯ У ВИКОРИСТАННІ ЗЕМЕЛЬ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u7974popup">
               <div class="ContainerGroup clearfix" id="u7974"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u8142"><!-- group -->
                 <div class="clearfix grpelem" id="pu8316-4"><!-- column -->
                  <div class="clearfix colelem" id="u8316-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8314-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8315-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8317-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8313-4"><!-- column -->
                  <div class="clearfix colelem" id="u8313-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8321-4"><!-- content -->
                   <p id="u8321-2">Звернення юридичної або фізичної особи землевласника або землекористувача, органів виконавчої влади та органів місцевого самоврядування</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8319-6"><!-- content -->
                   <p id="u8319-2">Звернення юридичної або фізичної особи землевласника або землекористувача, органів виконавчої влади та органів місцевого самоврядування</p>
                   <p id="u8319-4">Копія довіреності (доручення) – для уповноваженої особи</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8320-4"><!-- content -->
                   <p id="u8320-2">Подаються до центру надання адміністративних послуг особисто заявником (уповноваженою особою заявника), направлення поштою або замовлення послуги в електронному вигляді через офіційний веб-сайт Держгеокадастру.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8318-4"><!-- content -->
                   <p id="u8318-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u34502" href="https://e.land.gov.ua/services" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34502"><i class="fa fa-share-square"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u8148"><!-- group -->
                 <div class="clearfix grpelem" id="pu8280-4"><!-- column -->
                  <div class="clearfix colelem" id="u8280-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8278-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8279-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8281-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8277-4"><!-- column -->
                  <div class="clearfix colelem" id="u8277-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8285-4"><!-- content -->
                   <p id="u8285-2">Заява про надання відомостей з&nbsp; Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8283-8"><!-- content -->
                   <p id="u8283-2">1. Заява про надання відомостей з&nbsp; Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u8283-4">2. Документ, що підтверджує оплату послуг з надання витягу з Державного земельного кадастру про земельну ділянку</p>
                   <p id="u8283-6">3. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8284-6"><!-- content -->
                   <p id="u8284-2">Заява про надання відомостей з Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення або в електронній формі за власним електронним цифровим підписом (печаткою) заявника надсилається через Єдиний державний портал адміністративних послуг, у тому числі через інтегровану з ним інформаційну систему Держгеокадастру</p>
                   <p id="u8284-4">У разі подання заяви органом державної влади, органом місцевого самоврядування у заяві зазначаються підстави надання відповідної інформації з посиланням на норму закону, яка передбачає право відповідного органу запитувати таку інформацію, а також реквізити справи, у зв’язку з якою виникла потреба в отриманні інформації. Така заява розглядається у позачерговому порядку</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8282-4"><!-- content -->
                   <p id="u8282-2">Послуга платна (у випадку звернення органів виконавчої влади та органів місцевого самоврядування – безоплатна)</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8340"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u8340" href="assets/zd3.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8340"><i class="fa fa-download"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u34507" href="https://e.land.gov.ua/services" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34507"><i class="fa fa-share-square"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u9167"><!-- group -->
                 <div class="clearfix grpelem" id="pu9230-4"><!-- column -->
                  <div class="clearfix colelem" id="u9230-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u9228-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9229-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9231-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9227-4"><!-- column -->
                  <div class="clearfix colelem" id="u9227-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9235-4"><!-- content -->
                   <p id="u9235-2">Заява про надання відомостей з Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9233-8"><!-- content -->
                   <p id="u9233-2">1. Заява про надання відомостей з&nbsp; Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u9233-4">2. Документ, що підтверджує оплату послуг з надання витягу з Державного земельного кадастру про землі в межах території адміністративно-територіальних одиниць</p>
                   <p id="u9233-6">3. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9234-6"><!-- content -->
                   <p id="u9234-2">Заява про надання відомостей з Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                   <p id="u9234-4">У разі подання заяви органом державної влади, органом місцевого самоврядування у заяві зазначаються підстави надання відповідної інформації з посиланням на норму закону, яка передбачає право відповідного органу запитувати таку інформацію, а також реквізити справи, у зв’язку з якою виникла потреба в отриманні інформації. Така заява розглядається у позачерговому порядку</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9232-4"><!-- content -->
                   <p id="u9232-2">Послуга платна (у випадку звернення органів виконавчої влади та органів місцевого самоврядування – безоплатна)</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u9236" href="assets/zd12.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9236"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u9194"><!-- group -->
                 <div class="clearfix grpelem" id="pu9264-4"><!-- column -->
                  <div class="clearfix colelem" id="u9264-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u9262-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9263-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9265-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9261-4"><!-- column -->
                  <div class="clearfix colelem" id="u9261-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9269-4"><!-- content -->
                   <p id="u9269-2">Заява про внесення відомостей (змін до них) до Державного земельного кадастру про межі частини земельної ділянки, на яку поширюються права суборенди, сервітуту</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9267-12"><!-- content -->
                   <p id="u9267-2">1. Заява за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051 (форма заяви додається)*.</p>
                   <p id="u9267-4">2. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою).</p>
                   <p id="u9267-6">3. Документи, на підставі яких виникає право суборенди, сервітуту, із зазначенням меж частини земельної ділянки, на яку поширюється таке право.</p>
                   <p id="u9267-8">4. Документацію із землеустрою щодо встановлення меж частини земельної ділянки, на яку поширюється право суборенди, сервітуту у паперовій або електронній формі відповідно до вимог Закону України «Про землеустрій»</p>
                   <p id="u9267-10">5. Електронній документ, що містить відомості про результати робіт із землеустрою, які підлягають внесенню до Державного земельного кадастру, відповідно до вимог Закону України «Про Державний земельний кадастр»</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9268-4"><!-- content -->
                   <p id="u9268-2">Заява про внесення відомостей (змін до них) до Державного земельного кадастру про межі частини земельної ділянки, на яку поширюються права суборенди, сервітуту у паперовій формі з доданими документами подається до центру надання&nbsp; адміністративних послуг заявником або уповноваженою ним особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9266-4"><!-- content -->
                   <p id="u9266-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u9270" href="assets/zd13.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9270"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u9200"><!-- group -->
                 <div class="clearfix grpelem" id="pu9298-4"><!-- column -->
                  <div class="clearfix colelem" id="u9298-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u9296-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9297-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9299-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9295-4"><!-- column -->
                  <div class="clearfix colelem" id="u9295-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9303-4"><!-- content -->
                   <p id="u9303-2">Заява про надання відомостей з&nbsp; Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9301-8"><!-- content -->
                   <p id="u9301-2">1. Заява про надання відомостей з&nbsp; Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u9301-4">2. Документ, що підтверджує оплату послуг з надання витягу з Державного земельного кадастру про обмеження у використанні земель</p>
                   <p id="u9301-6">3. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9302-6"><!-- content -->
                   <p id="u9302-2">Заява про надання відомостей з Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                   <p id="u9302-4">У разі подання заяви органом державної влади, органом&nbsp; місцевого самоврядування у заяві зазначаються підстави надання відповідної інформації з посиланням на норму закону, яка передбачає право відповідного органу запитувати таку інформацію, а також реквізити справи, у зв’язку з якою виникла потреба&nbsp; в отриманні інформації. Така заява розглядається у позачерговому порядку.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9300-4"><!-- content -->
                   <p id="u9300-2">Послуга платна (у випадку звернення органів виконавчої влади та органів місцевого самоврядування – безоплатна)</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u9304" href="assets/zd14.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9304"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u7986"><!-- group -->
             <div class="PamphletWidget clearfix grpelem" id="pamphletu8476"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u8488"><!-- none box -->
               <div class="popup_anchor" id="u8489popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8489"><!-- group -->
                 <div class="clearfix grpelem" id="u8490-4"><!-- content -->
                  <p>РЕЄСТРАЦІЯ ЗЕМЕЛЬНОЇ ДІЛЯНКИ З ВИДАЧЕЮ ВИТЯГУ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8537popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8537"><!-- group -->
                 <div class="clearfix grpelem" id="u8566-4"><!-- content -->
                  <p>РЕЄСТРАЦІЯ ОБМЕЖЕНЬ У ВИКОРИСТАННІ ЗЕМЕЛЬ З ВИДАЧЕЮ ВИТЯГУ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u8477popup">
               <div class="ContainerGroup clearfix" id="u8477"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u8478"><!-- group -->
                 <div class="clearfix grpelem" id="pu8529-4"><!-- column -->
                  <div class="clearfix colelem" id="u8529-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8481-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8486-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u8479-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8480-4"><!-- column -->
                  <div class="clearfix colelem" id="u8480-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8530-4"><!-- content -->
                   <p id="u8530-2">Заява про державну реєстрацію земельної ділянки</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8484-10"><!-- content -->
                   <p id="u8484-2">1. Заява про державну реєстрацію земельної ділянки за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u8484-4">2. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                   <p id="u8484-6">3. Оригінал погодженої відповідно до законодавства документації із землеустрою, яка є підставою для формування земельної ділянки (разом з позитивним висновком державної експертизи землевпорядної документації у разі, коли така документація підлягає обов’язковій державній експертизі землевпорядної документації) у паперовій або електронній формі відповідно до вимог Закону України «Про землеустрій»</p>
                   <p id="u8484-8">4. Електронний документ, що містить відомості про результати робіт із землеустрою, які підлягають внесенню до Державного земельного кадастру, відповідно до вимог Закону України «Про Державний земельний кадастр»</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8483-4"><!-- content -->
                   <p id="u8483-2">Заява про державну реєстрацію земельної ділянки у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8485-4"><!-- content -->
                   <p id="u8485-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8482"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u8482" href="assets/zd7.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8482"><i class="fa fa-download"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u34512" href="https://e.land.gov.ua/services" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34512"><i class="fa fa-share-square"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u8542"><!-- group -->
                 <div class="clearfix grpelem" id="pu8577-4"><!-- column -->
                  <div class="clearfix colelem" id="u8577-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8570-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8571-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u8572-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8569-4"><!-- column -->
                  <div class="clearfix colelem" id="u8569-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8578-4"><!-- content -->
                   <p id="u8578-2">Заява про державну реєстрацію обмеження у використанні земель</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8574-12"><!-- content -->
                   <p id="u8574-2">1. Заява про державну реєстрацію обмеження у використанні земель за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u8574-4">2. Документація із землеустрою, інші документі, які є підставою для виникнення, зміни та припинення обмеження у використанні земель</p>
                   <p id="u8574-6">3. Документація із землеустрою та оцінки земель, інші документи, які є підставою для виникнення, зміни та припинення обмеження у використанні земель, в електронній формі відповідно до вимог Закону України «Про землеустрій»</p>
                   <p id="u8574-8">4. Електронний документ відповідно до вимог Закону України «Про Державний земельний кадастр»</p>
                   <p id="u8574-10">5. Документ, які підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8575-4"><!-- content -->
                   <p id="u8575-2">Заява про державну реєстрацію обмеження у використанні земель у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8573-4"><!-- content -->
                   <p id="u8573-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8576" href="assets/zd8.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8576"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
             <div class="clearfix grpelem" id="u30983-8"><!-- content -->
              <p id="u30983-2">3/3</p>
              <p id="u30983-4">Оберіть</p>
              <p id="u30983-6">послугу</p>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u8062"><!-- group -->
             <div class="PamphletWidget clearfix grpelem" id="pamphletu8603"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u8630"><!-- none box -->
               <div class="popup_anchor" id="u8631popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8631"><!-- group -->
                 <div class="clearfix grpelem" id="u8632-4"><!-- content -->
                  <p>ВИДАЧА ДОВІДКИ З КІЛЬКІСНОГО ОБЛІКУ ЗЕМЕЛЬ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8633popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8633"><!-- group -->
                 <div class="clearfix grpelem" id="u8634-4"><!-- content -->
                  <p>НАДАННЯ ВІДОМОСТЕЙ У ФОРМІ ДОВІДКИ, ЩО МІСТИТЬ УЗАГАЛЬНЕНУ ІНФОРМАЦІЮ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8707popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8707"><!-- group -->
                 <div class="clearfix grpelem" id="u8771-4"><!-- content -->
                  <p>ВИДАЧА ДОВІДКИ ПРО НАЯВНІСТЬ ТА РОЗМІР ЗЕМЕЛЬНОЇ ЧАСТКИ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u8606popup">
               <div class="ContainerGroup clearfix" id="u8606"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u8618"><!-- group -->
                 <div class="clearfix grpelem" id="pu8627-4"><!-- column -->
                  <div class="clearfix colelem" id="u8627-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8625-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8619-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u8621-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8623-4"><!-- column -->
                  <div class="clearfix colelem" id="u8623-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8628-4"><!-- content -->
                   <p id="u8628-2">Заява суб’єкта звернення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8624-4"><!-- content -->
                   <p id="u8624-2">Заява</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8626-4"><!-- content -->
                   <p id="u8626-2">Заява про надання довідки з державної статистичної звітності з кількісного обліку земель про наявність земель та розподіл їх за власниками земель, землекористувачами, угіддями у паперовій формі подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається поштою.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8622-4"><!-- content -->
                   <p id="u8622-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8620"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u8620" href="assets/zd9.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8620"><i class="fa fa-download"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u34527" href="https://e.land.gov.ua/services" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34527"><i class="fa fa-share-square"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u8607"><!-- group -->
                 <div class="clearfix grpelem" id="pu8610-4"><!-- column -->
                  <div class="clearfix colelem" id="u8610-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8613-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8615-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u8612-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8608-4"><!-- column -->
                  <div class="clearfix colelem" id="u8608-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8617-4"><!-- content -->
                   <p id="u8617-2">Заява про надання відомостей з&nbsp; Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8609-8"><!-- content -->
                   <p id="u8609-2">1. Заява про надання відомостей з&nbsp; Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012&nbsp; № 1051;</p>
                   <p id="u8609-4">2. Документ, що підтверджує оплату послуг з надання довідки, що містить узагальнену інформацію про землі (території)</p>
                   <p id="u8609-6">3. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8614-6"><!-- content -->
                   <p id="u8614-2">Заява про надання відомостей з Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення.</p>
                   <p id="u8614-4">У разі подання заяви органом державної влади, органом&nbsp; місцевого самоврядування у заяві зазначаються підстави надання відповідної інформації з посиланням на норму закону, яка передбачає право відповідного органу запитувати таку інформацію, а також реквізити справи, у зв’язку з якою виникла потреба&nbsp; в отриманні інформації. Така заява розглядається у позачерговому порядку.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8616-4"><!-- content -->
                   <p id="u8616-2">Платно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8611" href="assets/zd10.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8611"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u8712"><!-- group -->
                 <div class="clearfix grpelem" id="pu8744-4"><!-- column -->
                  <div class="clearfix colelem" id="u8744-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8737-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8738-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u8739-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8736-4"><!-- column -->
                  <div class="clearfix colelem" id="u8736-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8745-4"><!-- content -->
                   <p id="u8745-2">Заява про надання відомостей з&nbsp; Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8741-6"><!-- content -->
                   <p id="u8741-2">1. Заява про надання відомо��тей з Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженого постановою Кабінету Міністрів України від 17.10.2012 року № 1051</p>
                   <p id="u8741-4">2. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8742-4"><!-- content -->
                   <p id="u8742-2">Заява про надання відомостей з Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8740-4"><!-- content -->
                   <p id="u8740-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8743" href="assets/zd11.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8743"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
             <div class="clearfix grpelem" id="u30986-8"><!-- content -->
              <p id="u30986-2">3/3</p>
              <p id="u30986-4">Оберіть</p>
              <p id="u30986-6">послугу</p>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u8068"><!-- group -->
             <div class="PamphletWidget clearfix grpelem" id="pamphletu8776"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u8778"><!-- none box -->
               <div class="popup_anchor" id="u8787popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8787"><!-- group -->
                 <div class="clearfix grpelem" id="u8788-4"><!-- content -->
                  <p>ВИДАЧА ВІДОМОСТЕЙ З ДОКУМЕНТАЦІЇ ІЗ ЗЕМЛЕУСТРОЮ, ЩО ВКЛЮЧЕНА ДО ДЕРЖАВНОГО ФОНДУ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8793popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8793"><!-- group -->
                 <div class="clearfix grpelem" id="u8794-4"><!-- content -->
                  <p>НАДАННЯ ВІДОМОСТЕЙ З ДЕРЖАВНОГО ЗЕМЕЛЬНОГО КАДАСТРУ У ФОРМІ ВИКОПІЮВАННЯ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8781popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8781"><!-- group -->
                 <div class="clearfix grpelem" id="u8782-4"><!-- content -->
                  <p>ВИПРАВЛЕННЯ ТЕХНІЧНОЇ ПОМИЛКИ У ВІДОМОСТЯХ З ДЕРЖАВНОГО ЗЕМЕЛЬНОГО КАДАСТРУ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8779popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8779"><!-- group -->
                 <div class="clearfix grpelem" id="u8780-4"><!-- content -->
                  <p>ВНЕСЕННЯ ВІДОМОСТЕЙ (ЗМІН ДО НИХ) ПРО ЗЕМЛІ В МЕЖАХ ТЕРИТОРІЙ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8783popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8783"><!-- group -->
                 <div class="clearfix grpelem" id="u8784-4"><!-- content -->
                  <p>ВНЕСЕННЯ ВІДОМОСТЕЙ ПРО ОБМЕЖЕННЯ У ВИКОРИСТАННІ ЗЕМЕЛЬ</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u8791popup">
                <div class="Thumb popup_element cardPlate rounded-corners shadow clearfix" id="u8791"><!-- group -->
                 <div class="clearfix grpelem" id="u8792-4"><!-- content -->
                  <p>ВНЕСЕННЯ ВІДОМОСТЕЙ (ЗМІН ДО НИХ) ПРО ЗЕМЕЛЬНУ ДІЛЯНКУ</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u8797popup">
               <div class="ContainerGroup clearfix" id="u8797"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u8830"><!-- group -->
                 <div class="clearfix grpelem" id="pu8837-4"><!-- column -->
                  <div class="clearfix colelem" id="u8837-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8831-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8832-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8838-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8836-4"><!-- column -->
                  <div class="clearfix colelem" id="u8836-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8835-4"><!-- content -->
                   <p id="u8835-2">Запит про надання відомостей з документації із землеустрою, що включена до Державного фонду документації із землеустрою</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8839-4"><!-- content -->
                   <p id="u8839-2">Запит про надання відомостей з документації із землеустрою, що включена до Державного фонду документації із землеустрою, у довільній формі</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8833-4"><!-- content -->
                   <p id="u8833-2">Подаються до центру надання адміністративних послуг особисто заявником (уповноваженою особою заявника), направлення поштою</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8834-4"><!-- content -->
                   <p id="u8834-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u34552" href="https://e.land.gov.ua/services" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34552"><i class="fa fa-share-square"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u8798"><!-- group -->
                 <div class="clearfix grpelem" id="pu8806-4"><!-- column -->
                  <div class="clearfix colelem" id="u8806-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8799-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8808-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8805-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8802-4"><!-- column -->
                  <div class="clearfix colelem" id="u8802-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8801-4"><!-- content -->
                   <p id="u8801-2">Заява про надання відомостей з Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8807-8"><!-- content -->
                   <p id="u8807-2">1. Заява про надання відомостей з Державного земельного кадастру за визначеною формою, встановленою Порядком ведення Державного земельного кадастру, затвердженого постановою Кабінету Міністрів України від 17 жовтня 2012 року № 1051.</p>
                   <p id="u8807-4">2. Документ, що підтверджує оплату послуг з надання викопіювання з картографічної основи Державного земельного кадастру, кадастрової карти (плану)</p>
                   <p id="u8807-6">3. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особисто)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8800-6"><!-- content -->
                   <p id="u8800-2">Заява про надання відомостей з Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення.</p>
                   <p id="u8800-4">У разі подання заяви органом державної влади, органом&nbsp; місцевого самоврядування у заяві зазначаються підстави надання відповідної інформації з посиланням на норму закону, яка передбачає право відповідного органу запитувати таку інформацію, а також реквізити справи, у зв’язку з якою виникла потреба&nbsp; в отриманні інформації. Така заява розглядається у позачерговому порядку.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8804-4"><!-- content -->
                   <p id="u8804-2">Платно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8803"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u8803" href="assets/zd1.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8803"><i class="fa fa-download"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u34557" href="https://e.land.gov.ua/services" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u34557"><i class="fa fa-share-square"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u8862"><!-- group -->
                 <div class="clearfix grpelem" id="pu8868-4"><!-- column -->
                  <div class="clearfix colelem" id="u8868-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8872-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8864-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8871-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8867-4"><!-- column -->
                  <div class="clearfix colelem" id="u8867-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8870-4"><!-- content -->
                   <p id="u8870-2">Виявлення фізичною або юридичною особою технічної помилки (описка, друкарська, граматична, арифметична чи інша помилка) у витязі, довідці з Державного земельного кадастру, викопіюванні з картографічних матеріалів Державного земельного кадастру.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8863-6"><!-- content -->
                   <p id="u8863-2">1. Повідомлення про виявлення технічної помилки із викладенням суті виявлених помилок за формою, що додається.</p>
                   <p id="u8863-4">2. Документи, що містять зазначені у повідомленні технічні помилки, та документи, що підтверджують такі помилки і містять правильну редакцію відповідних відомостей.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8869-4"><!-- content -->
                   <p id="u8869-2">Повідомлення разом з доданими до нього документами подається до центру надання адміністративних послуг заінтересованою особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8866-4"><!-- content -->
                   <p id="u8866-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8865" href="assets/zd2.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8865"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u8873"><!-- group -->
                 <div class="clearfix grpelem" id="pu8874-4"><!-- column -->
                  <div class="clearfix colelem" id="u8874-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8879-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8875-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8880-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8878-4"><!-- column -->
                  <div class="clearfix colelem" id="u8878-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8881-4"><!-- content -->
                   <p id="u8881-2">Заява про внесення відомостей до Державного земельного кадастру відомостей (змін до них) про землі в межах територій адміністративно-територіальних одиниць</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8877-12"><!-- content -->
                   <p id="u8877-2">1. Заява про внесення відомостей до Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u8877-4">2. Документація із землеустрою та оцінки земель, інші документи, які є підставою для внесення відомостей (змін до них) до Державного земельного кадастру</p>
                   <p id="u8877-6">3. Документація із землеустрою та оцінки земель, інші документи, які є підставою для внесення відомостей (змін до них) до Державного земельного кадастру, в електронній формі відповідно до вимог Закону України «Про землеустрій»</p>
                   <p id="u8877-8">4. Електронний документ, що містить відомості про результати робіт із землеустрою, які підлягають внесенню до Державного земельного кадастру, відповідно до вимог Закону України «Про Державний земельний кадастр»</p>
                   <p id="u8877-10">5. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8876-4"><!-- content -->
                   <p id="u8876-2">Заява про внесення відомостей (змін до них) до Державного земельного кадастру у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8883-4"><!-- content -->
                   <p id="u8883-2">Безоплатно</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8882" href="assets/zd4.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8882"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u8851"><!-- group -->
                 <div class="clearfix grpelem" id="pu8857-4"><!-- column -->
                  <div class="clearfix colelem" id="u8857-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8853-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8858-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8861-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8852-4"><!-- column -->
                  <div class="clearfix colelem" id="u8852-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8854-4"><!-- content -->
                   <p id="u8854-2">Заява органу виконавчої влади, органу місцевого самоврядування про внесення відомостей про обмеження у використанні земель, встановлені законами та прийнятими відповідно до них нормативно-правовими актами</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8859-10"><!-- content -->
                   <p id="u8859-2">1. Заява про внесення відомостей про обмеження у використанні земель, встановлені законами та прийнятими відповідно до них нормативно-правовими актами за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u8859-4">2. Документація із землеустрою у паперовій та електронній формі відповідно до вимог Закону України «Про землеустрій», інші документи, які згідно з пунктом 102 Порядку ведення Державного земельного кадастру, затвердженого постановою Кабінету Міністрів України від 17.10.2012 № 1051 є підставою для виникнення, зміни та припинення обмеження у використанні земель, а саме: схеми землеустрою і техніко-економічні обґрунтування використання та охорони земель адміністративно-територіальних одиниць; проекти землеустрою щодо створення нових та впорядкування існуючих землеволодінь і землекористувачів; проекти землеустрою щодо забезпечення еколого-економічного обґрунтування сівозміни та впорядкування угідь; проекти землеустрою щодо відведення земельних ділянок; технічна документація із землеустрою щодо&nbsp; встановлення меж земельної ділянки в натурі (на місцевості); інша документація із землеустрою відповідно до статті 25 Закону України «Про землеустрій»; договір; рішення суду</p>
                   <p id="u8859-6">3. Електронний документ відповідно до вимог Закону України «Про Державний земельний кадастр»</p>
                   <p id="u8859-8">4. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8860-4"><!-- content -->
                   <p id="u8860-2">Заява про внесення відомостей про обмеження у використанні земель, встановлені законами та прийнятими відповідно до них нормативно-правовими актами у паперовій формі разом з доданими до неї документами подається до центру надання адміністративних послуг заінтересованою особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8855-4"><!-- content -->
                   <p id="u8855-2">Безоплатно</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8856" href="assets/zd5.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8856"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u8840"><!-- group -->
                 <div class="clearfix grpelem" id="pu8846-4"><!-- column -->
                  <div class="clearfix colelem" id="u8846-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u8844-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8847-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u8843-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu8841-4"><!-- column -->
                  <div class="clearfix colelem" id="u8841-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8848-4"><!-- content -->
                   <p id="u8848-2">Заява про внесення відомостей (змін до них) до Державного земельного кадастру</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8850-10"><!-- content -->
                   <p id="u8850-2">1. Заява про внесення відомостей (змін до них) до Державного земельного кадастру за формою, встановленою Порядком ведення Державного земельного кадастру, затвердженим постановою Кабінету Міністрів України від 17.10.2012 № 1051</p>
                   <p id="u8850-4">2. Документ, який підтверджує повноваження діяти від імені заявника (у разі подання заяви уповноваженою заявником особою)</p>
                   <p id="u8850-6">3. Документацію із землеустрою, яка є підставою для внесення відомостей (змін до них) до Державного земельного кадастру про земельну ділянку у паперовій або електронній формі відповідно до вимог Закону України «Про землеустрій»</p>
                   <p id="u8850-8">4. Електронний документ, що містить відомості про результати робіт із землеустрою, які підлягають внесенню до Державного земельного кадастру, відповідно до вимог Закону України «Про Державний земельний кадастр»</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8842-4"><!-- content -->
                   <p id="u8842-2">Заява про внесення відомостей (змін до них) до Державного земельного кадастру про земельну ділянку у паперовій формі з доданими документами подається до центру надання адміністративних послуг заявником або уповноваженою ним особою особисто або надсилається рекомендованим листом з описом вкладення та повідомленням про вручення</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u8849-4"><!-- content -->
                   <p id="u8849-2">Безоплатно</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u8845" href="assets/zd6.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u8845"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
             <div class="clearfix grpelem" id="u30980-8"><!-- content -->
              <p id="u30980-2">3/3</p>
              <p id="u30980-4">Оберіть</p>
              <p id="u30980-6">послугу</p>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="Container invi clearfix grpelem" id="u351"><!-- group -->
         <div class="clearfix grpelem" id="u1091-8"><!-- content -->
          <p id="u1091-2">2/3</p>
          <p id="u1091-4">Оберіть</p>
          <p id="u1091-6">категорію</p>
         </div>
         <div class="PamphletWidget clearfix grpelem" id="pamphletu7812"><!-- none box -->
          <div class="ThumbGroup clearfix grpelem" id="u7813"><!-- none box -->
           <div class="popup_anchor" id="u7814popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u7814" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u7815"><!-- simple frame --></div><div class="clearfix colelem" id="u7816-6"><!-- content --><p id="u7816-2"><span class="ts-----1" id="u7816"><?php Pasport() ?></span></p><p id="u7816-4"><span class="ts-----1" id="u7816-3"></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u7898popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u7898" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u7951"><!-- simple frame --></div><div class="clearfix colelem" id="u7954-6"><!-- content --><p id="u7954-2"><span class="ts-----1" id="u7954"><?php  Dovidka()  ?></span></p><p id="u7954-4"><span class="ts-----1" id="u7954-3"></span></p></div></a>
           </div>
           <div class="popup_anchor" id="u9454popup">
            <a class="nonblock nontext Thumb popup_element anim_swing cardPlate rounded-corners shadow clearfix" id="u9454" href="start.php#s1"><!-- column --><div class="museBGSize colelem" id="u9470"><!-- simple frame --></div><div class="clearfix colelem" id="u9473-4"><!-- content --><p id="u9473-2"><span class="ts-----1" id="u9473"><?php Opelenya() ?></span></p></div></a>
           </div>
          </div>
          <div class="popup_anchor" id="u7817popup">
           <div class="ContainerGroup clearfix" id="u7817"><!-- stack box -->
            <div class="Container invi clearfix grpelem" id="u7818"><!-- group -->
             <div class="clearfix grpelem" id="u30689-8"><!-- content -->
              <p id="u30689-2">3/3</p>
              <p id="u30689-4">Оберіть</p>
              <p id="u30689-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu7819"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u7822"><!-- none box -->
               <div class="popup_anchor" id="u7823popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u7823"><!-- group -->
                 <div class="clearfix grpelem" id="u7824-4"><!-- content -->
                  <p>Видача будівельного&nbsp; паспорта&nbsp; забудови земельної ділянки</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9347popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u9347"><!-- group -->
                 <div class="clearfix grpelem" id="u9388-4"><!-- content -->
                  <p>Видача містобудівних умов та обмежень забудови земельної ділянки</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u7826popup">
               <div class="ContainerGroup clearfix" id="u7826"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u7827"><!-- group -->
                 <div class="clearfix grpelem" id="pu9335-4"><!-- column -->
                  <div class="clearfix colelem" id="u9335-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u9332-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u7830-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u7833-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu7834-4"><!-- column -->
                  <div class="clearfix colelem" id="u7834-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9336-4"><!-- content -->
                   <p id="u9336-2">Заява суб'єкта звернення на отримання будівельного паспорта забудови земельної ділянки</p>
                  </div>
                  <div class="Text clearfix colelem" id="u7835-16"><!-- content -->
                   <p id="u7835-2">1) Заяву на видачу будівельного паспорта зі згодою замовника на обробку персональних даних;</p>
                   <p id="u7835-4">2) Засвідчену в установленому порядку копію документа, що засвідчує право власності або користування земельною ділянкою;</p>
                   <p id="u7835-6">3) Засвідчену в установленому порядку згоду співвласників земельної ділянки (житлового будинку) на забудову зазначеної земельної ділянки(у разі необхідності);</p>
                   <p id="u7835-8">4) Ескіз намірів забудови (місце розташування будівель та споруд на земельній ділянці, фасади, максимальні відмітки висотності, відстані до сусідніх земельних ділянок);</p>
                   <p id="u7835-10">5) Проект будівництва (за наявності);</p>
                   <p id="u7835-12">6) План викопіюваня;</p>
                   <p id="u7835-14">7) Фотофіксація.</p>
                  </div>
                  <div class="Text clearfix colelem" id="u7831-4"><!-- content -->
                   <p id="u7831-2">Документи надаються особисто замовником або через уповноважену особу</p>
                  </div>
                  <div class="Text clearfix colelem" id="u7829-4"><!-- content -->
                   <p id="u7829-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u7832" href="assets/%d0%b1%d1%83%d0%b4-%d0%bf%d0%b0%d1%81%d0%bf%d0%be%d1%80%d1%82.doc"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u7832"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u9352"><!-- group -->
                 <div class="clearfix grpelem" id="pu9402-4"><!-- column -->
                  <div class="clearfix colelem" id="u9402-4"><!-- content -->
                   <p>Підстава</p>
                  </div>
                  <div class="clearfix colelem" id="u9395-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9396-4"><!-- content -->
                   <p>Порядок та спосіб подання документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9397-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9394-4"><!-- column -->
                  <div class="clearfix colelem" id="u9394-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9403-4"><!-- content -->
                   <p id="u9403-2">Намір здійснити забудову земельних ділянок</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9399-16"><!-- content -->
                   <p id="u9399-2">1) Заява;</p>
                   <p id="u9399-4">2) Засвідчена в установленому порядку копія документа про право власності (користування) земельною ділянкою;</p>
                   <p id="u9399-6">3) Викопіювання з топографо-геодезичного плану М 1:2000;</p>
                   <p id="u9399-8">4) Кадастрова довідка з містобудівного кадастру (у разі наявності);</p>
                   <p id="u9399-10">5) Черговий кадастровий план (витяг із земельного кадастру - за умови відсутності містобудівного кадастру);</p>
                   <p id="u9399-12">6) Фотофіксація земельної ділянки (з оточенням);</p>
                   <p id="u9399-14">7) Містобудівний розрахунок з техніко-економічними показниками запланованого об'єкта будівництва.</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9400-4"><!-- content -->
                   <p id="u9400-2">Одержувач адміністративної послуги подає документи вказані в пункті 9 до відділу містобудування та архітектури райдержадміністрації особисто (через представника)</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9398-4"><!-- content -->
                   <p id="u9398-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9445-6"><!-- column -->
                  <div class="clearfix colelem" id="u9445-6"><!-- content -->
                   <p>Фіз.</p>
                   <p>особам</p>
                  </div>
                  <a class="nonblock nontext size_fixed colelem" id="u9401" href="assets/%d0%bc%d1%96%d1%81%d1%82%d0%be%d0%b1%d1%83%d0%b4.-%d1%83%d0%bc%d0%be%d0%b2%d0%b8-%d1%84%d1%96%d0%b7.%d0%be%d1%81%d0%be%d0%b1%d0%b0.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9401"><i class="fa fa-download"></i></div>
  	  </a>
                  <div class="clearfix colelem" id="u9451-6"><!-- content -->
                   <p>Юр.</p>
                   <p>особам</p>
                  </div>
                  <a class="nonblock nontext size_fixed colelem" id="u9431" href="assets/%d0%bc%d1%96%d1%81%d1%82%d0%be%d0%b1%d1%83%d0%b4.-%d1%83%d0%bc%d0%be%d0%b2%d0%b8-%d1%8e%d1%80.%d0%be%d1%81%d0%be%d0%b1%d0%b0.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9431"><i class="fa fa-download"></i></div>
  	  </a>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u7905"><!-- group -->
             <div class="clearfix grpelem" id="u30698-8"><!-- content -->
              <p id="u30698-2">3/3</p>
              <p id="u30698-4">Оберіть</p>
              <p id="u30698-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu9587"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u9599"><!-- none box -->
               <div class="popup_anchor" id="u9600popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u9600"><!-- group -->
                 <div class="clearfix grpelem" id="u9601-4"><!-- content -->
                  <p>Довідка про перебування (не перебування) на квартирному обліку</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9701popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u9701"><!-- group -->
                 <div class="clearfix grpelem" id="u9729-4"><!-- content -->
                  <p>Видача свідоцтва про право власності на житло</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u9588popup">
               <div class="ContainerGroup clearfix" id="u9588"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u9589"><!-- group -->
                 <div class="clearfix grpelem" id="pu9592-4"><!-- column -->
                  <div class="clearfix colelem" id="u9592-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9597-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u9590-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9593-4"><!-- column -->
                  <div class="clearfix colelem" id="u9593-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9595-6"><!-- content -->
                   <p id="u9595-2">1. Заява на надання довідки.</p>
                   <p id="u9595-4">2. Ксерокопія паспортів всіх членів сім’ї з пред’явленням оригіналу.</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9591-4"><!-- content -->
                   <p id="u9591-2">5 днів</p>
                  </div>
                  <div class="Text clearfix colelem" id="u9596-4"><!-- content -->
                   <p id="u9596-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u9594" href="assets/%d0%ba%d0%b2%d0%b0%d1%80%d1%82%d0%b8%d1%80%d0%bd%d0%b8%d0%b9-%d0%be%d0%b1%d0%bb%d1%96%d0%ba.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9594"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
                <div class="Container invi clearfix grpelem" id="u9706"><!-- group -->
                 <div class="clearfix grpelem" id="pu9733-4"><!-- column -->
                  <div class="clearfix colelem" id="u9733-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9734-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u9735-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9732-4"><!-- column -->
                  <div class="clearfix colelem" id="u9732-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 colelem" id="u10986"><!-- custom html -->
                   <div class="card">
  <div class="container">
    <p><p>1. Заява на приватизацію житла.<br />2. Довідка про склад сім&rsquo;ї.<br />3.Копія ордеру про надання жилої площі, або копії підтверджуючих документів про надання житлової площі.<br />4. Документ, що підтверджує невикористання громадянином житлових чеків для приватизації державного житлового фонду(довідка з попередніх місць реєстрації(проживання) з 22.07.1992 року щодо невикористання права на приватизацію державного житлового фонду).<br />5.Довідка про участь в приватизації (при умові прийняття участі в приватизації).<br />6. Копія технічного паспорту на квартиру з пред&rsquo;явленням оригіналу.<br />7.Копія форми Б, завірена належним чином (поквартирна карточка).<br />8.Ксерокопія паспортів всіх членів сім&rsquo;ї з пред&rsquo;явленням оригіналу.<br />9. За неповнолітніх членів сім&rsquo;ї наймача рішення щодо приватизації житла приймають батьки(усиновлювачі) або піклувальники;згоду на участь у приватизації дітей вони засвідчують своїми підписами у заяві біля прізвища дитини; якщо дитини віком від 14 до 18 років(настає неповна цивільна дієздатність особи), додатково до заяви додається письмова нотаріально засвідчена згода батьків(усиновлювачів) або піклувальників.<br />10. Нотаріально посвідчена заява про місце реєстрації або проживання(заява надається у разі відсутності підтверджуючих документів частини періоду реєстрації місця проживання).<br />11. У разі відмови квартиронаймачів приймати участь у приватизації квартири надається відповідна нотаріально посвідчена заява-відмова.<br />12. У разі зміни основного наймача або відмови основного наймача приймати участь у приватизації квартири, надається копія рішення про переоформлення особового рахунку або витяг з рішення.<br />13.Якщо квартира знаходиться у ветхому будинку необхідний дозвіл на приватизацію такої квартири(рішення виконкому міської ради).<br />14. Додаткові відомості та документи для уточнення даних надаються у разі необхідності.</p></p>
  </div>
</div>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9738-4"><!-- content -->
                   <p id="u9738-2">30 днів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9736-4"><!-- content -->
                   <p id="u9736-2">Безоплатно.</p>
                  </div>
                 </div>
                 <a class="nonblock nontext size_fixed grpelem" id="u9739" href="assets/%d0%bf%d1%80%d0%b8%d0%b2%d0%b0%d1%82%d0%b8%d0%b7%d0%b0%d1%86%d1%96%d1%8f-%d0%b6%d0%b8%d1%82%d0%bb%d0%b0.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9739"><i class="fa fa-download"></i></div>
  	  </a>
                </div>
               </div>
              </div>
             </div>
            </div>
            <div class="Container invi clearfix grpelem" id="u9461"><!-- group -->
             <div class="clearfix grpelem" id="u30701-8"><!-- content -->
              <p id="u30701-2">3/3</p>
              <p id="u30701-4">Оберіть</p>
              <p id="u30701-6">послугу</p>
             </div>
             <div class="PamphletWidget clearfix grpelem" id="pamphletu9648"><!-- none box -->
              <div class="ThumbGroup clearfix grpelem" id="u9661"><!-- none box -->
               <div class="popup_anchor" id="u9662popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u9662"><!-- group -->
                 <div class="clearfix grpelem" id="u9663-4"><!-- content -->
                  <p>Відключення споживачів&nbsp; від мереж централізованого опалення та гарячої води</p>
                 </div>
                </div>
               </div>
               <div class="popup_anchor" id="u9758popup">
                <div class="Thumb popup_element cardPlate shadow clearfix" id="u9758"><!-- group -->
                 <div class="clearfix grpelem" id="u9786-4"><!-- content -->
                  <p>Надання дозволу на встановлення газової колонки</p>
                 </div>
                </div>
               </div>
              </div>
              <div class="popup_anchor" id="u9650popup">
               <div class="ContainerGroup clearfix" id="u9650"><!-- stack box -->
                <div class="Container invi clearfix grpelem" id="u9651"><!-- group -->
                 <div class="clearfix grpelem" id="pu9655-4"><!-- column -->
                  <div class="clearfix colelem" id="u9655-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9654-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u9652-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9656-4"><!-- column -->
                  <div class="clearfix colelem" id="u9656-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9659-6"><!-- content -->
                   <p id="u9659-2">1. Заява.</p>
                   <p id="u9659-4">2. Довідки про відсутність заборгованості за житлово-комунальні послуги.</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9657-4"><!-- content -->
                   <p id="u9657-2">30 днів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9658-4"><!-- content -->
                   <p id="u9658-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9653"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u9653" href="assets/%d1%96%d0%bd%d0%b4%d0%b8%d0%b2%d1%96%d0%b4%d1%83%d0%b0%d0%bb%d1%8c%d0%bd%d0%b5-%d0%be%d0%bf%d0%b0%d0%bb%d0%b5%d0%bd%d0%bd%d1%8f.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9653"><i class="fa fa-download"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u35577" href="http://api.berdychiv.com.ua/%D0%B2%D1%96%D0%B4%D0%BA%D0%BB%D1%8E%D1%87%D0%B5%D0%BD%D0%BD%D1%8F/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35577"><i class="fa fa-share-square"></i></div>
  	  </a>
                 </div>
                </div>
                <div class="Container invi clearfix grpelem" id="u9763"><!-- group -->
                 <div class="clearfix grpelem" id="pu9790-4"><!-- column -->
                  <div class="clearfix colelem" id="u9790-4"><!-- content -->
                   <p>Перелік документів</p>
                  </div>
                  <div class="clearfix colelem" id="u9791-4"><!-- content -->
                   <p>Термін виконання</p>
                  </div>
                  <div class="clearfix colelem" id="u9792-4"><!-- content -->
                   <p>Платність</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9789-4"><!-- column -->
                  <div class="clearfix colelem" id="u9789-4"><!-- content -->
                   <p>Умови отримання адміністративної послуги</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9794-12"><!-- content -->
                   <p id="u9794-2">1. Заява</p>
                   <p id="u9794-4">2. Розгортка димових та вентиляційних каналів</p>
                   <p id="u9794-6">3. Акт перевірки&nbsp; димових та вентиляційних каналів</p>
                   <p id="u9794-8">4. Дозвіл балансоутримовача.</p>
                   <p id="u9794-10">5. Технічні умови МКП «Бердичівводоканал».</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9795-4"><!-- content -->
                   <p id="u9795-2">30 днів</p>
                  </div>
                  <div class="Text-1 clearfix colelem" id="u9793-4"><!-- content -->
                   <p id="u9793-2">Безоплатно.</p>
                  </div>
                 </div>
                 <div class="clearfix grpelem" id="pu9796"><!-- column -->
                  <a class="nonblock nontext size_fixed colelem" id="u9796" href="assets/%d0%b4%d0%be%d0%b7%d0%b2%d1%96%d0%bb-%d0%bd%d0%b0-%d0%b3%d0%b0%d0%b7.%d0%ba%d0%be%d0%bb%d0%be%d0%bd%d0%ba%d1%83.docx"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9796"><i class="fa fa-download"></i></div>
  	  </a>
                  <a class="nonblock nontext size_fixed colelem" id="u35664" href="http://api.berdychiv.com.ua/%D0%BD%D0%B0%D0%B4%D0%B0%D0%BD%D0%BD%D1%8F-%D0%B4%D0%BE%D0%B7%D0%B2%D0%BE%D0%BB%D1%83/" target="_blank" title="Отримати послугу"><!-- custom html -->
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u35664"><i class="fa fa-share-square"></i></div>
  	  </a>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="PamphletWidget clearfix widget_invisible grpelem" id="pamphletu9815" data-islightbox="true"><!-- none box -->
      <div class="ThumbGroup clearfix grpelem" id="u9816"><!-- none box -->
       <div class="popup_anchor" id="u9818popup">
        <div class="Thumb popup_element clearfix" id="u9818"><!-- group -->
         <div class="size_fixed grpelem" id="u9869"><!-- custom html -->
          
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9869"><i class="fa fa-info-circle"></i></div>
  	  
         </div>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u9824popup" data-lightbox="true">
       <div class="ContainerGroup rgba-background clearfix" id="u9824"><!-- stack box -->
        <div class="Container clearfix grpelem" id="u9825"><!-- group -->
         <div class="shadow rounded-corners clearfix grpelem" id="u9874-14"><!-- content -->
          <p id="u9874-2">До уваги представників громадських організацій, профспілкових організацій та осередків політичних партій!</p>
          <p id="u9874-4">Зареєструвати громадські формування, первинну профспілкову організацію та осередок політичної партії відтепер можна через Центр надання адміністративних послуг (ЦНАП)</p>
          <p id="u9874-6">На виконання вимог Закону України «Про державну реєстрацію юридичних осіб, фізичних осіб-підприємців та громадських формувань» наказом Міністерства юстиції України від 09 лютого 2016 року № 359/5 затверджено Порядок державної реєстрації юридичних осіб, фізичних осіб – підприємців та громадських формувань, що не мають статусу юридичної особи (далі – Порядок), розділом ІІІ якого врегульовано особливості проведення державної реєстрації громадських формувань, а саме передбачено що державна реєстрація може проводитись за заявою заявника шляхом звернення до центрів надання адміністративних послуг, що забезпечують прийняття та видачу відповідно до цього Порядку документів під час державної реєстрації.</p>
          <p id="u9874-8">Звертаємо увагу, що наразі технічними засобами ведення Єдиного державного реєстру юридичних осіб, фізичних осіб – підприємців та громадських формувань Центром надання адміністративних послуг організовується можливість прийняття та обробки документів уповноваженими особами громадських формувань для державної реєстрації громадських об’єднань (громадських організацій та громадських спілок) зі статусом юридичної особи. Розгляд таких документів та прийняття по них рішень здійснює Головне територіальне управління юстиції у Житомирській області.</p>
          <p id="u9874-10">ВАЖЛИВО!</p>
          <p id="u9874-12">Наказом Головного територіального управління юстиції у Житомирській області від 16.01.2017 №7/6 &quot;Про затвердження інформаційних карток адміністративних послуг у сфері державної реєстрації юридичних осіб, громадських формувань, що не мають статусу юридичної особи&quot; затверджено нові інформаційні картки у сфері державної реєстрації.</p>
         </div>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u9820popup">
       <div class="PamphletCloseButton PamphletLightboxPart popup_element clearfix" id="u9820"><!-- group -->
        <div class="size_fixed grpelem" id="u9877"><!-- custom html -->
         
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u9877"><i class="fa fa-times"></i></div>
  	  
        </div>
       </div>
      </div>
     </div>
     <div class="PamphletWidget clearfix widget_invisible grpelem" id="pamphletu20950" data-islightbox="true"><!-- none box -->
      <div class="ThumbGroup clearfix grpelem" id="u20952"><!-- none box -->
       <div class="popup_anchor" id="u20953popup">
        <div class="Thumb popup_element clearfix" id="u20953"><!-- group -->
         <div class="size_fixed grpelem" id="u20954"><!-- custom html -->
          
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u20954"><i class="fa fa-info-circle"></i></div>
  	  
         </div>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u20957popup" data-lightbox="true">
       <div class="ContainerGroup rgba-background clearfix" id="u20957"><!-- stack box -->
        <div class="Container clearfix grpelem" id="u20958"><!-- group -->
         <div class="shadow rounded-corners clearfix grpelem" id="u20959-30"><!-- content -->
          <p id="u20959-2">Державна реєстрація юридичних осіб та фізичних осіб-підприємців</p>
          <p id="u20959-3">&nbsp;</p>
          <p id="u20959-8">Державна реєстрація здійснюється відповідно до Закону України від 15 травня 2003 року № 755 <a class="nonblock" href="http://zakon3.rada.gov.ua/laws/show/755-15">«Про державну реєстрацію юридичних осіб, фізичних осіб - підприємців та громадських формувань»</a> (далі – Закон 755).</p>
          <p id="u20959-10">Державну реєстрацію здійснюють:</p>
          <p id="u20959-12">суб’єкти державної реєстрації:</p>
          <p id="u20959-14">- державні реєстратори;</p>
          <p id="u20959-16">- уповноважені особи суб’єкта державної реєстрації;</p>
          <p id="u20959-18">- нотаріуси.</p>
          <p id="u20959-20">Здійснення державної реєстрації</p>
          <p id="u20959-22">Державна реєстрація проводиться за заявою заявника шляхом звернення до суб’єкта державної реєстрації або нотаріуса.</p>
          <p id="u20959-24">Також документи для державної реєстрації можна подати до центру надання адміністративних послуг, утворений відповідно до Закону України “Про адміністративні послуги”.</p>
          <p id="u20959-26">Важливо! Державна реєстрація проводиться незалежно від місця знаходження юридичної чи фізичної особи в межах Автономної Республіки Крим, області, міст Києва та Севастополя, крім державної реєстрації на підставі документів, поданих в електронній формі, що проводиться незалежно від місця знаходження юридичної чи фізичної особи в межах України (ст. 4 Закону № 755-IV).</p>
          <p id="u20959-28">Державна реєстрація юридичних осіб, фізичних осіб – підприємців та громадських формувань, що не мають статусу юридичної особи, місцезнаходженням / місцем проживання яких є територія АР Крим, міста Севастополя, а також населені пункти, в яких органи державної влади тимчасово не здійснюють свої повноваження, та населені пункти, що розташовані на лінії зіткнення в Донецькій та Луганській областях, здійснюється незалежно від їх місцезнаходження / місця проживання в межах України (пп.1 п.1 наказу Мін’юсту від 25.11.2016 № 3359/5 “Про врегулювання відносин, пов'язаних з державною реєстрацією юридичних осіб, фізичних осіб - підприємців та громадських формувань, що не мають статусу юридичної особи, в межах декількох адміністративно-територіальних одиниць”).</p>
         </div>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u20955popup">
       <div class="PamphletCloseButton PamphletLightboxPart popup_element clearfix" id="u20955"><!-- group -->
        <div class="size_fixed grpelem" id="u20956"><!-- custom html -->
         
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u20956"><i class="fa fa-times"></i></div>
  	  
        </div>
       </div>
      </div>
     </div>
     <div class="PamphletWidget clearfix widget_invisible grpelem" id="pamphletu21166" data-islightbox="true"><!-- none box -->
      <div class="ThumbGroup clearfix grpelem" id="u21174"><!-- none box -->
       <div class="popup_anchor" id="u21175popup">
        <div class="Thumb popup_element clearfix" id="u21175"><!-- group -->
         <div class="size_fixed grpelem" id="u21176"><!-- custom html -->
          
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u21176"><i class="fa fa-info-circle"></i></div>
  	  
         </div>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u21170popup" data-lightbox="true">
       <div class="ContainerGroup rgba-background clearfix" id="u21170"><!-- stack box -->
        <div class="Container clearfix grpelem" id="u21171"><!-- group -->
         <div class="shadow rounded-corners clearfix grpelem" id="u21172-20"><!-- content -->
          <p id="u21172-2">З 4 квітня 2016 року повноваження з реєстрації / зняття з реєстрації місця проживання від Державної міграційної служби перейшли до органів місцевого самоврядування і Центрів надання адміністративних послуг (ЦНАП).</p>
          <p id="u21172-4">У сільській місцевості функції з оформлення прописки / виписки виконують виконавчі органи сільських або&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; селищних рад. У разі, якщо такі органи не створені, обов’язок по реєстрації громадян покладено на сільського голову.</p>
          <p id="u21172-6">Порядок реєстрації місця проживання передбачено Законом України «Про свободу пересування та вільний вибір місця проживання в Україні» від 11.12.2003 № 1382-ІV;</p>
          <p id="u21172-8">&nbsp;Постановою Кабінету Міністрів України від 2 березня 2016 року №207 «Про затвердження Правил реєстрації місця проживання та Порядку передачі органами реєстрації інформації до Єдиного державного демографічного реєстру»</p>
          <p id="u21172-10">За реєстрацію, зняття з реєстрації місця проживання сплачується адміністративний збір, який перераховується до місцевого бюджету:</p>
          <p id="u21172-12">• у разі звернення особи протягом 30 днів з моменту зняття з реєстрації – 0,0085 розміру прожиткового мінімуму (станом на 01.01.2018 р. це 15,00 грн.);</p>
          <p id="u21172-14">• у разі звернення особи з порушенням зазначеного терміну – 0,0255 розміру прожиткового мінімуму (станом на 01.01.2018 р це 45,00 гривень).</p>
          <p id="u21172-16">У разі прийняття рішення про зміну нумерації будинків, перейменування вулиць (проспектів, бульварів, площ, провулків, кварталів тощо), населених пунктів, адміністративно-територіальних одиниць за бажанням особи ці відомості безоплатно вносяться до документів, в яких зазначаються відомості про місце проживання/перебування.</p>
          <p id="u21172-18">Громадянин України, а також іноземець чи особа без громадянства, які постійно або тимчасово проживають в Україні, зобов’язані протягом 30 календарних днів після зняття з реєстрації місця проживання та прибуття до нового місця проживання зареєструвати своє місце проживання. Реєстрація місця проживання за заявою особи може бути здійснена одночасно із зняттям з попереднього місця проживання. При цьому повинні бути виконані всі умови зняття з реєстрації. У разі реєстрації місця проживання одночасно зі зняттям з попереднього місця проживання адміністративний збір стягується тільки за одну адміністративну послугу і зараховується до місцевого бюджету за новим місцем проживання.</p>
         </div>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u21167popup">
       <div class="PamphletCloseButton PamphletLightboxPart popup_element clearfix" id="u21167"><!-- group -->
        <div class="size_fixed grpelem" id="u21168"><!-- custom html -->
         
<!-- This Adobe Muse widget was created by the team at MuseFree.com -->
<div class="u21168"><i class="fa fa-times"></i></div>
  	  
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
   <a class="nonblock nontext anim_swing clip_frame ose_pre_init" id="u1482" href="start.php#start" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=-1,M12=0,M21=0,M22=-1,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-7.105427357601002e-15" data-mu-ie-matrix-dy="-7.105427357601002e-15"><!-- image --><img class="block" id="u1482_img" src="images/arr.gif?crc=4000146970" data-hidpi-src="images/arr_2x.gif?crc=4000146970" alt="" width="50" height="50"/></a>
   <div class="verticalspacer" data-offset-top="2679" data-content-above-spacer="2679" data-content-below-spacer="61"></div>
   <div class="browser_width grpelem" id="u3806-bw">
    <div class="shadow" id="u3806"><!-- group -->
     <div class="clearfix" id="u3806_align_to_page">
      <div class="clearfix grpelem" id="pu28900-4"><!-- column -->
       <a class="nonblock nontext clearfix colelem" id="u28900-4" href="phone/start.php?devicelock=phone"><!-- content --><p>Мобільна версія ресурсу</p></a>
       <div class="clearfix colelem" id="u3809-4"><!-- content -->
        <p>Інформаційний ресурс ЦНАП м. Бердичів 2018 р.</p>
       </div>
      </div>
      <div class="clip_frame grpelem" id="u3836"><!-- svg -->
       <img class="svg" id="u3834" src="images/logo.svg?crc=527946279" width="80" height="51" alt="" data-mu-svgfallback="images/%d0%b2%d1%81%d1%82%d0%b0%d0%b2%d0%bb%d0%b5%d0%bd%d0%bd%d1%8b%d0%b5%20svg%20808x512_poster_.png?crc=84114240"/>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
  ('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="scripts/jquery-1.8.3.min.js?crc=209076791" type="text/javascript">\x3C/script>');
</script>
  <!-- Other scripts -->
  <script type="text/javascript">
   // Decide weather to suppress missing file error or not based on preference setting
var suppressMissingFileError = false
</script>
  <script type="text/javascript">
   window.Muse.assets.check=function(d){if(!window.Muse.assets.checked){window.Muse.assets.checked=!0;var b={},c=function(a,b){if(window.getComputedStyle){var c=window.getComputedStyle(a,null);return c&&c.getPropertyValue(b)||c&&c[b]||""}if(document.documentElement.currentStyle)return(c=a.currentStyle)&&c[b]||a.style&&a.style[b]||"";return""},a=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),
16);return 0},g=function(g){for(var f=document.getElementsByTagName("link"),h=0;h<f.length;h++)if("text/css"==f[h].type){var i=(f[h].href||"").match(/\/?css\/([\w\-]+\.css)\?crc=(\d+)/);if(!i||!i[1]||!i[2])break;b[i[1]]=i[2]}f=document.createElement("div");f.className="version";f.style.cssText="display:none; width:1px; height:1px;";document.getElementsByTagName("body")[0].appendChild(f);for(h=0;h<Muse.assets.required.length;){var i=Muse.assets.required[h],l=i.match(/([\w\-\.]+)\.(\w+)$/),k=l&&l[1]?
l[1]:null,l=l&&l[2]?l[2]:null;switch(l.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");f.className+=" "+k;k=a(c(f,"color"));l=a(c(f,"backgroundColor"));k!=0||l!=0?(Muse.assets.required.splice(h,1),"undefined"!=typeof b[i]&&(k!=b[i]>>>24||l!=(b[i]&16777215))&&Muse.assets.outOfDate.push(i)):h++;f.className="version";break;case "js":h++;break;default:throw Error("Unsupported file type: "+l);}}d?d().jquery!="1.8.3"&&Muse.assets.outOfDate.push("jquery-1.8.3.min.js"):Muse.assets.required.push("jquery-1.8.3.min.js");
f.parentNode.removeChild(f);if(Muse.assets.outOfDate.length||Muse.assets.required.length)f="Некоторые файлы на сервере могут отсутствовать или быть некорректными. Очистите кэш-память браузера и повторите попытку. Если проблему не удается устранить, свяжитесь с разработчиками сайта.",g&&Muse.assets.outOfDate.length&&(f+="\nOut of date: "+Muse.assets.outOfDate.join(",")),g&&Muse.assets.required.length&&(f+="\nMissing: "+Muse.assets.required.join(",")),suppressMissingFileError?(f+="\nUse SuppressMissingFileError key in AppPrefs.xml to show missing file error pop up.",console.log(f)):alert(f)};location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi)?
setTimeout(function(){g(!0)},5E3):g()}};
var muse_init=function(){require.config({baseUrl:""});require(["jquery","museutils","whatinput","jquery.musepolyfill.bgsize","jquery.musemenu","jquery.watch","webpro","musewpslideshow","jquery.museoverlay","touchswipe","jquery.scrolleffects"],function(d){var $ = d;$(document).ready(function(){try{
window.Muse.assets.check($);/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.detectScreenResolution();/* HiDPI screens */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight('.browser_width');/* resize height */
Muse.Utils.requestAnimationFrame(function() { $('body').addClass('initialized'); });/* mark body as initialized */
Muse.Utils.makeButtonsVisibleAfterSettingMinWidth();/* body */
Muse.Utils.initWidget('.MenuBar', ['#bp_infinity'], function(elem) { return $(elem).museMenu(); });/* unifiedNavBar */
Muse.Utils.initWidget('#pamphletu3117', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu3117 */
Muse.Utils.initWidget('#pamphletu20387', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu20387 */
Muse.Utils.initWidget('#pamphletu3172', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu3172 */
Muse.Utils.initWidget('#pamphletu3042', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu3042 */
Muse.Utils.initWidget('#pamphletu3023', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu3023 */
Muse.Utils.initWidget('#pamphletu6361', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu6361 */
Muse.Utils.initWidget('#pamphletu6569', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu6569 */
Muse.Utils.initWidget('#pamphletu34149', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'loose',event:'mouseover',deactivationEvent:'mouseout_both',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:true,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu34149 */
Muse.Utils.initWidget('#pamphletu6393', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu6393 */
Muse.Utils.initWidget('#pamphletu34117', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'loose',event:'mouseover',deactivationEvent:'mouseout_both',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:true,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu34117 */
Muse.Utils.initWidget('#pamphletu6356', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu6356 */
Muse.Utils.initWidget('#pamphletu4816', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu4816 */
Muse.Utils.initWidget('#pamphletu4837', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu4837 */
Muse.Utils.initWidget('#pamphletu4875', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu4875 */
Muse.Utils.initWidget('#pamphletu5135', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu5135 */
Muse.Utils.initWidget('#pamphletu4812', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu4812 */
Muse.Utils.initWidget('#pamphletu7239', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu7239 */
Muse.Utils.initWidget('#pamphletu7203', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu7203 */
Muse.Utils.initWidget('#pamphletu9949', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu9949 */
Muse.Utils.initWidget('#pamphletu9919', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu9919 */
Muse.Utils.initWidget('#pamphletu19136', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu19136 */
Muse.Utils.initWidget('#pamphletu19432', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu19432 */
Muse.Utils.initWidget('#pamphletu9904', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu9904 */
Muse.Utils.initWidget('#pamphletu1633', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu1633 */
Muse.Utils.initWidget('#pamphletu2489', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu2489 */
Muse.Utils.initWidget('#pamphletu2708', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu2708 */
Muse.Utils.initWidget('#pamphletu2960', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu2960 */
Muse.Utils.initWidget('#pamphletu1617', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu1617 */
Muse.Utils.initWidget('#pamphletu7968', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu7968 */
Muse.Utils.initWidget('#pamphletu8476', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu8476 */
Muse.Utils.initWidget('#pamphletu8603', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu8603 */
Muse.Utils.initWidget('#pamphletu8776', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu8776 */
Muse.Utils.initWidget('#pamphletu7957', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu7957 */
Muse.Utils.initWidget('#pamphletu7819', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu7819 */
Muse.Utils.initWidget('#pamphletu9587', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu9587 */
Muse.Utils.initWidget('#pamphletu9648', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu9648 */
Muse.Utils.initWidget('#pamphletu7812', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu7812 */
Muse.Utils.initWidget('#pamphletu341', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'stack',event:'click',deactivationEvent:'none',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:true,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu341 */
Muse.Utils.initWidget('#pamphletu9815', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'lightbox',event:'click',deactivationEvent:'none',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:false,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu9815 */
Muse.Utils.initWidget('#pamphletu20950', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'lightbox',event:'click',deactivationEvent:'none',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:false,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu20950 */
Muse.Utils.initWidget('#pamphletu21166', ['#bp_infinity'], function(elem) { return new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'lightbox',event:'click',deactivationEvent:'none',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:false,shuffle:false,enableSwipe:false,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false,autoActivate_runtime:false,isResponsive:false}); });/* #pamphletu21166 */
Muse.Utils.fullPage('#page');/* 100% height page */
$('#u1482').registerOpacityScrollEffect([{"fade":150,"in":[-Infinity,517.08],"opacity":0},{"in":[517.08,517.08],"opacity":100},{"fade":50,"in":[517.08,Infinity],"opacity":100}]);/* scroll effect */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
}catch(b){if(b&&"function"==typeof b.notify?b.notify():Muse.Assert.fail("Error calling selector function: "+b),false)throw b;}})})};

</script>
  <!-- RequireJS script -->
  <script src="scripts/require.js?crc=4157109226" type="text/javascript" async data-main="scripts/museconfig.js?crc=380897831" onload="if (requirejs) requirejs.onError = function(requireType, requireModule) { if (requireType && requireType.toString && requireType.toString().indexOf && 0 <= requireType.toString().indexOf('#scripterror')) window.Muse.assets.check(); }" onerror="window.Muse.assets.check();"></script>
   </body>
</html>
